#![no_std] // don't link the Rust standard library
#![cfg_attr(not(test), no_main)] // disable all Rust-level entry points
#![cfg_attr(test, allow(dead_code, unused_macros, unused_imports))] // disable unused code in tests
#![feature(asm)]

#[macro_use]
extern crate bootloader_precompiled;
extern crate iroax;

use core::panic::PanicInfo;

use bootloader_precompiled::bootinfo::BootInfo;

#[cfg(not(test))]
fn init(boot_info: &'static BootInfo) -> ! {
    iroax::arch::x86_64::init(&boot_info.memory_map, main);
}

fn main() -> ! {
    use ::core::fmt::Write;
    let mut port = iroax::arch::x86_64::io::serial_port::SerialPort::new(0x3F8);
    port.init();

    iroax::cpu::thread::spawn(|| {
        let mut port = iroax::arch::x86_64::io::serial_port::SerialPort::new(0x3F8);
        port.init();

        loop {
            writeln!(port, "thread 1").unwrap();
            iroax::cpu::thread::yield_now();
        }
    });

    iroax::cpu::thread::spawn(|| {
        let mut port = iroax::arch::x86_64::io::serial_port::SerialPort::new(0x3F8);
        port.init();

        writeln!(port, "thread 2").unwrap();
    });

    loop {
        writeln!(port, "main").unwrap();
        iroax::arch::x86_64::cpu::halt();
        //iroax::cpu::thread::yield_now();
    }
}

#[cfg(not(test))]
entry_point!(init);

#[cfg(not(test))]
#[panic_handler]
fn panic(info: &PanicInfo) -> ! {
    iroax::arch::x86_64::panic(info);
}
