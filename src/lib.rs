#![no_std] // don't link the Rust standard library
#![feature(asm)] // enable inline assembly
#![feature(abi_x86_interrupt)] // enable x86 interrupt ABI
#![feature(const_fn)] // enable const fns
#![feature(try_from)] // enable TryInto trait
#![cfg_attr(not(test), feature(alloc, alloc_error_handler))] // enable alloc
#![feature(box_syntax)] // enable box syntax
#![cfg_attr(not(test), feature(lang_items))] // enable language items
#![feature(get_type_id)] // enable get_type_id
#![feature(slice_concat_ext)] // enable [].join
#![feature(test)] // enable test crate
#![feature(global_asm)] // enable global asm
#![feature(never_type)] // enable ! return type
#![feature(fn_traits)] // enable Fn, FnOnce, FnMut trait usage

#[cfg(not(test))]
#[macro_use]
extern crate alloc;
#[macro_use]
extern crate bitflags;
extern crate bootloader_precompiled;
#[cfg(test)]
#[macro_use]
extern crate std;
#[cfg(test)]
extern crate test;

pub mod stdlib;
pub mod parser;
pub mod acpi;
#[macro_use]
pub mod arch;
pub mod cpu;
#[macro_use]
pub mod mem;
#[cfg(test)]
pub mod test_utils;

#[cfg_attr(not(test), global_allocator)]
pub static GLOBAL_ALLOCATOR: mem::alloc::GlobalAllocator = mem::alloc::GlobalAllocator::new();
