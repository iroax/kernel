use ::std::vec::Vec;

use super::mem::addr::VirtAddr;

pub fn alloc_pages(size: usize) -> Option<VirtAddr> {
    #[repr(C, align(4096))]
    struct Page([u8; 0x1000]);

    let mut allocation = Vec::<Page>::with_capacity(size);
    let addr = VirtAddr::from_ptr(allocation.as_mut_ptr());

    ::core::mem::forget(allocation);

    Some(addr)
}

pub fn dealloc_pages(_addr: VirtAddr, _size: usize) {}
