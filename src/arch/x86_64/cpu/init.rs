use crate::arch::x86_64::cpu::load_task_state;
use crate::arch::x86_64::interrupts::idt::InterruptDescriptorTable;
use crate::arch::x86_64::interrupts::tss::TaskStateSegment;
use crate::arch::x86_64::segmentation::GlobalDescriptorTable;
use crate::arch::x86_64::segmentation::load_segment_selectors;
use crate::arch::x86_64::segmentation::SegmentDescriptor;
use crate::mem::map_pages;
use crate::mem::{alloc_heap, dealloc_heap};
use crate::mem::addr::{PhysAddr, VirtAddr};
use crate::stdlib::boxed::Box;

use super::super::apic::icr;

pub const DOUBLE_FAULT_IST_INDEX: u16 = 0;

#[cfg(not(test))]
global_asm!(include_str!("init.S"));

#[cfg(not(test))]
extern "C" {
    fn _cpu_init_start();
    fn _cpu_init_end();
}

pub struct ApInit {
    pub idt: &'static InterruptDescriptorTable,
    pub double_fault_ist_index: u16,
    pub main: VirtAddr,
    pub init_blk_phys: PhysAddr,
}

#[cfg(not(test))]
fn get_cpu_init_code() -> &'static [u8] {
    let start_addr = VirtAddr::from_ptr(_cpu_init_start as *const ());
    let end_addr = VirtAddr::from_ptr(_cpu_init_end as *const ());
    let len = (end_addr - start_addr) as usize;
    unsafe { ::core::slice::from_raw_parts(start_addr.as_ptr::<u8>(), len) }
}

#[cfg(test)]
fn get_cpu_init_code() -> &'static [u8] {
    &[]
}

static AP_INIT: spin::RwLock<Option<ApInit>> = spin::RwLock::new(None);

pub fn init_aps(ap_init: ApInit) {
    let init_blk_phys = ap_init.init_blk_phys;

    *AP_INIT.write() = Some(ap_init);
    let ap_init_lock = AP_INIT.read();

    assert!(init_blk_phys.as_u64() <= 0xFF000);
    let init_vector = (init_blk_phys.as_u64() >> 12) as u8;

    unsafe {
        // broadcast Init IPI
        icr::write_and_wait(icr::InterruptCommand::new_init());
    }

    let init_blk_rw = map_pages(init_blk_phys, 1, true).expect("could not map init as writable");
    let bytes = get_cpu_init_code();
    let init_blk_ro = map_pages(init_blk_phys, 1, false).expect("could not map init as read-only");

    unsafe {
        ::core::ptr::copy_nonoverlapping(bytes.as_ptr(), init_blk_rw.as_mut_ptr(), bytes.len());

        // write _cpu_init_start RO address
        (init_blk_rw + 0xFD0u64).as_mut_ptr::<u64>().write_volatile(init_blk_rw.as_u64());

        // write _alloc_stack address
        (init_blk_rw + 0xFD8u64).as_mut_ptr::<u64>().write_volatile(_alloc_stack as *const () as u64);

        // write _cpu_init_start RO address
        (init_blk_rw + 0xFE0u64).as_mut_ptr::<u64>().write_volatile(init_blk_ro.as_u64());

        // write _cpu_init_done address
        (init_blk_rw + 0xFE8u64).as_mut_ptr::<u64>().write_volatile(_cpu_init_done as *const () as u64);

        // write P4 phys address
        (init_blk_rw + 0xFF0u64).as_mut_ptr::<u32>().write_volatile(0x1000);

        // broadcast Startup IPI
        icr::write_and_wait(icr::InterruptCommand::new_startup(init_vector));
    }

    // TODO: wait 100ms, if not all APs are present, broadcast Startup IPI again

    ::core::mem::drop(ap_init_lock);

    // TODO: unmap init_blk and init_blk_ro, and dealloc init_blk_phys
    // TODO: unset AP_INIT
}

#[no_mangle]
extern "C" fn _alloc_stack() -> u64 {
    alloc_stack().as_u64()
}

#[no_mangle]
extern "C" fn _cpu_init_done() -> ! {
    let ap_init_lock = AP_INIT.read();
    let ap_init = ap_init_lock.as_ref().expect("AP_INIT not set");

    // initialize TSS
    // ==============
    let mut tss = Box::leak(Box::new(TaskStateSegment::new()));
    tss.interrupt_stack_table[ap_init.double_fault_ist_index as usize] = alloc_stack();

    // initialize GDT
    // ==============
    let gdt = Box::leak(Box::new(GlobalDescriptorTable::new()));
    // read/write
    let data_selector = gdt.add_entry(SegmentDescriptor::UserSegment(0x0000920000000000));
    // exec/read
    let code_selector = gdt.add_entry(SegmentDescriptor::UserSegment(0x00209A0000000000));
    // task state segment
    let tss_selector = gdt.add_entry(SegmentDescriptor::tss_segment(tss));
    gdt.load();

    unsafe {
        load_segment_selectors(code_selector, data_selector);
        load_task_state(tss_selector);
    }

    // load IDT
    // ==============
    ap_init.idt.load();

    // enter main
    // ==========
    unsafe {
        asm!("
            pushq %rax
            retq
        " :: "{rax}"(ap_init.main.as_u64()));
    }

    unreachable!();
}

const STACK_SIZE: usize = 0x8000;
const STACK_ALIGN: usize = 0x1000;

pub fn alloc_stack() -> VirtAddr {
    let stack_start = alloc_heap(STACK_SIZE, STACK_ALIGN);
    stack_start + (STACK_SIZE as u64)
}

pub fn dealloc_stack(stack_end: VirtAddr) {
    let stack_start = stack_end - (STACK_SIZE as u64);
    dealloc_heap(stack_start);
}

