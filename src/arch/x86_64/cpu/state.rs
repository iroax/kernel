use crate::stdlib::boxed::Box;
use crate::mem::addr::VirtAddr;
use super::{read_cs, read_ds};
use crate::cpu::thread::scheduler::end_of_thread;
use crate::arch::x86_64::cpu::thread::yield_now;

#[derive(Debug, Default, Clone)]
pub struct CpuState {
    // TODO: multimedia registers
    // TODO: floating point registers

    pub rax: u64,
    pub rbx: u64,
    pub rcx: u64,
    pub rdx: u64,

    pub rsi: u64,
    pub rdi: u64,
    pub rbp: u64,

    pub r8: u64,
    pub r9: u64,
    pub r10: u64,
    pub r11: u64,
    pub r12: u64,
    pub r13: u64,
    pub r14: u64,
    pub r15: u64,

    pub rip: u64,
    pub cs: u64,
    pub flags: u64,
    pub rsp: u64,
    pub ss: u64,
}

extern "C" fn trampoline<F, T>(f_addr: u64) -> !
    where F: FnOnce() -> T, F: Send + 'static, T: Send + 'static {
    let f = unsafe { Box::from_raw(f_addr as *mut F) };

    end_of_thread(f());

    yield_now();
    unreachable!();
}

impl CpuState {
    pub fn new<F, T>(f: F, stack_end: VirtAddr) -> CpuState
        where F: FnOnce() -> T, F: Send + 'static, T: Send + 'static {
        let mut cpu_state = CpuState::default();
        cpu_state.rip = trampoline::<F, T> as u64;
        cpu_state.rdi = Box::into_raw(Box::new(f)) as u64;
        cpu_state.rsp = stack_end.as_u64();
        cpu_state.cs = read_cs() as u64;
        cpu_state.ss = read_ds() as u64;
        cpu_state.flags = 0x200; // enable interrupts

        return cpu_state;
    }
}
