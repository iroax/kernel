use super::segmentation::SegmentSelector;

pub mod init;
pub mod state;
pub mod thread;

/// Represents a protection ring level.
#[derive(Debug, Copy, Clone, PartialEq, Eq)]
#[repr(u8)]
pub enum PrivilegeLevel {
    /// Privilege-level 0 (most privilege): This level is used by critical system-software
    /// components that require direct access to, and control over, all processor and system
    /// resources. This can include BIOS, memory-management functions, and interrupt handlers.
    Ring0 = 0,

    /// Privilege-level 1 (moderate privilege): This level is used by less-critical system-
    /// software services that can access and control a limited scope of processor and system
    /// resources. Software running at these privilege levels might include some device drivers
    /// and library routines. The actual privileges of this level are defined by the
    /// operating system.
    Ring1 = 1,

    /// Privilege-level 2 (moderate privilege): Like level 1, this level is used by
    /// less-critical system-software services that can access and control a limited scope of
    /// processor and system resources. The actual privileges of this level are defined by the
    /// operating system.
    Ring2 = 2,

    /// Privilege-level 3 (least privilege): This level is used by application software.
    /// Software running at privilege-level 3 is normally prevented from directly accessing
    /// most processor and system resources. Instead, applications request access to the
    /// protected processor and system resources by calling more-privileged service routines
    /// to perform the accesses.
    Ring3 = 3,
}

impl PrivilegeLevel {
    /// Creates a `PrivilegeLevel` from a numeric value. The value must be in the range 0..4.
    ///
    /// This function panics if the passed value is >3.
    pub fn from_u16(value: u16) -> PrivilegeLevel {
        match value {
            0 => PrivilegeLevel::Ring0,
            1 => PrivilegeLevel::Ring1,
            2 => PrivilegeLevel::Ring2,
            3 => PrivilegeLevel::Ring3,
            i => panic!("{} is not a valid privilege level", i),
        }
    }
}

#[inline(always)]
pub fn halt() {
    unsafe {
        asm!("hlt" :::: "volatile");
    }
}

#[inline(always)]
pub fn halt_loop() -> ! {
    loop {
        halt();
    }
}

/// A struct describing a pointer to a descriptor table (GDT / IDT).
/// This is in a format suitable for giving to 'lgdt' or 'lidt'.
#[derive(Debug, Clone, Copy, Default)]
#[repr(C, packed)]
pub struct DescriptorTablePointer {
    pub limit: u16,
    pub base: u64,
}

pub unsafe fn load_global_descriptor_table(pointer: &DescriptorTablePointer) {
    asm!("lgdt ($0)" :: "r"(pointer) : "memory" : "volatile");
}

pub unsafe fn load_interrupt_descriptor_table(pointer: &DescriptorTablePointer) {
    asm!("lidt ($0)" :: "r"(pointer) : "memory" : "volatile");
}

pub unsafe fn load_task_state(task_state_selector: SegmentSelector) {
    asm!("ltr $0" :: "r" (task_state_selector.0) :: "volatile");
}

pub unsafe fn read_cr3() -> u64 {
    let result: u64;
    asm!("mov %cr3, $0" : "=r"(result) ::: "volatile");
    result
}

pub fn read_cs() -> u16 {
    let result: u16;
    unsafe {
        asm!("mov %cs, $0" : "=r"(result) ::: "volatile");
    }
    result
}

pub fn read_ds() -> u16 {
    let result: u16;
    unsafe {
        asm!("mov %ds, $0" : "=r"(result) ::: "volatile");
    }
    result
}

pub unsafe fn write_cr3(value: u64) {
    asm!("mov $0, %cr3" :: "r"(value) :: "volatile");
}

pub unsafe fn invalidate_page(addr: super::VirtAddr) {
    asm!("invlpg ($0)" :: "r"(addr.as_u64()) :: "volatile");
}
