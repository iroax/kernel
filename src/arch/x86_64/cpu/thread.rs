use core::any::Any;
use core::cell::UnsafeCell;

use crate::cpu::thread::scheduler;
use crate::cpu::thread::scheduler::ThreadImp;
use crate::mem::addr::VirtAddr;

use super::init::{alloc_stack, dealloc_stack};
use super::state::CpuState;

global_asm!(include_str!("context_switch.S"));

#[derive(Debug)]
pub struct Thread {
    stack_end: VirtAddr,
    cpu_state: CpuState,
}

impl ThreadImp for Thread {
    fn as_any(&self) -> &Any {
        self
    }

    fn as_any_mut(&mut self) -> &mut Any {
        self
    }
}

impl Thread {
    pub fn new<F, T>(entry_point: F, _stack_size: Option<usize>) -> Thread
        where F: FnOnce() -> T, F: Send + 'static, T: Send + 'static {
        let stack_end = alloc_stack();

        Thread {
            stack_end,
            cpu_state: CpuState::new(entry_point, stack_end),
        }
    }

    pub fn set_cpu_state_from(&mut self, cpu_state: &CpuState) {
        self.cpu_state = cpu_state.clone();
    }

    pub fn get_cpu_state_into(&self, cpu_state: &mut CpuState) {
        *cpu_state = self.cpu_state.clone();
    }
}

impl Drop for Thread {
    fn drop(&mut self) {
        dealloc_stack(self.stack_end);
    }
}

pub fn yield_now() {
    unsafe {
        asm!("int $$47");
    }
}

pub fn context_switch(cpu_state: &mut CpuState) {
    let cell = UnsafeCell::new(cpu_state);

    scheduler::context_switch(|old| {
        let old = old.as_any_mut().downcast_mut::<Thread>()
            .expect("could not downcast old thread imp");

        let cpu_state = unsafe { &*cell.get() };

        old.set_cpu_state_from(cpu_state);
    }, |new| {
        let new = new.as_any().downcast_ref::<Thread>()
            .expect("could not downcast new thread imp");

        let cpu_state = unsafe { &mut *cell.get() };

        new.get_cpu_state_into(cpu_state);
    });
}
