use bit_field::BitField;
use crate::mem::addr::VirtAddr;

#[derive(Debug, PartialEq)]
pub enum Destination {
    OnlyToSelf,
    AllIncludingSelf,
    AllExcludingSelf,
}

#[derive(Debug, PartialEq)]
pub enum TriggerMode {
    Edge,
    Level,
}

#[derive(Debug, PartialEq)]
pub enum Level {
    High,
    Low,
}

#[derive(Debug, PartialEq)]
pub enum DeliveryStatus {
    Idle,
    Pending,
}

#[derive(Debug, PartialEq)]
pub enum DestinationMode {
    Logical,
    Physical,
}

#[derive(Debug, PartialEq)]
pub enum DeliveryMode {
    Fixed,
    LowestPriority,
    SMI,
    NMI,
    Init,
    Startup,
}

#[derive(Debug, PartialEq, Copy, Clone)]
pub struct InterruptCommand(u64);

impl InterruptCommand {
    pub fn new_init() -> InterruptCommand {
        InterruptCommand(0b00000000_00001100_01000101_00000000)
    }

    pub fn new_startup(init_vector: u8) -> InterruptCommand {
        let mut cmd = InterruptCommand(0b00000000_00001100_01000110_00000000);
        cmd.set_vector(init_vector);
        cmd
    }

    pub fn new_interrupt(vector: u8) -> InterruptCommand {
        let mut cmd = InterruptCommand(0b00000000_00001100_01000001_00000000);
        cmd.set_vector(vector);
        cmd
    }

    pub fn get_delivery_status(&self) -> DeliveryStatus {
        if self.0.get_bit(12) { DeliveryStatus::Pending } else { DeliveryStatus::Idle }
    }

    pub fn set_vector(&mut self, vector: u8) {
        self.0.set_bits(0..8, vector as u64);
    }

    pub fn set_destination(&mut self, destination: Destination) {
        match destination {
            Destination::OnlyToSelf => self.0.set_bits(18..20, 0b01),
            Destination::AllIncludingSelf => self.0.set_bits(18..20, 0b10),
            Destination::AllExcludingSelf => self.0.set_bits(18..20, 0b11),
        };
    }
}

fn get_ptrs() -> (*mut u32, *mut u32) {
    static PTRS: spin::Once<(VirtAddr, VirtAddr)> = spin::Once::new();

    let (low, high) = *PTRS.call_once(|| {
        let base = super::get_base_addr();

        let low = VirtAddr::new(base.as_u64() + 0x300);
        let high = VirtAddr::new(base.as_u64() + 0x310);

        (low, high)
    });

    (low.as_mut_ptr(), high.as_mut_ptr())
}

pub unsafe fn write(cmd: InterruptCommand) {
    let (ptr_low, ptr_high) = get_ptrs();

    ptr_high.write_volatile((cmd.0 >> 32) as u32);
    ptr_low.write_volatile(cmd.0 as u32);
}

pub unsafe fn read() -> InterruptCommand {
    let (ptr_low, ptr_high) = get_ptrs();

    let high = ptr_high.read_volatile();
    let low = ptr_low.read_volatile();
    InterruptCommand((high as u64) << 32 | (low as u64))
}

pub unsafe fn write_and_wait(cmd: InterruptCommand) {
    write(cmd);

    while read().get_delivery_status() == DeliveryStatus::Pending {
        ::core::sync::atomic::spin_loop_hint();
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_new_startup() {
        assert_eq!(InterruptCommand::new_startup(0x11).0, 0x000C4611);
    }
}
