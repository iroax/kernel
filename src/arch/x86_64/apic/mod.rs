use crate::mem::addr::{PhysAddr, VirtAddr};
use crate::mem::map_pages;

pub mod icr;
pub mod id;

pub fn get_base_addr() -> VirtAddr {
    static BASE_ADDR: spin::Once<VirtAddr> = spin::Once::new();

    *BASE_ADDR.call_once(|| {
        map_pages(PhysAddr::new(0xFEE00000), 1, true)
            .expect("could not map APIC page")
    })
}
