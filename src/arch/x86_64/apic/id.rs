use crate::mem::addr::VirtAddr;

fn get_ptr() -> (*mut u32) {
    static ADDR: spin::Once<VirtAddr> = spin::Once::new();

    let addr = *ADDR.call_once(|| {
        let base = super::get_base_addr();

        base + 0x20u64
    });

    addr.as_mut_ptr()
}

pub unsafe fn read() -> u8 {
    ((*get_ptr()) >> 24) as u8
}
