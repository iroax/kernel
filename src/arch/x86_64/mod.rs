use core::panic::PanicInfo;

use bootloader_precompiled::bootinfo::MemoryMap;

use crate::mem::addr::VirtAddr;
use crate::mem::alloc_heap;
use crate::stdlib::boxed::Box;

use self::cpu::init::ApInit;
use self::cpu::load_task_state;
pub use self::cpu::PrivilegeLevel;
use self::interrupts::idt::{ExceptionStackFrame, HandlerFunc, HandlerFuncWithErrCode, InterruptDescriptorTable};
use self::interrupts::pic8259::ChainedPics;
use self::interrupts::tss::TaskStateSegment;
use self::segmentation::{GlobalDescriptorTable, load_segment_selectors, SegmentDescriptor};
use crate::cpu::thread;

pub mod acpi;
pub mod apic;
#[macro_use]
pub mod cpu;
pub mod interrupts;
pub mod segmentation;
pub mod io;
pub mod paging;
pub mod qemu;

const DOUBLE_FAULT_IST_INDEX: u16 = 0;

const PIC_1_OFFSET: u8 = 32;
const PIC_2_OFFSET: u8 = PIC_1_OFFSET + 8;
const TIMER_INTERRUPT_ID: u8 = PIC_1_OFFSET + 0;
const CONTEXT_SWITCH_INTERRUPT_ID: u8 = 47;

static PIC_8259: spin::Once<ChainedPics> = spin::Once::new();

pub fn get_pic_8259() -> &'static ChainedPics {
    PIC_8259.call_once(|| {
        unsafe { ChainedPics::new(PIC_1_OFFSET, PIC_2_OFFSET) }
    })
}

extern "x86-interrupt" fn timer_handler(_stack_frame: &mut ExceptionStackFrame) {
    /*let mut command = self::apic::icr::InterruptCommand::new_interrupt(CONTEXT_SWITCH_INTERRUPT_ID);
    command.set_destination(self::apic::icr::Destination::AllExcludingSelf);
    unsafe { self::apic::icr::write_and_wait(command) };*/

    unsafe { get_pic_8259().notify_end_of_interrupt(TIMER_INTERRUPT_ID); }

    thread::yield_now();
}

extern "x86-interrupt" fn default_double_fault_handler(stack_frame: &mut ExceptionStackFrame, _error_code: u64) {
    panic!("DOUBLE FAULT EXCEPTION\n{:#?}", stack_frame);
}

extern "x86-interrupt" {
    fn _cpu_context_switch_interrupt_handler(stack_frame: &mut ExceptionStackFrame);
}

#[no_mangle]
extern "C" fn _cpu_context_switch(cpu_state_addr: u64) {
    let cpu_state = unsafe { &mut *(cpu_state_addr as *mut cpu::state::CpuState) };

    self::cpu::thread::context_switch(cpu_state);
}

pub struct InitConfig {
    pub double_fault_handler: HandlerFuncWithErrCode,
    pub breakpoint_handler: Option<HandlerFunc>,
    pub memory_map: &'static MemoryMap,
    pub main: fn() -> !,
}

pub fn get_default_config(memory_map: &'static MemoryMap, main: fn() -> !) -> InitConfig {
    InitConfig {
        memory_map,
        main,
        double_fault_handler: default_double_fault_handler,
        breakpoint_handler: None,
    }
}

pub fn init_with_config(config: InitConfig) -> ! {
    // initialize paging and heap
    // ==========================
    crate::mem::set_paging_impl(paging::get_stage1_impl());
    let init_blk_phys = crate::mem::init(config.memory_map);

    // TODO: set stage 2 paging impl

    // initialize TSS
    // ==============
    let tss = Box::leak(Box::new(TaskStateSegment::new()));
    tss.interrupt_stack_table[DOUBLE_FAULT_IST_INDEX as usize] = {
        const STACK_SIZE: usize = 4096;

        let stack_start = alloc_heap(STACK_SIZE, 0x1000);
        let stack_end = stack_start + STACK_SIZE;
        stack_end
    };

    // initialize GDT
    // ==============
    let gdt = Box::leak(Box::new(GlobalDescriptorTable::new()));
    // read/write
    let data_selector = gdt.add_entry(SegmentDescriptor::UserSegment(0x0000920000000000));
    // exec/read
    let code_selector = gdt.add_entry(SegmentDescriptor::UserSegment(0x00209A0000000000));
    // task state segment
    let tss_selector = gdt.add_entry(SegmentDescriptor::tss_segment(tss));
    gdt.load();

    unsafe {
        load_segment_selectors(code_selector, data_selector);
        load_task_state(tss_selector);
    }

    // initialize PIC 8259
    // ===================
    unsafe { get_pic_8259().initialize(); }

    // initialize IDT
    // ==============
    let idt = Box::leak(Box::new(InterruptDescriptorTable::new()));
    idt.double_fault.set_handler_fn(config.double_fault_handler, code_selector)
        .set_stack_index(DOUBLE_FAULT_IST_INDEX);

    idt[TIMER_INTERRUPT_ID].set_handler_fn(timer_handler, code_selector);
    idt[CONTEXT_SWITCH_INTERRUPT_ID].set_handler_fn(_cpu_context_switch_interrupt_handler, code_selector);

    match config.breakpoint_handler {
        Some(handler) => {
            idt.breakpoint.set_handler_fn(handler, code_selector);
        }
        None => {}
    }

    idt.load();

    // initialize ACPI
    // ===============
    acpi::init();

    // initialize AP cores
    // ===================
    cpu::init::init_aps(ApInit {
        idt,
        init_blk_phys,
        double_fault_ist_index: DOUBLE_FAULT_IST_INDEX,
        main: VirtAddr::new(config.main as u64),
    });

    // initialize threading
    // ====================
    thread::set_idle_fn(cpu::halt_loop);

    thread::thread_builder::ThreadBuilder::new()
        .set_name("main kernel thread")
        .set_stack_size(0x1000)
        .spawn(move || {
            (config.main)();
        });

    self::interrupts::enable();

    thread::yield_now();
    unreachable!();
}

pub fn init(memory_map: &'static MemoryMap, main: fn() -> !) -> ! {
    let config = get_default_config(memory_map, main);
    init_with_config(config);
}

pub fn panic(info: &PanicInfo) -> ! {
    use ::core::fmt::Write;

    self::interrupts::disable();

    let mut serial_port = self::io::serial_port::SerialPort::new(0x3F8);
    serial_port.init();
    writeln!(serial_port, "{}", info).unwrap();

    let vga_control_port = self::io::port::Port::new(0x3D4);
    let vga_data_port = self::io::port::Port::new(0x3D5);

    unsafe {
        vga_control_port.write(0x0A as u8);
        vga_data_port.write(0x20 as u8);
    }

    let buffer = 0xb8000 as *mut u8;
    unsafe {
        for x in (0..80 * 25 * 2).step_by(2) {
            *buffer.offset(x) = b' ';
            *buffer.offset(x + 1) = 0x4f;
        }

        *buffer.offset(162) = b':';
        *buffer.offset(164) = b'(';
    }

    self::cpu::halt_loop();
}
