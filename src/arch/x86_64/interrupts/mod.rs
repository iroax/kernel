pub mod idt;
pub mod tss;
pub mod pic8259;

#[inline]
pub fn enable() {
    unsafe {
        asm!("sti" :::: "volatile");
    }
}

#[inline]
pub fn disable() {
    unsafe {
        asm!("cli" :::: "volatile");
    }
}
