use super::io::port::Port;

pub unsafe fn exit_qemu() {
    let port = Port::<u32>::new(0xf4);
    port.write(0);
}
