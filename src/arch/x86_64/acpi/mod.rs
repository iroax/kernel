use crate::acpi::*;
use crate::acpi::aml::AmlValue;
pub use crate::acpi::consts::{ACPI_PM1_CNT, ACPI_PM1_CNT_SLP_EN};
use crate::arch::x86_64::io::port::Port;
use crate::mem::addr::PhysAddr;

const RSDP_START_ADDR: u64 = 0xE_0000;
const RSDP_END_ADDR: u64 = 0xF_FFFF;

pub fn init() {
    let rsdp_start_addr = PhysAddr::new(RSDP_START_ADDR);
    let rsdp_end_addr = PhysAddr::new(RSDP_END_ADDR);

    crate::acpi::init(rsdp_start_addr, rsdp_end_addr);

    match enable() {
        EnableResult::Enabled | EnableResult::AlreadyEnabled => {}
        err => panic!("ACPI error while enabling: {:?}", err),
    }
}

#[derive(Debug)]
enum EnableResult {
    AlreadyEnabled,
    Enabled,
    Timeout,
    CouldNotEnable,
}

fn sleep(ms: u32) {
    use core::sync::atomic::spin_loop_hint as cpu_relax;

    for _i in 0..(ms * 2000) {
        cpu_relax();
    }
}

fn enable() -> EnableResult {
    let acpi = get_acpi();

    let pm1a_cnt_blk = acpi.structs.fadt.pm1a_control_block as u16;
    let pm1b_cnt_blk = acpi.structs.fadt.pm1b_control_block as u16;
    let pm1a_cnt = pm1a_cnt_blk + ACPI_PM1_CNT;
    let pm1b_cnt = pm1b_cnt_blk + ACPI_PM1_CNT;
    let smi_cmd = acpi.structs.fadt.smi_command_port as u16;
    let acpi_enable = acpi.structs.fadt.acpi_enable;

    let pm1a_cnt_port = Port::<u16>::new(pm1a_cnt);
    let pm1b_cnt_port = Port::<u16>::new(pm1b_cnt);
    let smi_cmd_port = Port::<u8>::new(smi_cmd);

    // check if acpi is enabled
    if (unsafe { pm1a_cnt_port.read() } & 1) == 0 {
        // check if acpi can be enabled
        if smi_cmd != 0 && acpi_enable != 0 {
            // send acpi enable command
            unsafe { smi_cmd_port.write(acpi_enable); }

            let mut i = 0;

            // give 3 seconds time to enable acpi
            while i < 300 {
                if unsafe { pm1a_cnt_port.read() } & 1 == 1 {
                    break;
                }
                sleep(10);
                i += 1;
            }

            if pm1b_cnt_blk != 0 {
                while i < 300 {
                    if unsafe { pm1b_cnt_port.read() } & 1 == 1 {
                        break;
                    }
                    sleep(10);
                    i += 1;
                }
            }

            if i < 300 {
                return EnableResult::Enabled;
            } else {
                return EnableResult::Timeout;
            }
        } else {
            return EnableResult::CouldNotEnable;
        }
    } else {
        return EnableResult::AlreadyEnabled;
    }
}

pub fn power_off() {
    let acpi = get_acpi();

    let pm1a_cnt_blk = acpi.structs.fadt.pm1a_control_block as u16;
    let pm1b_cnt_blk = acpi.structs.fadt.pm1b_control_block as u16;
    let pm1a_cnt = pm1a_cnt_blk + ACPI_PM1_CNT;
    let pm1b_cnt = pm1b_cnt_blk + ACPI_PM1_CNT;

    let (_, s5) = acpi.dsdt_aml.get("\\._S5_")
        .expect("could not find _S5_")
        .as_def_package()
        .expect("could not read _S5_ as package");

    assert!(s5.len() >= 2);

    let slp_typa = s5[0].as_u32().expect("could not cast SLP_TYPa to u32") as u16;
    let slp_typb = s5[1].as_u32().expect("could not cast SLP_TYPb to u32") as u16;

    let port = Port::new(pm1a_cnt);
    unsafe { port.write(slp_typa | ACPI_PM1_CNT_SLP_EN); }

    if pm1b_cnt_blk != 0 {
        let port = Port::new(pm1b_cnt);
        unsafe { port.write(slp_typb | ACPI_PM1_CNT_SLP_EN); }
    }
}
