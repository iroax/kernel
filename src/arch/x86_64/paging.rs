use core::ops::{Index, IndexMut};
use super::cpu::{invalidate_page, read_cr3, write_cr3};
use super::super::super::mem::{alloc_phys, PagingImpl};
use super::super::super::mem::addr::{PagingRoot, PhysAddr, VirtAddr};

const ENTRY_COUNT: usize = 4096 / 8;
const ENTRY_ADDR_MASK: u64 = 0x000fffff_fffff000;

bitflags! {
    struct EntryFlags: u64 {
        const PRESENT =         1 << 0;
        const WRITABLE =        1 << 1;
        const USER_ACCESSIBLE = 1 << 2;
        const WRITE_THROUGH =   1 << 3;
        const NO_CACHE =        1 << 4;
        const ACCESSED =        1 << 5;
        const DIRTY =           1 << 6;
        const HUGE_PAGE =       1 << 7;
        const GLOBAL =          1 << 8;
        const NO_EXECUTE =      1 << 63;
    }
}

#[derive(Debug, Copy, Clone)]
struct Entry(u64);

impl Entry {
    #[inline(always)]
    fn read(&self) -> u64 {
        unsafe { ::core::ptr::read_volatile(&self.0) }
    }

    #[inline(always)]
    fn write(&mut self, val: u64) {
        unsafe { ::core::ptr::write_volatile(&mut self.0, val); }
    }

    pub fn get_addr(&self) -> PhysAddr {
        PhysAddr::new(self.read() & ENTRY_ADDR_MASK)
    }

    pub fn get_flags(&self) -> EntryFlags {
        EntryFlags::from_bits_truncate(self.read())
    }

    pub fn set_addr(&mut self, addr: PhysAddr) {
        self.write((self.read() & !ENTRY_ADDR_MASK) | (addr.as_u64() & ENTRY_ADDR_MASK));
    }

    pub fn set_flags(&mut self, flags: EntryFlags) {
        self.write((self.read() & ENTRY_ADDR_MASK) | (flags.bits() & !ENTRY_ADDR_MASK));
    }

    pub fn set(&mut self, addr: PhysAddr, flags: EntryFlags) {
        self.write((addr.as_u64() & ENTRY_ADDR_MASK) | (flags.bits() & !ENTRY_ADDR_MASK));
    }
}

struct Table {
    entries: [Entry; ENTRY_COUNT],
}

impl Index<usize> for Table {
    type Output = Entry;

    fn index(&self, index: usize) -> &Entry {
        &self.entries[index]
    }
}

impl IndexMut<usize> for Table {
    fn index_mut(&mut self, index: usize) -> &mut Entry {
        &mut self.entries[index]
    }
}

fn get_current_paging_root() -> PagingRoot {
    PagingRoot(unsafe { read_cr3() })
}

unsafe fn set_current_paging_root(root: PagingRoot) {
    write_cr3(root.0);
}

fn map_pages_stage1(_root: PagingRoot, first_phys_page: PhysAddr, first_virt_page: VirtAddr, num_pages: usize, writable: bool) {
    const RECURSIVE_PAGE_TABLE: u64 = 0xFFFFFFFFFFFFF000;
    let p4 = unsafe { &mut *VirtAddr::new(RECURSIVE_PAGE_TABLE).as_mut_ptr::<Table>() };
    let flags = if writable { EntryFlags::PRESENT | EntryFlags::WRITABLE } else { EntryFlags::PRESENT };

    fn with_tmp_mapping<F: FnOnce(VirtAddr)>(p4: &mut Table, phys_addr: PhysAddr, f: F) {
        let tmp_addr = VirtAddr::new(RECURSIVE_PAGE_TABLE - 0x1000);
        let backup_entry = p4[510];
        p4[510].set(phys_addr, EntryFlags::PRESENT | EntryFlags::WRITABLE);
        unsafe { invalidate_page(tmp_addr); }

        f(tmp_addr);
        p4[510] = backup_entry;
        unsafe { invalidate_page(tmp_addr); }
    }

    let mut phys_addr = first_phys_page;
    let mut virt_addr = first_virt_page;
    for _i in 0..num_pages {
        let p4_entry = &mut p4[u64::from(virt_addr.p4_index()) as usize];

        if !p4_entry.get_flags().contains(flags) {
            if p4_entry.get_addr().is_null() {
                let frame_addr = alloc_phys(1)
                    .expect("could not allocate frame for paging");
                p4_entry.set_addr(frame_addr);
            }

            p4_entry.set_flags(flags);
        }

        let p3_phys_addr = p4_entry.get_addr();
        let mut p2_phys_addr = None;
        with_tmp_mapping(p4, p3_phys_addr, |p3_virt_addr| {
            let p3 = unsafe { &mut *p3_virt_addr.as_mut_ptr::<Table>() };
            let p3_entry = &mut p3[u64::from(virt_addr.p3_index()) as usize];

            if !p3_entry.get_flags().contains(flags) {
                if p3_entry.get_addr().is_null() {
                    let frame_addr = alloc_phys(1)
                        .expect("could not allocate frame for paging");
                    p3_entry.set_addr(frame_addr);
                }

                p3_entry.set_flags(flags);
            }

            p2_phys_addr = Some(p3_entry.get_addr());
        });

        let mut p1_phys_addr = None;
        with_tmp_mapping(p4, p2_phys_addr.unwrap(), |p2_virt_addr| {
            let p2 = unsafe { &mut *p2_virt_addr.as_mut_ptr::<Table>() };
            let p2_entry = &mut p2[u64::from(virt_addr.p2_index()) as usize];

            if !p2_entry.get_flags().contains(flags) {
                if p2_entry.get_addr().is_null() {
                    let frame_addr = alloc_phys(1)
                        .expect("could not allocate frame for paging");
                    p2_entry.set_addr(frame_addr);
                }

                p2_entry.set_flags(flags);
            }

            p1_phys_addr = Some(p2_entry.get_addr());
        });

        with_tmp_mapping(p4, p1_phys_addr.unwrap(), |p1_virt_addr| {
            let p1 = unsafe { &mut *p1_virt_addr.as_mut_ptr::<Table>() };
            let p1_entry = &mut p1[u64::from(virt_addr.p1_index()) as usize];

            p1_entry.set(phys_addr, flags);
        });

        phys_addr += 0x1000u64;
        virt_addr += 0x1000u64;
    }
}

fn unmap_pages(_root: PagingRoot, _first_virt_page: VirtAddr, _num_pages: usize) {}

fn virt_to_phys(_root: PagingRoot, _virt: VirtAddr) -> Option<PhysAddr> {
    None
}

fn phys_to_virt(_root: PagingRoot, _phys: PhysAddr) -> Option<VirtAddr> {
    None
}

pub fn get_stage1_impl() -> PagingImpl {
    PagingImpl {
        get_current_paging_root,
        set_current_paging_root,
        map_pages: map_pages_stage1,
        unmap_pages,
        virt_to_phys,
        phys_to_virt,
    }
}
