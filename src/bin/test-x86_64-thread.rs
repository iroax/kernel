#![no_std]
#![cfg_attr(not(test), no_main)]
#![cfg_attr(test, allow(dead_code, unused_macros, unused_imports))]
#![feature(asm)] // enable inline assembly
#![feature(abi_x86_interrupt)] // enable x86 interrupt ABI

#[macro_use]
extern crate bootloader_precompiled;
extern crate iroax;

use bootloader_precompiled::bootinfo::BootInfo;
use core::fmt::Write;
use core::panic::PanicInfo;
use iroax::arch::x86_64::io::serial_port::SerialPort;
use iroax::arch::x86_64::qemu::exit_qemu;
use iroax::cpu::thread;
use core::sync::atomic::{ATOMIC_USIZE_INIT, Ordering};
use iroax::stdlib::sync::Arc;

#[cfg(not(test))]
fn init(boot_info: &'static BootInfo) -> ! {
    iroax::arch::x86_64::init(&boot_info.memory_map, main);
}

fn main() -> ! {
    let my_counter = Arc::new(ATOMIC_USIZE_INIT);
    let their_counter = my_counter.clone();

    let handle = thread::spawn(move || {
        assert_eq!(their_counter.load(Ordering::SeqCst), 0);
        their_counter.store(1, Ordering::SeqCst);
        0xDEADBEEFu32
    });

    assert_eq!(my_counter.load(Ordering::SeqCst), 0);
    assert_eq!(handle.join(), Some(0xDEADBEEFu32));
    assert_eq!(my_counter.load(Ordering::SeqCst), 1);

    let mut serial_port = SerialPort::new(0x3F8);
    serial_port.init();
    writeln!(serial_port, "ok").unwrap();

    unsafe { exit_qemu(); }
    iroax::arch::x86_64::cpu::halt_loop();
}

#[cfg(not(test))]
entry_point!(init);

#[cfg(not(test))]
#[panic_handler]
fn panic(info: &PanicInfo) -> ! {
    let mut serial_port = SerialPort::new(0x3F8);
    serial_port.init();
    writeln!(serial_port, "{}", info).unwrap();

    unsafe { exit_qemu(); }
    loop {}
}
