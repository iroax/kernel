#![no_std]
#![cfg_attr(not(test), no_main)]
#![cfg_attr(test, allow(dead_code, unused_macros, unused_imports))]

#[macro_use]
extern crate bootloader_precompiled;
extern crate iroax;

use bootloader_precompiled::bootinfo::BootInfo;
use core::fmt::Write;
use core::panic::PanicInfo;
use iroax::arch::x86_64::io::serial_port::SerialPort;
use iroax::arch::x86_64::qemu::exit_qemu;

#[cfg(not(test))]
fn main(_boot_info: &'static BootInfo) -> ! {
    let mut serial_port = SerialPort::new(0x3F8);
    serial_port.init();
    writeln!(serial_port, "ok").unwrap();

    unsafe { exit_qemu(); }
    loop {}
}

#[cfg(not(test))]
entry_point!(main);

#[cfg(not(test))]
#[panic_handler]
fn panic(_info: &PanicInfo) -> ! {
    unsafe { exit_qemu(); }
    loop {}
}
