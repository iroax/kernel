#![no_std]
#![cfg_attr(not(test), no_main)]
#![cfg_attr(test, allow(dead_code, unused_macros, unused_imports))]
#![feature(asm)] // enable inline assembly
#![feature(abi_x86_interrupt)] // enable x86 interrupt ABI

#[macro_use]
extern crate bootloader_precompiled;
extern crate iroax;

use bootloader_precompiled::bootinfo::BootInfo;
use core::fmt::Write;
use core::panic::PanicInfo;
use core::sync::atomic::{AtomicUsize, Ordering};
use iroax::arch::x86_64::{get_default_config, init_with_config};
use iroax::arch::x86_64::interrupts::idt::ExceptionStackFrame;
use iroax::arch::x86_64::io::serial_port::SerialPort;
use iroax::arch::x86_64::qemu::exit_qemu;

static BREAKPOINT_HANDLER_CALLED: AtomicUsize = AtomicUsize::new(0);

extern "x86-interrupt" fn breakpoint_handler(
    _stack_frame: &mut ExceptionStackFrame)
{
    BREAKPOINT_HANDLER_CALLED.fetch_add(1, Ordering::SeqCst);
}

#[cfg(not(test))]
fn init(boot_info: &'static BootInfo) -> ! {
    let mut config = get_default_config(&boot_info.memory_map, main);
    config.breakpoint_handler = Some(breakpoint_handler);
    init_with_config(config);
}

fn main() -> ! {
    unsafe {
        asm!("int $$3" :::: "volatile");
    }

    let mut serial_port = SerialPort::new(0x3F8);
    serial_port.init();

    match BREAKPOINT_HANDLER_CALLED.load(Ordering::SeqCst) {
        1 => {
            writeln!(serial_port, "ok").unwrap();
        }
        0 => {
            writeln!(serial_port, "failed").unwrap();
            writeln!(serial_port, "Breakpoint handler was not called").unwrap();
        }
        other => {
            writeln!(serial_port, "failed").unwrap();
            writeln!(serial_port, "Breakpoint handler called {} times", other).unwrap();
        }
    }

    unsafe { exit_qemu(); }
    loop {}
}

#[cfg(not(test))]
entry_point!(init);

#[cfg(not(test))]
#[panic_handler]
fn panic(_info: &PanicInfo) -> ! {
    unsafe { exit_qemu(); }
    loop {}
}
