#![no_std]
#![cfg_attr(not(test), no_main)]
#![cfg_attr(test, allow(dead_code, unused_macros, unused_imports))]
#![feature(asm)] // enable inline assembly
#![feature(abi_x86_interrupt)] // enable x86 interrupt ABI

#[macro_use]
extern crate bootloader_precompiled;
extern crate iroax;

use bootloader_precompiled::bootinfo::BootInfo;
use core::fmt::Write;
use core::panic::PanicInfo;
use iroax::arch::x86_64::{get_default_config, init_with_config};
use iroax::arch::x86_64::interrupts::idt::ExceptionStackFrame;
use iroax::arch::x86_64::io::serial_port::SerialPort;
use iroax::arch::x86_64::qemu::exit_qemu;

extern "x86-interrupt" fn double_fault_handler(
    _stack_frame: &mut ExceptionStackFrame, _error_code: u64)
{
    let mut serial_port = SerialPort::new(0x3F8);
    serial_port.init();
    writeln!(serial_port, "ok").unwrap();

    unsafe { exit_qemu(); }
    loop {}
}

#[cfg(not(test))]
fn init(boot_info: &'static BootInfo) -> ! {
    let mut config = get_default_config(&boot_info.memory_map, main);
    config.double_fault_handler = double_fault_handler;
    init_with_config(config);
}

fn main() -> ! {
    #[allow(unconditional_recursion)]
    fn stack_overflow() {
        stack_overflow();
    }

    stack_overflow();

    unsafe { exit_qemu(); }
    loop {}
}

#[cfg(not(test))]
entry_point!(init);

#[cfg(not(test))]
#[panic_handler]
fn panic(_info: &PanicInfo) -> ! {
    unsafe { exit_qemu(); }
    loop {}
}
