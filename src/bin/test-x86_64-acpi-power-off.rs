#![no_std]
#![cfg_attr(not(test), no_main)]
#![cfg_attr(test, allow(dead_code, unused_macros, unused_imports))]
#![feature(asm)] // enable inline assembly
#![feature(abi_x86_interrupt)] // enable x86 interrupt ABI

#[macro_use]
extern crate bootloader_precompiled;
extern crate iroax;

use bootloader_precompiled::bootinfo::BootInfo;
use core::fmt::Write;
use core::panic::PanicInfo;
use iroax::arch::x86_64::io::serial_port::SerialPort;
use iroax::arch::x86_64::qemu::exit_qemu;

#[cfg(not(test))]
fn init(boot_info: &'static BootInfo) -> ! {
    iroax::arch::x86_64::init(&boot_info.memory_map, main);
}

fn main() -> ! {
    let mut serial_port = SerialPort::new(0x3F8);
    serial_port.init();
    writeln!(serial_port, "ok").unwrap();

    iroax::arch::x86_64::acpi::power_off();

    // failing test will time out even though it has reported "ok"

    iroax::arch::x86_64::cpu::halt_loop();
}

#[cfg(not(test))]
entry_point!(init);

#[cfg(not(test))]
#[panic_handler]
fn panic(info: &PanicInfo) -> ! {
    let mut serial_port = SerialPort::new(0x3F8);
    serial_port.init();
    writeln!(serial_port, "{}", info).unwrap();

    unsafe { exit_qemu(); }
    loop {}
}
