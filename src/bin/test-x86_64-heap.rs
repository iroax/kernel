#![no_std]
#![cfg_attr(not(test), no_main)]
#![cfg_attr(test, allow(dead_code, unused_macros, unused_imports))]
#![feature(asm)] // enable inline assembly
#![feature(abi_x86_interrupt)] // enable x86 interrupt ABI

#[macro_use]
extern crate bootloader_precompiled;
extern crate iroax;

use bootloader_precompiled::bootinfo::BootInfo;
use core::fmt::Write;
use core::panic::PanicInfo;
use iroax::arch::x86_64::io::serial_port::SerialPort;
use iroax::arch::x86_64::qemu::exit_qemu;
use iroax::mem;

#[cfg(not(test))]
fn init(boot_info: &'static BootInfo) -> ! {
    iroax::arch::x86_64::init(&boot_info.memory_map, main);
}

fn main() -> ! {
    let mut heap_lock = mem::HEAP.write();
    let heap = heap_lock.as_mut().unwrap();

    let big_chunk = heap.alloc(0x20000, 8);
    let big_chunk_ptr = big_chunk.as_mut_ptr::<u8>();

    let med_chunk = heap.alloc(0x5000, 8);
    let med_chunk_ptr = med_chunk.as_mut_ptr::<u8>();

    unsafe {
        ::core::ptr::write_bytes(big_chunk_ptr, 0xDE as u8, 0x20000);
    }

    unsafe {
        ::core::ptr::write_bytes(med_chunk_ptr, 0xCE as u8, 0x5000);
    }

    for i in 0..0x20000 {
        assert_eq!(unsafe { big_chunk_ptr.offset(i).read_volatile() }, 0xDE);
    }

    for i in 0..0x5000 {
        assert_eq!(unsafe { med_chunk_ptr.offset(i).read_volatile() }, 0xCE);
    }

    let mut serial_port = SerialPort::new(0x3F8);
    serial_port.init();
    writeln!(serial_port, "ok").unwrap();

    unsafe { exit_qemu(); }
    loop {}
}

#[cfg(not(test))]
entry_point!(init);

#[cfg(not(test))]
#[panic_handler]
fn panic(info: &PanicInfo) -> ! {
    let mut serial_port = SerialPort::new(0x3F8);
    serial_port.init();
    writeln!(serial_port, "{}", info).unwrap();

    unsafe { exit_qemu(); }
    loop {}
}
