use super::stdlib::boxed::Box;
use super::stdlib::collections::BTreeMap;
use super::stdlib::vec::Vec;

pub use self::data::*;
pub use self::result::*;

mod data;
mod result;

pub struct ParserByTagMap<'a, T> {
    map: BTreeMap<&'a [u8], &'a Fn(&mut Data) -> ParseResult<T>>,
    min_prefix_length: usize,
    max_prefix_length: usize,
}

impl<'a, T> ::core::fmt::Debug for ParserByTagMap<'a, T> {
    fn fmt(&self, f: &mut ::core::fmt::Formatter) -> ::core::fmt::Result {
        f.debug_struct("ParserByTagMap")
            .field("min_prefix_length", &self.min_prefix_length)
            .field("max_prefix_length", &self.max_prefix_length)
            .field("known_prefixes", &self.map.keys())
            .finish()
    }
}

unsafe impl<'a, T> Send for ParserByTagMap<'a, T> {}

unsafe impl<'a, T> Sync for ParserByTagMap<'a, T> {}

impl<'a, T> ParserByTagMap<'a, T> {
    pub fn new(items: &[(&'a [u8], &'a Fn(&mut Data) -> ParseResult<T>)]) -> ParserByTagMap<'a, T> {
        let mut b = Self::builder();

        for (tag, parser) in items {
            b = b.add_parser(*tag, *parser);
        }

        b.finish()
    }

    pub fn builder() -> ParserByTagMapBuilder<'a, T> {
        ParserByTagMapBuilder::new()
    }
}

pub struct ParserByTagMapBuilder<'a, T> {
    map: BTreeMap<&'a [u8], &'a Fn(&mut Data) -> ParseResult<T>>,
    min_prefix_length: usize,
    max_prefix_length: usize,
}

impl<'a, T> ParserByTagMapBuilder<'a, T> {
    pub fn new() -> ParserByTagMapBuilder<'a, T> {
        ParserByTagMapBuilder { map: BTreeMap::new(), min_prefix_length: !0, max_prefix_length: 0 }
    }

    pub fn add_parser(mut self, tag: &'a [u8], parser: &'a Fn(&mut Data) -> ParseResult<T>) -> Self {
        self.map.insert(tag, parser);

        self.min_prefix_length = ::core::cmp::min(tag.len(), self.min_prefix_length);
        self.max_prefix_length = ::core::cmp::max(tag.len(), self.max_prefix_length);
        self
    }

    pub fn add_map<M>(mut self, map: &ParserByTagMap<'a, M>, map_fn: &'a Fn(M) -> T) -> Self {
        for (tag, parser) in map.map.iter() {
            let p = *parser;
            let f = Box::new(move |data: &mut Data| {
                Ok(map_fn(p(data)?))
            });

            self = self.add_parser(*tag, Box::leak(f));
        }
        self
    }

    pub fn add_map_ex<M>(mut self, map: &ParserByTagMap<'a, M>, ex_fn: &'a Fn(M, &mut Data) -> ParseResult<T>) -> Self {
        for (tag, parser) in map.map.iter() {
            let p = *parser;
            let f = Box::new(move |data: &mut Data| {
                ex_fn(p(data)?, data)
            });

            self = self.add_parser(tag, Box::leak(f));
        }
        self
    }

    pub fn add_const(self, tag: &'a [u8], value: T) -> Self where T: Clone {
        let f = Box::new(move |data: &mut Data| {
            data.eat_bytes(tag)?;
            Ok(value.clone())
        });

        self.add_parser(tag, Box::leak(f))
    }

    pub fn finish(self) -> ParserByTagMap<'a, T> {
        ParserByTagMap {
            map: self.map,
            max_prefix_length: self.max_prefix_length,
            min_prefix_length: self.min_prefix_length,
        }
    }
}

pub fn parse_tag(data: &mut Data, tag: &[u8]) -> ParseResult<()> {
    data.eat_bytes(tag)
}

pub fn parse_cut<T>(data: &mut Data, limit_parser: &Fn(&mut Data) -> ParseResult<usize>, content_parser: &Fn(&mut Data) -> ParseResult<T>) -> ParseResult<T> {
    let limit = limit_parser(data)?;
    let mut sub_data = data.cut(limit)?;
    content_parser(&mut sub_data)
}

pub fn parse_by_tag<T>(data: &mut Data, map: &ParserByTagMap<T>) -> ParseResult<T> {
    for prefix_len in (map.min_prefix_length..=map.max_prefix_length).rev() {
        let prefix = match data.peek_bytes(prefix_len) {
            Ok(bytes) => bytes,
            Err(_) => { continue; }
        };

        let parser = match map.map.get(prefix) {
            Some(parser) => parser,
            None => { continue; }
        };

        return parser(data);
    }

    data.error(ErrorCode::NoViableAlternative)
}

pub fn parse_all<T>(data: &mut Data, item_parser: &Fn(&mut Data) -> ParseResult<T>) -> ParseResult<Vec<T>> {
    let mut result = vec![];

    while data.len() > 0 {
        result.push(item_parser(data)?);
    }

    Ok(result)
}

pub fn parse_unless<T, P>(data: &mut Data, predicate_parser: &Fn(&mut Data) -> ParseResult<P>, item_parser: &Fn(&mut Data) -> ParseResult<T>) -> ParseResult<(P, Vec<T>)> {
    let mut result = vec![];

    loop {
        match predicate_parser(data) {
            Ok(p) => { return Ok((p, result)); }
            Err(_) => {
                if data.len() == 0 {
                    return data.error(ErrorCode::EndOfStream);
                }
            }
        }

        result.push(item_parser(data)?);
    }
}

pub fn parse_n<T>(data: &mut Data, n: usize, item_parser: &Fn(&mut Data) -> ParseResult<T>) -> ParseResult<Vec<T>> {
    let mut result = vec![];

    for _i in 0..n {
        result.push(item_parser(data)?);
    }

    Ok(result)
}

pub fn parse_times<T>(data: &mut Data, times_parser: &Fn(&mut Data) -> ParseResult<usize>, item_parser: &Fn(&mut Data) -> ParseResult<T>) -> ParseResult<Vec<T>> {
    let times = times_parser(data)?;
    parse_n(data, times, item_parser)
}

pub fn parse_or<T>(data: &mut Data, first: &Fn(&mut Data) -> ParseResult<T>, second: &Fn(&mut Data) -> ParseResult<T>) -> ParseResult<T> {
    let copy = data.clone();

    match first(data) {
        Ok(res) => Ok(res),
        Err(_) => {
            *data = copy;
            second(data)
        }
    }
}
