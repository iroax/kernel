use super::ScalarValue;
use super::super::stdlib::string::String;
use super::super::stdlib::vec::Vec;

#[derive(PartialEq, Debug)]
pub enum ErrorCode {
    NoViableAlternative,
    ExpectedDifferentScalar(ScalarValue, ScalarValue),
    ExpectedSomethingElse,
    NeedMoreBytes(usize),
    EndOfStream,
    ContextRequired,
}

#[derive(PartialEq, Debug)]
pub struct ErrorStackFrame {
    pub parser_name: String,
}

#[derive(PartialEq, Debug)]
pub struct ParseError {
    pub error_code: ErrorCode,
    pub start: usize,
    pub end: usize,
    pub stack: Vec<ErrorStackFrame>,
}

pub type ParseResult<T> = Result<T, ParseError>;
