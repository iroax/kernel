use crate::stdlib::boxed::Box;

use super::result::*;

#[derive(PartialEq, Debug)]
pub enum ScalarValue {
    U8(u8),
    U16(u16),
    U32(u32),
    U64(u64),
}

pub trait Scalar: Sized + PartialEq {
    fn get_num_bytes() -> usize;
    fn from_bytes(bytes: &[u8]) -> Self;

    fn read(src: &mut ByteSource) -> ParseResult<Self> {
        Ok(Self::from_bytes(src.read(Self::get_num_bytes())?))
    }

    fn peek(src: &ByteSource) -> ParseResult<Self> {
        Ok(Self::from_bytes(src.peek(Self::get_num_bytes())?))
    }

    fn eat(src: &mut ByteSource, expected_value: Self) -> ParseResult<()> {
        match Self::peek(src)? {
            ref v if *v == expected_value => {
                Self::read(src)?;
                Ok(())
            }
            v => src.error(ErrorCode::ExpectedDifferentScalar(
                expected_value.into_scalar_value(),
                v.into_scalar_value(),
            ))
        }
    }

    fn into_scalar_value(self) -> ScalarValue;
}

impl Scalar for u8 {
    fn get_num_bytes() -> usize {
        1
    }

    fn from_bytes(bytes: &[u8]) -> u8 {
        bytes[0]
    }

    fn into_scalar_value(self) -> ScalarValue {
        ScalarValue::U8(self)
    }
}

impl Scalar for u16 {
    fn get_num_bytes() -> usize {
        2
    }

    fn from_bytes(bytes: &[u8]) -> u16 {
        (bytes[0] as u16) | (bytes[1] as u16) << 8
    }

    fn into_scalar_value(self) -> ScalarValue {
        ScalarValue::U16(self)
    }
}

impl Scalar for u32 {
    fn get_num_bytes() -> usize {
        4
    }

    fn from_bytes(bytes: &[u8]) -> u32 {
        (bytes[0] as u32) | (bytes[1] as u32) << 8 |
            (bytes[2] as u32) << 16 | (bytes[3] as u32) << 24
    }

    fn into_scalar_value(self) -> ScalarValue {
        ScalarValue::U32(self)
    }
}

impl Scalar for u64 {
    fn get_num_bytes() -> usize {
        8
    }

    fn from_bytes(bytes: &[u8]) -> u64 {
        (bytes[0] as u64) | (bytes[1] as u64) << 8 |
            (bytes[2] as u64) << 16 | (bytes[3] as u64) << 24 |
            (bytes[4] as u64) << 32 | (bytes[5] as u64) << 40 |
            (bytes[6] as u64) << 48 | (bytes[7] as u64) << 56
    }

    fn into_scalar_value(self) -> ScalarValue {
        ScalarValue::U64(self)
    }
}

pub trait ByteSource<'b> {
    fn peek(&self, n: usize) -> ParseResult<&'b [u8]>;
    fn read(&mut self, n: usize) -> ParseResult<&'b [u8]>;
    fn eat(&mut self, bytes: &[u8]) -> ParseResult<()>;
    fn error(&self, code: ErrorCode) -> ParseResult<()>;
}

pub trait ScalarSource<'b, T: Scalar>: ByteSource<'b> + Sized {
    fn peek(&self) -> ParseResult<T> {
        T::peek(self)
    }

    fn read(&mut self) -> ParseResult<T> {
        T::read(self)
    }

    fn eat(&mut self, expected_value: T) -> ParseResult<()> {
        T::eat(self, expected_value)
    }
}

pub trait Downcast: ::core::any::Any {
    fn as_any(&self) -> &::core::any::Any;
}

pub trait Context: Downcast {
    fn debug_fmt(&self, f: &mut ::core::fmt::Formatter) -> ::core::fmt::Result;
    fn clone(&self) -> Box<Context>;
    fn eq(&self, rhs: &Context) -> bool;
}

impl<T: Context> Downcast for T {
    fn as_any(&self) -> &::core::any::Any {
        self
    }
}

struct ContextBox(Box<Context>);

impl PartialEq for ContextBox {
    fn eq(&self, rhs: &Self) -> bool {
        self.0.eq(rhs.0.as_ref())
    }
}

impl ::core::fmt::Debug for ContextBox {
    fn fmt(&self, f: &mut ::core::fmt::Formatter) -> ::core::fmt::Result {
        self.0.debug_fmt(f)
    }
}

impl Clone for ContextBox {
    fn clone(&self) -> ContextBox {
        ContextBox(self.0.clone())
    }
}

#[derive(PartialEq, Debug, Clone)]
pub struct Data<'b> {
    bytes: &'b [u8],
    start: usize,
    end: usize,
    context: Option<ContextBox>,
}

impl<'b> Data<'b> {
    fn _new(bytes: &'b [u8], context: Option<ContextBox>) -> Data {
        Data { bytes, start: 0, end: bytes.len(), context }
    }

    pub fn new(bytes: &'b [u8]) -> Data {
        Self::_new(bytes, None)
    }

    pub fn new_with_context(bytes: &'b [u8], context: Box<Context>) -> Data {
        Self::_new(bytes, Some(ContextBox(context)))
    }

    pub fn get_context(&self) -> Option<&Context> {
        self.context.as_ref().map(|context_box| context_box.0.as_ref())
    }

    pub fn len(&self) -> usize {
        self.end - self.start
    }

    pub fn check_len(&self, n: usize) -> ParseResult<()> {
        match self.start + n <= self.end {
            true => Ok(()),
            false => self.error(ErrorCode::NeedMoreBytes(n - (self.end - self.start))),
        }
    }

    fn _cut(&mut self, n: usize, context: Option<ContextBox>) -> ParseResult<Data<'b>> {
        self.check_len(n)?;

        let start = self.start;
        self.start += n;

        Ok(Data {
            bytes: self.bytes,
            start,
            end: start + n,
            context,
        })
    }

    pub fn cut(&mut self, n: usize) -> ParseResult<Data<'b>> {
        self._cut(n, self.context.clone())
    }

    pub fn cut_with_context(&mut self, n: usize, context: Box<Context>) -> ParseResult<Data<'b>> {
        self._cut(n, Some(ContextBox(context)))
    }

    pub fn error<T>(&self, error_code: ErrorCode) -> ParseResult<T> {
        Err(ParseError {
            start: self.start,
            end: self.end,
            error_code,
            stack: vec![],
        })
    }

    pub fn peek_bytes(&self, n: usize) -> ParseResult<&'b [u8]> where Self: ByteSource<'b> {
        ByteSource::peek(self, n)
    }

    pub fn read_bytes(&mut self, n: usize) -> ParseResult<&'b [u8]> where Self: ByteSource<'b> {
        ByteSource::read(self, n)
    }

    pub fn eat_bytes(&mut self, bytes: &[u8]) -> ParseResult<()> where Self: ByteSource<'b> {
        ByteSource::eat(self, bytes)
    }

    pub fn peek_scalar<T: Scalar>(&self) -> ParseResult<T> where Self: ScalarSource<'b, T> {
        ScalarSource::<T>::peek(self)
    }

    pub fn read_scalar<T: Scalar>(&mut self) -> ParseResult<T> where Self: ScalarSource<'b, T> {
        ScalarSource::<T>::read(self)
    }

    pub fn eat_scalar<T: Scalar>(&mut self, expected_value: T) -> ParseResult<()> where Self: ScalarSource<'b, T> {
        ScalarSource::<T>::eat(self, expected_value)
    }
}

impl<'b> ByteSource<'b> for Data<'b> {
    fn peek(&self, n: usize) -> ParseResult<&'b [u8]> {
        self.check_len(n)?;
        Ok(&self.bytes[self.start..(self.start + n)])
    }

    fn read(&mut self, n: usize) -> ParseResult<&'b [u8]> {
        self.check_len(n)?;

        let start = self.start;
        self.start += n;

        Ok(&self.bytes[start..(start + n)])
    }

    fn eat(&mut self, bytes: &[u8]) -> ParseResult<()> {
        self.check_len(bytes.len())?;

        let prefix = &self.bytes[self.start..(self.start + bytes.len())];

        if *prefix == *bytes {
            self.start += bytes.len();
            Ok(())
        } else {
            self.error(ErrorCode::ExpectedSomethingElse)
        }
    }

    fn error(&self, code: ErrorCode) -> ParseResult<()> {
        self.error::<()>(code)
    }
}

impl<'b> ScalarSource<'b, u8> for Data<'b> {}

impl<'b> ScalarSource<'b, u16> for Data<'b> {}

impl<'b> ScalarSource<'b, u32> for Data<'b> {}

impl<'b> ScalarSource<'b, u64> for Data<'b> {}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_new() {
        let d1 = Data::new(&[1][..]);
        let d2 = Data::new(&[2, 3][..]);
        let d3 = Data::new(&[][..]);

        assert_eq!(d1, Data { context: None, bytes: &[1][..], start: 0, end: 1 });
        assert_eq!(d2, Data { context: None, bytes: &[2, 3][..], start: 0, end: 2 });
        assert_eq!(d3, Data { context: None, bytes: &[][..], start: 0, end: 0 });
    }

    #[test]
    fn test_error() {
        let d1 = Data::new(&[1][..]);
        let d2 = Data::new(&[2, 3][..]);

        assert_eq!(d1.error::<()>(ErrorCode::NoViableAlternative), Err(ParseError {
            error_code: ErrorCode::NoViableAlternative,
            start: 0,
            end: 1,
            stack: vec![],
        }));
        assert_eq!(d2.error::<()>(ErrorCode::NeedMoreBytes(2)), Err(ParseError {
            error_code: ErrorCode::NeedMoreBytes(2),
            start: 0,
            end: 2,
            stack: vec![],
        }));
    }

    #[test]
    fn test_check_len() {
        let d1 = Data::new(&[1][..]);
        let d2 = Data::new(&[2, 3][..]);
        let d3 = Data { context: None, bytes: &[2, 3, 4, 5, 6][..], start: 2, end: 5 };

        assert_eq!(d1.check_len(1), Ok(()));
        assert_eq!(d1.check_len(2), Err(ParseError {
            error_code: ErrorCode::NeedMoreBytes(1),
            start: 0,
            end: 1,
            stack: vec![],
        }));

        assert_eq!(d2.check_len(2), Ok(()));
        assert_eq!(d2.check_len(5), Err(ParseError {
            error_code: ErrorCode::NeedMoreBytes(3),
            start: 0,
            end: 2,
            stack: vec![],
        }));

        assert_eq!(d3.check_len(2), Ok(()));
        assert_eq!(d3.check_len(5), Err(ParseError {
            error_code: ErrorCode::NeedMoreBytes(2),
            start: 2,
            end: 5,
            stack: vec![],
        }));
    }

    #[test]
    fn test_len() {
        let d1 = Data::new(&[1][..]);
        let d2 = Data::new(&[2, 3][..]);
        let d3 = Data { context: None, bytes: &[2, 3, 4, 5, 6][..], start: 2, end: 5 };

        assert_eq!(d1.len(), 1);
        assert_eq!(d2.len(), 2);
        assert_eq!(d3.len(), 3);
    }

    #[test]
    fn test_peek_u8() {
        let d1 = Data::new(&[1][..]);
        let d2 = Data::new(&[2, 3][..]);
        let d3 = Data::new(&[][..]);

        assert_eq!(d1.peek_scalar::<u8>(), Ok(1));
        assert_eq!(d1, Data { context: None, bytes: &[1][..], start: 0, end: 1 });

        assert_eq!(d2.peek_scalar::<u8>(), Ok(2));
        assert_eq!(d2, Data { context: None, bytes: &[2, 3][..], start: 0, end: 2 });

        assert_eq!(d3.peek_scalar::<u8>(), Err(ParseError {
            error_code: ErrorCode::NeedMoreBytes(1),
            start: 0,
            end: 0,
            stack: vec![],
        }));
        assert_eq!(d3, Data { context: None, bytes: &[][..], start: 0, end: 0 });
    }

    #[test]
    fn test_eat_u8() {
        let mut d1 = Data::new(&[1][..]);
        let mut d2 = Data::new(&[2, 3][..]);
        let mut d3 = Data::new(&[][..]);

        assert_eq!(d1.eat_scalar::<u8>(1), Ok(()));
        assert_eq!(d1, Data { context: None, bytes: &[1][..], start: 1, end: 1 });

        assert_eq!(d2.eat_scalar::<u8>(2), Ok(()));
        assert_eq!(d2, Data { context: None, bytes: &[2, 3][..], start: 1, end: 2 });

        assert_eq!(d2.eat_scalar::<u8>(2), Err(ParseError {
            error_code: ErrorCode::ExpectedDifferentScalar(ScalarValue::U8(2), ScalarValue::U8(3)),
            start: 1,
            end: 2,
            stack: vec![],
        }));
        assert_eq!(d2, Data { context: None, bytes: &[2, 3][..], start: 1, end: 2 });

        assert_eq!(d3.eat_scalar::<u8>(3), Err(ParseError {
            error_code: ErrorCode::NeedMoreBytes(1),
            start: 0,
            end: 0,
            stack: vec![],
        }));
        assert_eq!(d3, Data { context: None, bytes: &[][..], start: 0, end: 0 });
    }
}
