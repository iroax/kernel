use bit_field::BitField;

const PAGES_PER_ITEM: usize = core::mem::size_of::<u64>() * 8;

pub struct Bitmap<'a> {
    storage: &'a mut [u64],
}

impl<'a> Bitmap<'a> {
    pub fn new(storage: &'a mut [u64]) -> Bitmap {
        return Bitmap { storage };
    }

    fn is_set(&self, index: usize) -> bool {
        let entry = self.storage[index / PAGES_PER_ITEM];
        return entry.get_bit(index % PAGES_PER_ITEM);
    }

    fn set(&mut self, index: usize, val: bool) {
        let entry = &mut self.storage[index / PAGES_PER_ITEM];
        let bit_index = index % PAGES_PER_ITEM;

        if entry.get_bit(bit_index) == val {
            return;
        }

        entry.set_bit(bit_index, val);
    }

    pub fn mark_used(&mut self, index: usize) {
        self.set(index, true);
    }

    pub fn mark_free(&mut self, index: usize) {
        self.set(index, false);
    }

    pub fn find_free(&self, number: usize) -> Option<usize> {
        let total_pages = self.storage.len() as usize * PAGES_PER_ITEM;
        let mut current_page = 0;

        while current_page < total_pages {
            // find number of available pages
            let mut pages_available = 0;
            while current_page + pages_available < total_pages
                && pages_available <= number
                && !self.is_set(current_page + pages_available) {
                pages_available += 1;
            }

            // if enough pages to fulfil request then done
            if pages_available >= number {
                return Some(current_page);
            }

            // else skip available pages and one unavailable
            current_page += pages_available + 1;
        }

        return None;
    }

    pub fn borrow_storage(&self) -> &[u64] {
        self.storage
    }

    pub fn borrow_storage_mut(&mut self) -> &mut [u64] {
        self.storage
    }
}

impl<'a> ::core::fmt::Debug for Bitmap<'a> {
    fn fmt(&self, f: &mut ::core::fmt::Formatter) -> ::core::fmt::Result {
        let total_pages = self.storage.len() as usize * PAGES_PER_ITEM;
        let mut from = 0;
        let mut is_free = !self.is_set(0);
        let mut current = 0;

        while current < total_pages {
            current += 1;

            if current == total_pages || is_free == self.is_set(current) {
                writeln!(f, "0x{:x} - 0x{:x}: {}", from * 0x1000, current * 0x1000, if is_free { "free" } else { "used" })?;

                from = current;
                is_free = !is_free;
            }
        }

        Ok(())
    }
}

#[cfg(test)]
mod test {
    use super::Bitmap;

    #[test]
    fn test_mark_used() {
        let mut storage = [0 as u64; 1];
        let mut bitmap = Bitmap::new(&mut storage);

        bitmap.mark_used(2);
        bitmap.mark_used(22);

        assert_eq!(storage[0], 0x400004);
    }

    #[test]
    fn test_mark_free() {
        let mut storage = [0x400004 as u64; 1];
        let mut bitmap = Bitmap::new(&mut storage);

        bitmap.mark_free(2);

        assert_eq!(storage[0], 0x400000);
    }

    #[test]
    fn test_find_free() {
        let mut storage = [0x400004, 0];
        let bitmap = Bitmap::new(&mut storage);

        assert_eq!(bitmap.find_free(2), Some(0));
        assert_eq!(bitmap.find_free(8), Some(3));
        assert_eq!(bitmap.find_free(32), Some(23));
        assert_eq!(bitmap.find_free(42), Some(23));
        assert_eq!(bitmap.find_free(64), Some(23));
        assert_eq!(bitmap.find_free(105), Some(23));
        assert_eq!(bitmap.find_free(106), None);
    }

    #[test]
    fn test_find_free_and_mark_used() {
        let mut storage = [0x400004, 0];
        let mut bitmap = Bitmap::new(&mut storage);

        let index = bitmap.find_free(105).expect("should find");
        for i in index..(index + 105) {
            bitmap.mark_used(i);
        }

        assert_eq!(storage[0], 0xFFFFFFFFFFC00004);
        assert_eq!(storage[1], 0xFFFFFFFFFFFFFFFF);
    }
}
