pub struct LinearAllocator<T> {
    next: T,
}

impl <T> LinearAllocator<T> where T: core::ops::Add<usize, Output=T> + Copy {
    pub fn new(start: T) -> LinearAllocator<T> {
        LinearAllocator {
            next: start,
        }
    }

    pub fn alloc(&mut self, count: usize) -> Option<T> {
        let result = self.next;
        self.next = self.next + count;
        Some(result)
    }

    pub fn dealloc(&mut self, _obj: T, _count: usize) {
        // noop
    }
}
