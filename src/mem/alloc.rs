use core::alloc::{GlobalAlloc, Layout};
use super::{alloc_heap, dealloc_heap};
use super::addr::VirtAddr;

pub struct GlobalAllocator(u8);

impl GlobalAllocator {
    pub const fn new() -> GlobalAllocator {
        GlobalAllocator(0)
    }
}

unsafe impl GlobalAlloc for GlobalAllocator {
    unsafe fn alloc(&self, layout: Layout) -> *mut u8 {
        alloc_heap(layout.size(), layout.align()).as_mut_ptr::<u8>()
    }

    unsafe fn dealloc(&self, ptr: *mut u8, _layout: Layout) {
        dealloc_heap(VirtAddr::from_ptr(ptr))
    }
}

#[cfg(not(test))]
#[alloc_error_handler]
fn handle_alloc_error(layout: Layout) -> ! {
    panic!("alloc error {:?}", layout);
}
