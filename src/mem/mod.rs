use bootloader_precompiled::bootinfo::{MemoryMap, MemoryRegionType};
use self::addr::{PagingRoot, PhysAddr, VirtAddr};
use spin::RwLock;

pub mod alloc;
pub mod addr;
mod bitmap;
mod heap;
mod linear_allocator;

#[derive(Copy, Clone)]
pub struct PagingImpl {
    pub get_current_paging_root: fn() -> PagingRoot,
    pub set_current_paging_root: unsafe fn(PagingRoot),
    pub map_pages: fn(PagingRoot, PhysAddr, VirtAddr, usize, bool),
    pub unmap_pages: fn(PagingRoot, VirtAddr, usize),
    pub virt_to_phys: fn(PagingRoot, VirtAddr) -> Option<PhysAddr>,
    pub phys_to_virt: fn(PagingRoot, PhysAddr) -> Option<VirtAddr>,
}

// enough memory to map 128MiB of phys memory
const STAGE_1_BITMAP_SIZE: usize = 512;

// current paging implementation
static PAGING: RwLock<Option<PagingImpl>> = RwLock::new(None);

// current physical memory bitmap
static PHYS_BITMAP: RwLock<Option<bitmap::Bitmap>> = RwLock::new(None);

// current virtual memory allocator
static VIRT_ALLOCATOR: RwLock<Option<linear_allocator::LinearAllocator<VirtAddr>>> = RwLock::new(None);

// current heap
pub static HEAP: RwLock<Option<heap::Heap>> = RwLock::new(None);

pub fn set_paging_impl(paging_impl: PagingImpl) {
    *PAGING.write() = Some(paging_impl);
}

pub fn get_paging_impl() -> PagingImpl {
    PAGING.read().expect("paging implementation not set")
}

pub fn alloc_phys(size: usize) -> Option<PhysAddr> {
    let mut bitmap_lock = PHYS_BITMAP.write();
    let bitmap_mut: &mut bitmap::Bitmap = bitmap_lock.as_mut()
        .expect("phys memory bitmap not set");

    let maybe_frame_index = bitmap_mut.find_free(size);

    if maybe_frame_index.is_none() {
        return None;
    }

    let frame_index = maybe_frame_index.unwrap();

    for i in 0..size {
        bitmap_mut.mark_used(frame_index + i);
    }

    return Some(PhysAddr::new(frame_index as u64 * 0x1000));
}

pub fn dealloc_phys(addr: PhysAddr, size: usize) {
    let frame_index = addr.as_u64() / 0x1000;
    let mut bitmap_lock = PHYS_BITMAP.write();
    let bitmap_mut: &mut bitmap::Bitmap = bitmap_lock.as_mut()
        .expect("phys memory bitmap not set");

    for i in 0..size {
        bitmap_mut.mark_free(frame_index as usize + i);
    }
}

pub fn alloc_virt(size: usize) -> Option<VirtAddr> {
    let mut allocator_lock = VIRT_ALLOCATOR.write();
    let allocator: &mut linear_allocator::LinearAllocator<VirtAddr> = allocator_lock.as_mut()
        .expect("virtual memory allocator not set");

    allocator.alloc(size * 0x1000)
}

pub fn dealloc_virt(addr: VirtAddr, size: usize) {
    let mut allocator_lock = VIRT_ALLOCATOR.write();
    let allocator: &mut linear_allocator::LinearAllocator<VirtAddr> = allocator_lock.as_mut()
        .expect("virtual memory allocator not set");

    allocator.dealloc(addr, size)
}

pub fn alloc_pages(size: usize) -> Option<VirtAddr> {
    let maybe_virt_addr = alloc_virt(size);

    if maybe_virt_addr.is_none() {
        return None;
    }

    let virt_addr = maybe_virt_addr.unwrap();
    let paging = get_paging_impl();
    let get_current_paging_root = paging.get_current_paging_root;
    let paging_root = get_current_paging_root();
    let map_pages = paging.map_pages;

    let mut pages_mapped = 0;
    let mut attempt_size = size;
    while pages_mapped < size {
        let maybe_phys_addr = alloc_phys(attempt_size);

        if maybe_phys_addr.is_none() {
            if attempt_size == 1 {
                unimplemented!("roll back alloc_pages allocated frames on error");
            }

            attempt_size = ::core::cmp::max(1, attempt_size / 2);
            continue;
        }

        let phys_addr = maybe_phys_addr.unwrap();
        map_pages(paging_root, phys_addr, virt_addr + (pages_mapped * 0x1000), attempt_size, true);

        pages_mapped += attempt_size;
    }

    return Some(virt_addr);
}

pub fn dealloc_pages(_addr: VirtAddr, _size: usize) {
    // TODO
}

pub fn alloc_heap(bytes: usize, align: usize) -> VirtAddr {
    let mut heap_lock = HEAP.write();
    let heap: &mut heap::Heap = heap_lock.as_mut()
        .expect("heap not set");

    heap.alloc(bytes, align)
}

pub fn dealloc_heap(addr: VirtAddr) {
    let mut heap_lock = HEAP.write();
    let heap: &mut heap::Heap = heap_lock.as_mut()
        .expect("heap not set");

    heap.dealloc(addr)
}

pub fn map_pages(phys_addr: PhysAddr, size: usize, writable: bool) -> Option<VirtAddr> {
    let paging_impl = get_paging_impl();
    let virt_addr = alloc_virt(size)?;

    (paging_impl.map_pages)((paging_impl.get_current_paging_root)(), phys_addr, virt_addr, size, writable);

    Some(virt_addr)
}

pub fn unmap_pages(virt_addr: VirtAddr, size: usize) {
    let paging_impl = get_paging_impl();
    let get_current_paging_root = paging_impl.get_current_paging_root;
    let unmap_pages = paging_impl.unmap_pages;

    unmap_pages(get_current_paging_root(), virt_addr, size);

    dealloc_virt(virt_addr, size);
}

pub fn init(memory_map: &MemoryMap) -> PhysAddr {
    let get_current_paging_root = get_paging_impl().get_current_paging_root;
    let paging_root = get_current_paging_root();

    // stage 1: create fixed size bitmap, map first 128MiBs of memory, allocate pages for stage 2
    //          allocate pages for bitmap to cover entire memory, map entire memory
    // stage 2: use dynamic size bitmap

    // start allocating virtual memory at 0x10000000
    let virt_memory_allocator = linear_allocator::LinearAllocator::new(VirtAddr::new(0x10000000));
    *VIRT_ALLOCATOR.write() = Some(virt_memory_allocator);

    // stage 1
    const STAGE_1_BITMAP_LIMIT: u64 = (STAGE_1_BITMAP_SIZE * 64 * 0x1000) as u64;
    let stage1_storage = {
        static mut STORAGE: [u64; STAGE_1_BITMAP_SIZE] = [!0; STAGE_1_BITMAP_SIZE];
        unsafe { &mut STORAGE }
    };
    let mut stage1_bitmap = bitmap::Bitmap::new(stage1_storage);
    let mut last_usable_frame = 0;

    // fill stage 1 bitmap from memory map
    for memory_region in memory_map.iter() {
        if memory_region.region_type != MemoryRegionType::Usable && memory_region.region_type != MemoryRegionType::Bootloader {
            continue;
        }

        if last_usable_frame < memory_region.range.end_addr() - 0x1000 {
            last_usable_frame = memory_region.range.end_addr() - 0x1000;
        }

        if memory_region.range.start_addr() >= STAGE_1_BITMAP_LIMIT {
            continue;
        }

        for frame_addr in (memory_region.range.start_addr()..memory_region.range.end_addr()).step_by(0x1000) {
            if frame_addr >= STAGE_1_BITMAP_LIMIT {
                break;
            }

            stage1_bitmap.mark_free((frame_addr / 0x1000) as usize);
        }
    }

    *PHYS_BITMAP.write() = Some(stage1_bitmap);

    // reserve page in low range
    let reserved_page_addr = alloc_phys(1)
        .expect("could not reserve page in low range");

    let bitmap_storage_size = (((last_usable_frame / 0x1000) / 64) + 1) as usize;
    let bitmap_storage_size_pages = (bitmap_storage_size * 8 + 0x1000 - 1) / 0x1000;
    let map_pages = get_paging_impl().map_pages;

    // to allocate dynamic count of pages, we need to
    // 1. find free pages in bitmap
    // 2. allocate continuous virtual address space
    // 3. map free individual pages to continuous virtual address space
    let bitmap_storage_virt_addr = alloc_virt(bitmap_storage_size_pages)
        .expect("out of virtual memory");

    for i in 0..bitmap_storage_size_pages {
        let phys_addr = alloc_phys(1)
            .expect("out of memory");
        let virt_addr = bitmap_storage_virt_addr + (i * 0x1000) as u64;

        map_pages(paging_root, phys_addr, virt_addr, 1, true);
    }

    let bitmap_storage = unsafe {
        ::core::slice::from_raw_parts_mut::<'static, u64>(bitmap_storage_virt_addr.as_mut_ptr::<u64>(), bitmap_storage_size)
    };

    // mark everything as used
    for i in 0..bitmap_storage_size {
        unsafe { ::core::ptr::write_volatile(&mut bitmap_storage[i], !0); }
    }

    let mut stage2_bitmap = bitmap::Bitmap::new(bitmap_storage);

    // fill stage 2 bitmap from memory map
    for memory_region in memory_map.iter() {
        if memory_region.region_type != MemoryRegionType::Usable {
            continue;
        }

        for frame_addr in (memory_region.range.start_addr()..memory_region.range.end_addr()).step_by(0x1000) {
            stage2_bitmap.mark_free((frame_addr / 0x1000) as usize);
        }
    }

    // copy stage 1 bitmap over to stage 2 bitmap to persist changes already made
    // and switch to stage 2 bitmap
    {
        let mut lock = PHYS_BITMAP.write();
        let stage1_bitmap: &mut bitmap::Bitmap = lock.as_mut().unwrap();
        let stage1_storage = stage1_bitmap.borrow_storage();
        let stage2_storage = stage2_bitmap.borrow_storage_mut();

        for index in 0..::core::cmp::min(stage1_storage.len(), stage2_storage.len()) {
            unsafe {
                ::core::ptr::write_volatile(&mut stage2_storage[index], stage1_storage[index]);
            }
        }

        *lock = Some(stage2_bitmap);
    }

    // initialize heap
    *HEAP.write() = Some(heap::Heap::new());

    // return address of reserved page
    reserved_page_addr
}
