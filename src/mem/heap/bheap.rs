use core::ptr::{copy, copy_nonoverlapping, NonNull};
use spin::RwLock;

use crate::mem::addr::VirtAddr;

#[derive(PartialEq)]
struct Node<K, V> {
    key: K,
    val: V,
}

impl<K, V> ::core::fmt::Debug for Node<K, V> where K: ::core::fmt::Debug, V: ::core::fmt::Debug {
    fn fmt(&self, f: &mut ::core::fmt::Formatter) -> ::core::fmt::Result {
        f.debug_struct("Node")
            .field("key", &self.key)
            .field("val", &self.val)
            .finish()
    }
}

pub struct BHeap<K, V> {
    alloc_pages: &'static Fn(usize) -> Option<VirtAddr>,
    dealloc_pages: &'static Fn(VirtAddr, usize),
    buf: RwLock<Option<NonNull<Node<K, V>>>>,
    len: usize,
    cap: usize,
}

unsafe impl<K, V> Sync for BHeap<K, V> where K: Sync, V: Sync {}
unsafe impl<K, V> Send for BHeap<K, V> where K: Send, V: Send {}

impl<K, V> PartialEq for BHeap<K, V> {
    fn eq(&self, rhs: &Self) -> bool {
        self.alloc_pages as *const _ == rhs.alloc_pages as *const _ &&
            self.dealloc_pages as *const _ == rhs.dealloc_pages as *const _ &&
            *self.buf.read() == *rhs.buf.read() &&
            self.len == rhs.len &&
            self.cap == rhs.cap
    }
}

impl<K, V> ::core::fmt::Debug for BHeap<K, V> where K: ::core::fmt::Debug + PartialOrd, V: ::core::fmt::Debug {
    fn fmt(&self, f: &mut ::core::fmt::Formatter) -> ::core::fmt::Result {
        f.debug_struct("BHeap")
            .field("alloc_pages", &VirtAddr::from_ptr(&self.alloc_pages))
            .field("dealloc_pages", &VirtAddr::from_ptr(&self.dealloc_pages))
            .field("buf", &self.buf)
            .field("len", &self.len)
            .field("cap", &self.cap)
            .field("items", &self.iter())
            .finish()
    }
}

impl<K, V> BHeap<K, V> where K: PartialOrd {
    pub fn new(alloc_pages: &'static Fn(usize) -> Option<VirtAddr>, dealloc_pages: &'static Fn(VirtAddr, usize)) -> BHeap<K, V> {
        assert!(::core::mem::size_of::<K>() + ::core::mem::size_of::<V>() > 0);

        BHeap {
            alloc_pages,
            dealloc_pages,
            buf: RwLock::new(None),
            len: 0,
            cap: 0,
        }
    }

    fn grow(&mut self) {
        let node_size = ::core::mem::size_of::<Node<K, V>>();
        let old_pages = (self.cap * node_size + 0xFFF) / 0x1000;
        let new_pages = if old_pages == 0 { 1 } else { old_pages * 2 };
        let new_cap = new_pages * 0x1000 / node_size;
        let new_buf = (self.alloc_pages)(new_pages).expect("could not allocate memory for heap");
        let mut buf_lock = self.buf.write();

        if self.len != 0 {
            unsafe {
                copy_nonoverlapping(buf_lock.unwrap().as_ptr(), new_buf.as_mut_ptr(), self.len);
            }
        }

        if self.cap != 0 {
            (self.dealloc_pages)(VirtAddr::from_ptr(buf_lock.unwrap().as_ptr()), old_pages);
        }

        *buf_lock = NonNull::new(new_buf.as_mut_ptr());
        self.cap = new_cap;
    }

    fn binary_search_side(&self, key: &K, right: bool) -> Option<usize> {
        if self.cap == 0 {
            return None;
        }

        if self.len == 0 {
            return Some(0);
        }

        let buf_lock = self.buf.read();
        let buf = buf_lock.unwrap().as_ptr();
        let mut low = 0;
        let mut high = self.len - 1;

        while low <= high {
            let mid = (low + high) / 2;
            let mid_key = unsafe { &buf.offset(mid as isize).as_ref().unwrap().key };

            if right && mid_key > key || !right && mid_key >= key {
                if mid == 0 {
                    break;
                }

                high = mid - 1;
            } else {
                low = mid + 1;
            }
        }

        Some(low)
    }

    pub fn insert(&mut self, key: K, val: V) {
        if self.len == self.cap {
            self.grow();
        }

        let offset = self.binary_search_side(&key, true).unwrap() as isize;
        let buf_lock = self.buf.read();
        let buf = buf_lock.unwrap().as_ptr();

        if (offset as usize) < self.len {
            unsafe {
                copy(buf.offset(offset), buf.offset(offset + 1), self.len - offset as usize);
            }
        }

        unsafe {
            ::core::ptr::write_volatile(buf.offset(offset), Node { key, val });
        }

        self.len += 1;
    }

    fn binary_search(&self, key: &K) -> Option<usize> {
        if self.len == 0 {
            return None;
        }

        let buf_lock = self.buf.read();
        let buf = buf_lock.unwrap().as_ptr();
        let mut left = 0;
        let mut right = self.len - 1;

        while left <= right {
            let mid = (right - left) / 2 + left;
            let mid_key = unsafe { &buf.offset(mid as isize).as_ref().unwrap().key };

            if key < mid_key {
                if mid == 0 {
                    break;
                }

                right = mid - 1;
            } else if key > mid_key {
                left = mid + 1;
            } else {
                return Some(mid);
            }
        }

        None
    }

    #[cfg(test)]
    fn includes(&self, key: &K) -> bool {
        self.binary_search(key).is_some()
    }

    fn delete_by_offset(&mut self, offset: usize) {
        use ::core::mem::{needs_drop, drop};

        let buf_lock = self.buf.read();
        let buf = buf_lock.unwrap().as_ptr();

        if needs_drop::<Node<K, V>>() {
            let node = unsafe { buf.offset(offset as isize).read_volatile() };
            drop(node);
        }

        self.len -= 1;

        if offset < self.len {
            unsafe {
                copy(buf.offset((offset + 1) as isize), buf.offset(offset as isize), self.len - offset as usize);
            }
        }
    }

    pub fn delete(&mut self, key: &K) {
        while let Some(offset) = self.binary_search(&key) {
            self.delete_by_offset(offset);
        }
    }

    pub fn get(&self, key: &K) -> Option<&V> {
        self.binary_search(key).map(|offset| unsafe { &self.node_at(offset).val })
    }

    unsafe fn node_at(&self, count: usize) -> &Node<K, V> {
        let buf_lock = self.buf.read();
        let buf = buf_lock.unwrap().as_ptr();
        buf.offset(count as isize).as_ref().unwrap()
    }

    pub fn iter(&self) -> Iter<K, V> {
        Iter { heap: self, offset: 0, end: self.len }
    }

    pub fn iter_from(&self, key: &K) -> Iter<K, V> {
        let start = self.binary_search_side(key, false).unwrap_or(self.len);
        Iter { heap: self, offset: start, end: self.len }
    }

    pub fn iter_to(&self, key: &K) -> Iter<K, V> {
        let end = self.binary_search_side(key, true).unwrap_or(self.len);
        Iter { heap: self, offset: 0, end }
    }
}

pub struct Iter<'a, K, V> where K: PartialOrd {
    heap: &'a BHeap<K, V>,
    offset: usize,
    end: usize,
}

impl<'a, K, V> Iterator for Iter<'a, K, V> where K: PartialOrd {
    type Item = (&'a K, &'a V);

    fn next(&mut self) -> Option<(&'a K, &'a V)> {
        if self.offset >= self.end {
            None
        } else {
            let node = unsafe { self.heap.node_at(self.offset) };
            self.offset += 1;
            Some((&node.key, &node.val))
        }
    }
}

impl<'a, K, V> DoubleEndedIterator for Iter<'a, K, V> where K: PartialOrd {
    fn next_back(&mut self) -> Option<(&'a K, &'a V)> {
        if self.offset >= self.end {
            None
        } else {
            let node = unsafe { self.heap.node_at(self.end - 1) };
            self.end -= 1;
            Some((&node.key, &node.val))
        }
    }
}

impl<'a, K, V> ::core::fmt::Debug for Iter<'a, K, V> where K: ::core::fmt::Debug + PartialOrd, V: ::core::fmt::Debug {
    fn fmt(&self, f: &mut ::core::fmt::Formatter) -> ::core::fmt::Result {
        match *self.heap.buf.read() {
            None => f.debug_map().finish(),
            Some(buf) => {
                let entries = unsafe { ::core::slice::from_raw_parts(buf.as_ptr(), self.heap.len) }.into_iter()
                    .map(|node| (&node.key, &node.val));

                f.debug_map().entries(entries).finish()
            }
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::test_utils::{alloc_pages, dealloc_pages};

    #[test]
    #[should_panic]
    fn test_asserts_on_zero_size_items() {
        BHeap::<(), ()>::new(&alloc_pages, &dealloc_pages);
    }

    #[test]
    fn test_new() {
        let alloc = &alloc_pages;
        let dealloc = &dealloc_pages;

        let heap = BHeap::<usize, ()>::new(alloc, dealloc);

        assert_eq!(heap, BHeap {
            alloc_pages: alloc,
            dealloc_pages: dealloc,
            buf: RwLock::new(None),
            len: 0,
            cap: 0,
        });
    }

    #[test]
    fn test_insert() {
        let mut heap = BHeap::new(&alloc_pages, &dealloc_pages);
        heap.insert(5u32, "hello");

        assert_eq!(heap.cap, 170);
        assert_eq!(heap.len, 1);
        assert!(heap.buf.read().is_some());
        assert_eq!(unsafe { heap.buf.read().as_ref().unwrap().as_ref() }, &Node { key: 5, val: "hello" });
    }

    #[test]
    fn test_insert_multiple() {
        let mut heap = BHeap::new(&alloc_pages, &dealloc_pages);
        heap.insert(5u32, "how");
        heap.insert(3u32, "hello");
        heap.insert(6u32, "are");
        heap.insert(7u32, "you");
        heap.insert(4u32, "world");

        assert_eq!(heap.cap, 170);
        assert_eq!(heap.len, 5);

        let buf = heap.buf.read();
        assert!(buf.is_some());
        assert_eq!(unsafe { buf.unwrap().as_ptr().offset(0).read_volatile() }, Node { key: 3, val: "hello" });
        assert_eq!(unsafe { buf.unwrap().as_ptr().offset(1).read_volatile() }, Node { key: 4, val: "world" });
        assert_eq!(unsafe { buf.unwrap().as_ptr().offset(2).read_volatile() }, Node { key: 5, val: "how" });
        assert_eq!(unsafe { buf.unwrap().as_ptr().offset(3).read_volatile() }, Node { key: 6, val: "are" });
        assert_eq!(unsafe { buf.unwrap().as_ptr().offset(4).read_volatile() }, Node { key: 7, val: "you" });
    }

    #[test]
    fn test_insert_same_key() {
        let mut heap = BHeap::new(&alloc_pages, &dealloc_pages);
        heap.insert(5u32, "how");
        heap.insert(3u32, "hello");
        heap.insert(5u32, "are");
        heap.insert(5u32, "you");
        heap.insert(4u32, "world");

        assert_eq!(heap.cap, 170);
        assert_eq!(heap.len, 5);

        let buf = heap.buf.read();
        assert!(buf.is_some());
        assert_eq!(unsafe { buf.unwrap().as_ptr().offset(0).read_volatile() }, Node { key: 3, val: "hello" });
        assert_eq!(unsafe { buf.unwrap().as_ptr().offset(1).read_volatile() }, Node { key: 4, val: "world" });
        assert_eq!(unsafe { buf.unwrap().as_ptr().offset(2).read_volatile() }, Node { key: 5, val: "how" });
        assert_eq!(unsafe { buf.unwrap().as_ptr().offset(3).read_volatile() }, Node { key: 5, val: "are" });
        assert_eq!(unsafe { buf.unwrap().as_ptr().offset(4).read_volatile() }, Node { key: 5, val: "you" });
    }

    #[test]
    fn test_grow() {
        let mut heap = BHeap::new(&alloc_pages, &dealloc_pages);

        for i in 0..1000 {
            heap.insert(i, "v");
        }

        assert_eq!(heap.cap, 1365);
        assert_eq!(heap.len, 1000);

        let mut i = 1000;

        for (k, _) in heap.iter().rev() {
            i -= 1;
            assert_eq!(*k, i);
        }

        assert_eq!(i, 0);
    }

    #[test]
    fn test_includes() {
        let mut heap = BHeap::new(&alloc_pages, &dealloc_pages);
        heap.insert(5u32, "how");
        heap.insert(3u32, "hello");
        heap.insert(6u32, "are");
        heap.insert(7u32, "you");
        heap.insert(4u32, "world");

        assert!(!heap.includes(&2));
        assert!(heap.includes(&3));
        assert!(heap.includes(&4));
        assert!(heap.includes(&5));
        assert!(heap.includes(&6));
        assert!(heap.includes(&7));
        assert!(!heap.includes(&8));
    }

    #[test]
    fn test_delete() {
        let mut heap = BHeap::new(&alloc_pages, &dealloc_pages);
        heap.insert(5u32, "how");
        heap.insert(3u32, "hello");
        heap.insert(6u32, "are");
        heap.insert(7u32, "you");
        heap.insert(4u32, "world");

        assert_eq!(heap.len, 5);
        assert!(!heap.includes(&2));
        assert!(heap.includes(&3));
        assert!(heap.includes(&4));
        assert!(heap.includes(&5));
        assert!(heap.includes(&6));
        assert!(heap.includes(&7));
        assert!(!heap.includes(&8));

        heap.delete(&5);

        assert_eq!(heap.len, 4);
        assert!(!heap.includes(&2));
        assert!(heap.includes(&3));
        assert!(heap.includes(&4));
        assert!(!heap.includes(&5));
        assert!(heap.includes(&6));
        assert!(heap.includes(&7));
        assert!(!heap.includes(&8));

        let buf = heap.buf.read();
        assert_eq!(unsafe { buf.unwrap().as_ptr().offset(0).read_volatile() }, Node { key: 3, val: "hello" });
        assert_eq!(unsafe { buf.unwrap().as_ptr().offset(1).read_volatile() }, Node { key: 4, val: "world" });
        assert_eq!(unsafe { buf.unwrap().as_ptr().offset(2).read_volatile() }, Node { key: 6, val: "are" });
        assert_eq!(unsafe { buf.unwrap().as_ptr().offset(3).read_volatile() }, Node { key: 7, val: "you" });
    }

    #[test]
    fn test_iter_from() {
        let mut heap = BHeap::new(&alloc_pages, &dealloc_pages);
        heap.insert(5u32, "how");
        heap.insert(3u32, "hello");
        heap.insert(6u32, "are");
        heap.insert(7u32, "you");
        heap.insert(4u32, "world");

        let mut iter = heap.iter_from(&5);
        assert_eq!(iter.next(), Some((&5, &"how")));
        assert_eq!(iter.next(), Some((&6, &"are")));
        assert_eq!(iter.next(), Some((&7, &"you")));
        assert_eq!(iter.next(), None);
    }

    #[test]
    fn test_iter_from_rev() {
        let mut heap = BHeap::new(&alloc_pages, &dealloc_pages);
        heap.insert(5u32, "how");
        heap.insert(3u32, "hello");
        heap.insert(6u32, "are");
        heap.insert(7u32, "you");
        heap.insert(4u32, "world");

        let mut iter = heap.iter_from(&5).rev();
        assert_eq!(iter.next(), Some((&7, &"you")));
        assert_eq!(iter.next(), Some((&6, &"are")));
        assert_eq!(iter.next(), Some((&5, &"how")));
        assert_eq!(iter.next(), None);
    }

    #[test]
    fn test_iter_from_same_key() {
        let mut heap = BHeap::new(&alloc_pages, &dealloc_pages);
        heap.insert(5u32, "how");
        heap.insert(3u32, "hello");
        heap.insert(6u32, "are");
        heap.insert(7u32, "you");
        heap.insert(5u32, "world");

        let mut iter = heap.iter_from(&5);
        assert_eq!(iter.next(), Some((&5, &"how")));
        assert_eq!(iter.next(), Some((&5, &"world")));
        assert_eq!(iter.next(), Some((&6, &"are")));
        assert_eq!(iter.next(), Some((&7, &"you")));
        assert_eq!(iter.next(), None);
    }

    #[test]
    fn test_iter_to() {
        let mut heap = BHeap::new(&alloc_pages, &dealloc_pages);
        heap.insert(5u32, "how");
        heap.insert(3u32, "hello");
        heap.insert(6u32, "are");
        heap.insert(7u32, "you");
        heap.insert(4u32, "world");

        let mut iter = heap.iter_to(&5);
        assert_eq!(iter.next(), Some((&3, &"hello")));
        assert_eq!(iter.next(), Some((&4, &"world")));
        assert_eq!(iter.next(), Some((&5, &"how")));
        assert_eq!(iter.next(), None);
    }

    #[test]
    fn test_iter_to_rev() {
        let mut heap = BHeap::new(&alloc_pages, &dealloc_pages);
        heap.insert(5u32, "how");
        heap.insert(3u32, "hello");
        heap.insert(6u32, "are");
        heap.insert(7u32, "you");
        heap.insert(4u32, "world");

        let mut iter = heap.iter_to(&5).rev();
        assert_eq!(iter.next(), Some((&5, &"how")));
        assert_eq!(iter.next(), Some((&4, &"world")));
        assert_eq!(iter.next(), Some((&3, &"hello")));
        assert_eq!(iter.next(), None);
    }

    #[test]
    fn test_iter_to_same_key() {
        let mut heap = BHeap::new(&alloc_pages, &dealloc_pages);
        heap.insert(5u32, "how");
        heap.insert(3u32, "hello");
        heap.insert(6u32, "are");
        heap.insert(7u32, "you");
        heap.insert(5u32, "world");

        let mut iter = heap.iter_to(&5);
        assert_eq!(iter.next(), Some((&3, &"hello")));
        assert_eq!(iter.next(), Some((&5, &"how")));
        assert_eq!(iter.next(), Some((&5, &"world")));
        assert_eq!(iter.next(), None);
    }
}
