#[cfg(test)]
use crate::test_utils::{alloc_pages, dealloc_pages};

#[cfg(not(test))]
use super::{alloc_pages, dealloc_pages};
use super::addr::VirtAddr;

mod bheap;

#[derive(PartialEq, Clone, Debug)]
struct Region {
    start: VirtAddr,
    len: usize,
}

impl Region {
    pub fn is_right_before(&self, rhs: &Self) -> bool {
        self.start + (self.len as u64) == rhs.start
    }
}

const REGION_SPLIT_THRESHOLD: usize = ::core::mem::size_of::<Region>() * 2;

impl PartialOrd for Region {
    fn partial_cmp(&self, other: &Self) -> Option<::core::cmp::Ordering> {
        let len_ord = self.len.partial_cmp(&other.len);

        if let Some(::core::cmp::Ordering::Equal) = len_ord {
            self.start.partial_cmp(&other.start)
        } else {
            len_ord
        }
    }
}

#[derive(Debug)]
pub struct Heap {
    free_space_by_size: bheap::BHeap<Region, ()>,
    free_space_by_addr: bheap::BHeap<VirtAddr, Region>,
    used_space: bheap::BHeap<VirtAddr, Region>,
}

impl Heap {
    pub fn new() -> Heap {
        let alloc = &alloc_pages;
        let dealloc = &dealloc_pages;

        Heap {
            free_space_by_size: bheap::BHeap::new(alloc, dealloc),
            free_space_by_addr: bheap::BHeap::new(alloc, dealloc),
            used_space: bheap::BHeap::new(alloc, dealloc),
        }
    }

    fn insert_free_space(&mut self, mut region: Region) {
        if let Some(region_before) = self.free_space_by_addr
            .iter_to(&VirtAddr::new(region.start.as_u64() - 1))
            .rev().next().map(|(_, r)| r.clone()) {
            if region_before.is_right_before(&region) {
                self.delete_free_space(&region_before);
                region.start = region_before.start;
                region.len += region_before.len;
            }
        }

        if let Some(region_after) = self.free_space_by_addr
            .iter_from(&VirtAddr::new(region.start.as_u64() + region.len as u64))
            .next().map(|(_, r)| r.clone()) {
            if region.is_right_before(&region_after) {
                self.delete_free_space(&region_after);
                region.len += region_after.len;
            }
        }

        self.free_space_by_addr.insert(region.start, region.clone());
        self.free_space_by_size.insert(region, ());
    }

    fn delete_free_space(&mut self, region: &Region) {
        self.free_space_by_addr.delete(&region.start);
        self.free_space_by_size.delete(region);
    }

    fn alloc_in_region(&mut self, mut region: Region, bytes: usize, align: usize) -> VirtAddr {
        self.delete_free_space(&region);

        let aligned_start = region.start.align_up(align as u64);
        let space_before = (aligned_start - region.start) as usize;
        let space_after = (region.start + region.len - (aligned_start + bytes)) as usize;

        if space_before >= REGION_SPLIT_THRESHOLD {
            self.insert_free_space(Region {
                start: region.start,
                len: space_before,
            });

            region.start = aligned_start;
            region.len -= space_before;
        }

        if space_after >= REGION_SPLIT_THRESHOLD {
            self.insert_free_space(Region {
                start: aligned_start + bytes,
                len: space_after,
            });

            region.len -= space_after;
        }

        self.used_space.insert(aligned_start, region);
        aligned_start
    }

    pub fn alloc(&mut self, bytes: usize, align: usize) -> VirtAddr {
        assert!(bytes > 0);
        assert!(align > 0);

        let min_region_size = bytes + align - 1;
        let min_region = Region { len: min_region_size, start: VirtAddr::new(0) };
        let free_region = self.free_space_by_size.iter_from(&min_region).next()
            .map(|(region, _)| region.clone());

        match free_region {
            Some(region) => {
                assert!(region.len >= min_region_size);

                self.alloc_in_region(region, bytes, align)
            }
            None => {
                let num_pages = (min_region_size + 0xFFF) / 0x1000;
                let page_addr = alloc_pages(num_pages)
                    .expect("could not allocate memory for heap");
                let region_size = num_pages * 0x1000;

                self.insert_free_space(Region {
                    start: page_addr,
                    len: region_size,
                });

                self.alloc(bytes, align)
            }
        }
    }

    pub fn dealloc(&mut self, addr: VirtAddr) {
        if let Some(used_region) = self.used_space.get(&addr) {
            let region = used_region.clone();

            self.used_space.delete(&addr);
            self.insert_free_space(region);
        } else {
            panic!("unable to free unallocated memory: {:#?}", addr);
        }
    }
}

#[cfg(test)]
mod test {
    use ::test::Bencher;

    use super::*;

    #[test]
    fn test_alloc_first() {
        let mut heap = Heap::new();
        let addr = heap.alloc(1, 1);

        assert_eq!(addr.as_u64() & 0xFFF, 0);

        let mut used_iter = heap.used_space.iter();
        assert_eq!(used_iter.next(), Some((&addr, &Region { start: addr, len: 1 })));
        assert_eq!(used_iter.next(), None);

        let mut free_iter = heap.free_space_by_size.iter();
        assert_eq!(free_iter.next(), Some((&Region { start: VirtAddr::new(addr.as_u64() + 1), len: 0xFFF }, &())));
        assert_eq!(free_iter.next(), None);

        heap.dealloc(addr);

        let mut used_iter = heap.used_space.iter();
        assert_eq!(used_iter.next(), None);

        let mut free_iter = heap.free_space_by_size.iter();
        assert_eq!(free_iter.next(), Some((&Region { start: addr, len: 0x1000 }, &())));
        assert_eq!(free_iter.next(), None);
    }

    #[test]
    fn test_dealloc_merge() {
        let mut heap = Heap::new();
        let addr1 = heap.alloc(1, 1);
        let addr2 = heap.alloc(1, 1);
        let addr3 = heap.alloc(1, 1);

        heap.dealloc(addr1);
        heap.dealloc(addr2);
        heap.dealloc(addr3);

        let mut used_iter = heap.used_space.iter();
        assert_eq!(used_iter.next(), None);

        let mut free_iter = heap.free_space_by_size.iter();
        assert_eq!(free_iter.next(), Some((&Region { start: addr1, len: 0x1000 }, &())));
        assert_eq!(free_iter.next(), None);
    }

    #[test]
    fn test_alloc_alignment() {
        let mut heap = Heap::new();
        let mut garbage = heap.alloc(0x1001, 1);
        heap.dealloc(garbage);
        garbage = heap.alloc(1, 1);
        let page = heap.alloc(1, 0x1000);

        assert_eq!(page.as_u64() & 0xFFF, 0);

        eprintln!("{:#?}", heap);

        let mut used_iter = heap.used_space.iter();
        assert_eq!(used_iter.next(), Some((&garbage, &Region { start: garbage, len: 1 })));
        assert_eq!(used_iter.next(), Some((&page, &Region { start: page, len: 1 })));
        assert_eq!(used_iter.next(), None);

        let mut free_iter = heap.free_space_by_size.iter();
        assert_eq!(free_iter.next(), Some((&Region { start: garbage + 1u64, len: 0xFFF }, &())));
        assert_eq!(free_iter.next(), Some((&Region { start: page + 1u64, len: 0xFFF }, &())));
        assert_eq!(free_iter.next(), None);

        heap.dealloc(garbage);
        heap.dealloc(page);

        let mut used_iter = heap.used_space.iter();
        assert_eq!(used_iter.next(), None);

        let mut free_iter = heap.free_space_by_size.iter();
        assert_eq!(free_iter.next(), Some((&Region { start: garbage, len: 0x2000 }, &())));
        assert_eq!(free_iter.next(), None);
    }

    #[bench]
    fn test_alloc_one_byte(b: &mut Bencher) {
        let mut heap = Heap::new();

        b.iter(|| {
            let addr = heap.alloc(1, 1);
            heap.dealloc(addr);
        });
    }

    #[bench]
    fn test_alloc_one_byte_after_1000(b: &mut Bencher) {
        let mut heap = Heap::new();

        for _i in 0..1000 {
            heap.alloc(1, 1);
        }

        b.iter(|| {
            let addr = heap.alloc(1, 1);
            heap.dealloc(addr);
        });
    }

    #[bench]
    fn test_alloc_one_byte_after_10000(b: &mut Bencher) {
        let mut heap = Heap::new();

        for _i in 0..10000 {
            heap.alloc(1, 1);
        }

        b.iter(|| {
            let addr = heap.alloc(1, 16);
            heap.dealloc(addr);
        });
    }
}
