use crate::stdlib::vec::Vec;
use crate::mem::addr::PhysAddr;
use super::table::{table_checksum, TableHeader, with_table_mapped};

pub const DSDT_SIGNATURE: &str = "DSDT";

#[derive(Debug)]
pub struct Dsdt {
    pub header: TableHeader,
    pub raw_data: Vec<u8>,
}

pub fn read_dsdt(phys_addr: PhysAddr) -> Option<Dsdt> {
    with_table_mapped(phys_addr, |header, data| {
        if table_checksum(header) != 0 || header.signature != DSDT_SIGNATURE.as_ref() {
            return None;
        }

        let raw_data_length = header.length as usize - ::core::mem::size_of::<TableHeader>();
        let mut raw_data = Vec::with_capacity(raw_data_length);

        unsafe {
            raw_data.set_len(raw_data_length);
            ::core::ptr::copy_nonoverlapping(data.as_ptr::<u8>(), raw_data.as_mut_ptr(), raw_data_length);
        }

        Some((header.clone(), raw_data))
    }).unwrap_or(None).map(|(header, raw_data)| -> Option<Dsdt> {
        Some(Dsdt {
            header,
            raw_data,
        })
    }).unwrap_or(None)
}
