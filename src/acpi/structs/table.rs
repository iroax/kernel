use crate::mem::addr::{PhysAddr, VirtAddr};
use crate::mem::{map_pages, unmap_pages};

#[derive(Debug, Clone, Copy, Default)]
#[repr(packed)]
pub struct TableHeader {
    pub signature: [u8; 4],
    pub length: u32,
    pub revision: u8,
    pub checksum: u8,
    pub oem_id: [u8; 6],
    pub oem_table_id: [u8; 8],
    pub oem_revision: u32,
    pub creator_id: u32,
    pub creator_revision: u32,
}

#[derive(Debug, Copy, Clone, Default)]
#[repr(packed)]
pub struct GenericAddressStructure {
    address_space: u8,
    bit_width: u8,
    bit_offset: u8,
    access_size: u8,
    address: u64,
}

pub fn table_checksum(header: &TableHeader) -> u8 {
    let buf = VirtAddr::from_ptr(header).as_ptr::<u8>();
    let mut sum = 0u8;

    for i in 0..header.length {
        sum = (sum as u16 + unsafe { buf.offset(i as isize).read() as u16 }) as u8;
    }

    sum
}

pub fn with_table_mapped<T, F: FnOnce(&TableHeader, VirtAddr) -> T>(phys_addr: PhysAddr, f: F) -> Option<T> {
    let num_pages = (((phys_addr.as_u64() & 0xFFF) as usize + ::core::mem::size_of::<TableHeader>()) + 0xFFF) / 0x1000 + 4;
    let maybe_virt_addr = map_pages(phys_addr.align_down(0x1000u64), num_pages, false);

    if maybe_virt_addr.is_none() {
        return None;
    }

    let mapped_virt_addr = maybe_virt_addr.unwrap();
    let virt_addr = mapped_virt_addr + (phys_addr.as_u64() & 0xFFF);

    let header = unsafe { &*virt_addr.as_ptr::<TableHeader>() };
    let data_virt_addr = virt_addr + ::core::mem::size_of::<TableHeader>();

    // TODO: if more data than 2 pages, unmap and map again, now the whole table

    let res = f(header, data_virt_addr);

    unmap_pages(mapped_virt_addr, num_pages);
    Some(res)
}
