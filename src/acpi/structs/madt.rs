use crate::mem::addr::PhysAddr;
use crate::stdlib::vec::Vec;
use crate::parser;

use super::table::{table_checksum, TableHeader, with_table_mapped};

pub const MADT_SIGNATURE: &str = "APIC";

#[derive(Debug)]
pub struct ProcessorLocalApic {
    acpi_processor_id: u8,
    apic_id: u8,
    flags: u32,
}

#[derive(Debug)]
pub struct IoApic {
    apic_id: u8,
    io_apic_address: u32,
    global_system_interrupt_base: u32,
}

#[derive(Debug)]
pub struct InterruptSourceOverride {
    bus_source: u8,
    irq_source: u8,
    global_system_interrupt: u32,
    flags: u16,
}

#[derive(Debug)]
pub struct NonMaskableInterrupts {
    acpi_processor_id: u8, // 0xFF means all processors
    flags: u16,
    lint: u8,
}

#[derive(Debug)]
pub struct LocalApicAddressOverride {
    local_apic_addr: PhysAddr,
}

#[derive(Debug)]
pub enum MadtEntry {
    ProcessorLocalApic(ProcessorLocalApic),
    IoApic(IoApic),
    InterruptSourceOverride(InterruptSourceOverride),
    NonMaskableInterrupts(NonMaskableInterrupts),
    LocalApicAddressOverride(LocalApicAddressOverride),
}

#[derive(Debug)]
pub struct Madt {
    header: TableHeader,
    local_apic_addr: u32,
    flags: u32,
    entries: Vec<MadtEntry>,
}

fn get_madt_entry_map() -> &'static parser::ParserByTagMap<'static, MadtEntry> {
    static MAP: spin::Once<parser::ParserByTagMap<MadtEntry>> = spin::Once::new();

    MAP.call_once(|| {
        parser::ParserByTagMap::builder()
            .add_parser(&[0x00], &|data| {
                // Processor Local APIC
                data.read_bytes(2)?;

                let acpi_processor_id = data.read_scalar::<u8>()?;
                let apic_id = data.read_scalar::<u8>()?;
                let flags = data.read_scalar::<u32>()?;

                Ok(MadtEntry::ProcessorLocalApic(ProcessorLocalApic {
                    acpi_processor_id,
                    apic_id,
                    flags,
                }))
            })
            .add_parser(&[0x01], &|data| {
                // I/O APIC
                data.read_bytes(2)?;

                let apic_id = data.read_scalar::<u8>()?;
                data.read_bytes(1)?; // skip reserved byte
                let io_apic_address = data.read_scalar::<u32>()?;
                let global_system_interrupt_base = data.read_scalar::<u32>()?;

                Ok(MadtEntry::IoApic(IoApic {
                    apic_id,
                    io_apic_address,
                    global_system_interrupt_base,
                }))
            })
            .add_parser(&[0x02], &|data| {
                // Interrupt Source Override
                data.read_bytes(2)?;

                let bus_source = data.read_scalar::<u8>()?;
                let irq_source = data.read_scalar::<u8>()?;
                let global_system_interrupt = data.read_scalar::<u32>()?;
                let flags = data.read_scalar::<u16>()?;

                Ok(MadtEntry::InterruptSourceOverride(InterruptSourceOverride {
                    bus_source,
                    irq_source,
                    global_system_interrupt,
                    flags,
                }))
            })
            .add_parser(&[0x04], &|data| {
                // Non-maskable interrupts
                data.read_bytes(2)?;

                let acpi_processor_id = data.read_scalar::<u8>()?;
                let flags = data.read_scalar::<u16>()?;
                let lint = data.read_scalar::<u8>()?;

                Ok(MadtEntry::NonMaskableInterrupts(NonMaskableInterrupts {
                    acpi_processor_id,
                    flags,
                    lint,
                }))
            })
            .add_parser(&[0x05], &|data| {
                // Local APIC Address Override
                data.read_bytes(2)?;

                let local_apic_addr = PhysAddr::new(data.read_scalar::<u64>()?);

                Ok(MadtEntry::LocalApicAddressOverride(LocalApicAddressOverride {
                    local_apic_addr,
                }))
            })
            .finish()
    })
}

fn parse_madt(bytes: &[u8]) -> parser::ParseResult<(u32, u32, Vec<MadtEntry>)> {
    let mut data = parser::Data::new(bytes);

    let local_apic_addr = data.read_scalar::<u32>()?;
    let flags = data.read_scalar::<u32>()?;
    let entries = parser::parse_all(&mut data, &|data| {
        parser::parse_by_tag(data, get_madt_entry_map())
    })?;

    Ok((local_apic_addr, flags, entries))
}

pub fn read_madt(phys_addr: PhysAddr) -> Option<Madt> {
    with_table_mapped(phys_addr, |header, data_addr| -> Option<Madt> {
        if table_checksum(header) != 0 || header.signature != MADT_SIGNATURE.as_ref() {
            return None;
        }

        let input_ptr = data_addr.as_ptr::<u8>();
        let input_len = header.length as usize - ::core::mem::size_of::<TableHeader>();
        let input = unsafe { ::core::slice::from_raw_parts(input_ptr, input_len) };

        match parse_madt(input) {
            Ok((local_apic_addr, flags, entries)) => Some(Madt {
                header: *header,
                local_apic_addr,
                flags,
                entries,
            }),
            Err(_) => None,
        }
    }).unwrap_or(None)
}
