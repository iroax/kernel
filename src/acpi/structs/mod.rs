use crate::mem::addr::PhysAddr;
use crate::stdlib::boxed::Box;

pub mod dsdt;
pub mod fadt;
pub mod table;
pub mod rsdt;
pub mod rsdp;
pub mod hpet;
pub mod madt;

#[derive(Debug)]
pub struct AcpiStructs {
    pub rsdp: rsdp::Rsdp,
    pub rsdt: rsdt::Rsdt,
    pub dsdt: dsdt::Dsdt,
    pub fadt: Box<fadt::Fadt>,
    pub hpet: Option<Box<hpet::Hpet>>,
    pub madt: Option<madt::Madt>
}

pub fn read(rsdp_start_addr: PhysAddr, rsdp_end_addr: PhysAddr) -> Option<AcpiStructs> {
    let rsdp = rsdp::find(rsdp_start_addr, rsdp_end_addr)?;
    let rsdt = rsdt::read_rsdt(rsdp)?;
    let fadt = fadt::read_fadt(*(rsdt.get(fadt::FADT_SIGNATURE)?))?;
    let dsdt = dsdt::read_dsdt(fadt.get_dsdt_addr())?;
    let hpet = match rsdt.get(hpet::HPET_SIGNATURE) {
        None => None,
        Some(hpet_addr) => hpet::read_hpet(*hpet_addr),
    };
    let madt = match rsdt.get(madt::MADT_SIGNATURE) {
        None => None,
        Some(madt_addr) => madt::read_madt(*madt_addr),
    };

    Some(AcpiStructs {
        rsdp,
        rsdt,
        fadt,
        dsdt,
        hpet,
        madt,
    })
}
