use crate::stdlib::boxed::Box;
use crate::mem::addr::{PhysAddr, VirtAddr};
use super::table::{table_checksum, TableHeader, with_table_mapped, GenericAddressStructure};

pub const FADT_SIGNATURE: &str = "FACP";

#[derive(Debug, Copy, Clone, Default)]
#[repr(packed)]
pub struct Fadt {
    header: TableHeader,

    firmware_ctrl: u32,
    dsdt: u32,

    // field used in ACPI 1.0; no longer in use, for compatibility only
    reserved: u8,

    preferred_power_management_profile: u8,
    sci_interrupt: u16,
    pub smi_command_port: u32,
    pub acpi_enable: u8,
    acpi_disable: u8,
    s4bios_req: u8,
    pstate_control: u8,
    pm1a_event_block: u32,
    pm1b_event_block: u32,
    pub pm1a_control_block: u32,
    pub pm1b_control_block: u32,
    pm2control_block: u32,
    pmtimer_block: u32,
    gpe0block: u32,
    gpe1block: u32,
    pm1event_length: u8,
    pm1control_length: u8,
    pm2control_length: u8,
    pmtimer_length: u8,
    gpe0length: u8,
    gpe1length: u8,
    gpe1base: u8,
    cstate_control: u8,
    worst_c2latency: u16,
    worst_c3latency: u16,
    flush_size: u16,
    flush_stride: u16,
    duty_offset: u8,
    duty_width: u8,
    day_alarm: u8,
    month_alarm: u8,
    century: u8,

    // reserved in ACPI 1.0; used since ACPI 2.0+
    boot_architecture_flags: u16,

    reserved2: u8,
    flags: u32,

    // 12 byte structure; see below for details
    reset_reg: GenericAddressStructure,

    reset_value: u8,
    reserved3: [u8; 3],

    // 64bit pointers - Available on ACPI 2.0+
    x_firmware_control: u64,
    x_dsdt: u64,

    x_pm1a_event_block: GenericAddressStructure,
    x_pm1b_event_block: GenericAddressStructure,
    x_pm1a_control_block: GenericAddressStructure,
    x_pm1b_control_block: GenericAddressStructure,
    x_pm2control_block: GenericAddressStructure,
    x_pmtimer_block: GenericAddressStructure,
    x_gpe0block: GenericAddressStructure,
    x_gpe1block: GenericAddressStructure,
}

impl Fadt {
    pub fn get_dsdt_addr(&self) -> PhysAddr {
        if self.header.revision < 2 {
            PhysAddr::new(self.dsdt as u64)
        } else {
            PhysAddr::new(self.x_dsdt)
        }
    }
}

pub fn read_fadt(phys_addr: PhysAddr) -> Option<Box<Fadt>> {
    with_table_mapped(phys_addr, |header, _| -> Option<Box<Fadt>> {
        if table_checksum(header) != 0 || header.signature != FADT_SIGNATURE.as_ref() {
            return None;
        }

        let mut fadt = Fadt::default();
        let header_addr = VirtAddr::from_ptr(header).as_ptr::<u8>();
        let fadt_addr = VirtAddr::from_ptr(&mut fadt).as_mut_ptr::<u8>();
        let len = ::core::cmp::min(::core::mem::size_of::<Fadt>(), header.length as usize);

        unsafe {
            ::core::ptr::copy_nonoverlapping(header_addr, fadt_addr, len);
        }

        Some(Box::new(fadt))
    }).unwrap_or(None)
}
