use crate::mem::addr::{PhysAddr, VirtAddr};
use crate::mem::{map_pages, unmap_pages};

const VALID_SIGNATURE: &[u8; 8] = b"RSD PTR ";

#[derive(Debug, Clone, Copy)]
#[repr(packed)]
pub struct Rsdp {
    pub signature: [u8; 8],
    pub checksum: u8,
    pub oemid: [u8; 6],
    pub revision: u8,
    pub rsdt_address: u32,
    pub length: u32,
    pub xsdt_address: u64,
    pub extended_checksum: u8,
    pub reserved: [u8; 3]
}

fn checksum(rsdp: &Rsdp) -> u8 {
    let buf = VirtAddr::from_ptr(rsdp).as_ptr::<u8>();
    let size = ::core::mem::size_of::<Rsdp>();
    let mut sum = 0u8;

    for i in 0..size {
        sum = (sum as u16 + unsafe { buf.offset(i as isize).read() as u16 }) as u8;
    }

    sum
}

pub fn find(start_addr: PhysAddr, end_addr: PhysAddr) -> Option<Rsdp> {
    let num_pages = ((end_addr - start_addr + 0xFFF) / 0x1000) as usize;
    let virt_addr = map_pages(start_addr, num_pages, false)
        .expect("acpi/rsdp: could not map pages");
    let mut result = None;

    for i in 0..((end_addr.as_u64() + 1 - start_addr.as_u64()) / 16) {
        let rsdp = unsafe { &*(virt_addr + i * 16).as_mut_ptr::<Rsdp>() };

        if rsdp.signature == *VALID_SIGNATURE && checksum(rsdp) == 0 {
            result = Some(rsdp.clone());
            break;
        }
    }

    unmap_pages(virt_addr, num_pages);
    result
}
