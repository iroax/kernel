use bit_field::BitField;

use crate::mem::addr::{PhysAddr, VirtAddr};
use crate::stdlib::boxed::Box;

use super::table::{GenericAddressStructure, table_checksum, TableHeader, with_table_mapped};

pub const HPET_SIGNATURE: &str = "HPET";

#[derive(Debug, Copy, Clone, Default)]
#[repr(packed)]
pub struct Hpet {
    header: TableHeader,
    event_timer_blk_id: u32,
    base_address: GenericAddressStructure,
    hpet_num: u8,
    main_counter_min_clock_tick: u16,
    page_protection_and_oem: u8,
}

impl Hpet {
    fn get_blk_id(&self) -> u32 {
        self.event_timer_blk_id
    }

    pub fn get_hardware_rev_id(&self) -> u8 {
        self.get_blk_id().get_bits(0..8) as u8
    }

    pub fn get_comparator_count(&self) -> u8 {
        self.get_blk_id().get_bits(8..13) as u8
    }

    pub fn is_counter_64bits(&self) -> bool {
        self.get_blk_id().get_bit(13)
    }

    pub fn is_legacy_replacement_interrupt_route_supported(&self) -> bool {
        self.get_blk_id().get_bit(15)
    }

    pub fn get_vendor_id(&self) -> u16 {
        self.get_blk_id().get_bits(16..32) as u16
    }
}

pub fn read_hpet(phys_addr: PhysAddr) -> Option<Box<Hpet>> {
    with_table_mapped(phys_addr, |header, _| -> Option<Box<Hpet>> {
        if table_checksum(header) != 0 || header.signature != HPET_SIGNATURE.as_ref() {
            return None;
        }

        let mut hpet = Hpet::default();
        let header_addr = VirtAddr::from_ptr(header).as_ptr::<u8>();
        let hpet_addr = VirtAddr::from_ptr(&mut hpet).as_mut_ptr::<u8>();
        let len = ::core::cmp::min(::core::mem::size_of::<Hpet>(), header.length as usize);

        unsafe {
            ::core::ptr::copy_nonoverlapping(header_addr, hpet_addr, len);
        }

        Some(Box::new(hpet))
    }).unwrap_or(None)
}
