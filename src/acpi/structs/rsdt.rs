use crate::mem::addr::PhysAddr;
use crate::stdlib::collections::BTreeMap;
use crate::stdlib::string::String;

use super::table::{table_checksum, TableHeader, with_table_mapped};

const RSDT_SIGNATURE: &[u8] = b"RSDT";
const XSDT_SIGNATURE: &[u8] = b"XSDT";

pub type Rsdt = BTreeMap<String, PhysAddr>;

pub fn read_rsdt<'a>(rsdp: super::rsdp::Rsdp) -> Option<Rsdt> {
    let mut maybe_phys_addr = None;
    let mut maybe_signature = None;
    let is_extended = rsdp.revision >= 2;

    if is_extended && rsdp.xsdt_address != 0 {
        maybe_phys_addr = Some(PhysAddr::new(rsdp.xsdt_address));
        maybe_signature = Some(XSDT_SIGNATURE);
    } else if rsdp.revision == 0 && rsdp.rsdt_address != 0 {
        maybe_phys_addr = Some(PhysAddr::new(rsdp.rsdt_address as u64));
        maybe_signature = Some(RSDT_SIGNATURE);
    }

    if maybe_phys_addr.is_none() {
        return None;
    }

    let phys_addr = maybe_phys_addr.unwrap();
    let signature = maybe_signature.unwrap();

    with_table_mapped(phys_addr, |header, data_virt_addr| -> Option<BTreeMap<String, PhysAddr>> {
        if header.signature != *signature || table_checksum(header) != 0 {
            return None;
        }

        fn insert_table_signature(map: &mut BTreeMap<String, PhysAddr>, phys_addr: PhysAddr) {
            let maybe_key = with_table_mapped(phys_addr, |header, _| -> Option<String> {
                if table_checksum(header) != 0 {
                    return None;
                }

                ::core::str::from_utf8(&header.signature).ok().map(|s| { String::from(s) })
            }).unwrap_or(None);

            if maybe_key.is_none() {
                return;
            }

            let key = maybe_key.unwrap();
            map.insert(key, phys_addr);
        }

        let mut map = BTreeMap::new();

        if is_extended {
            let num_pointers = (header.length as usize - ::core::mem::size_of::<TableHeader>()) / 8;
            let pointers = data_virt_addr.as_ptr::<u64>();

            for i in 0..num_pointers {
                let table_phys_addr = PhysAddr::new(unsafe { *pointers.offset(i as isize) });
                insert_table_signature(&mut map, table_phys_addr);
            }
        } else {
            let num_pointers = (header.length as usize - ::core::mem::size_of::<TableHeader>()) / 4;
            let pointers = data_virt_addr.as_ptr::<u32>();

            for i in 0..num_pointers {
                let table_phys_addr = PhysAddr::new(unsafe { *pointers.offset(i as isize) } as u64);
                insert_table_signature(&mut map, table_phys_addr);
            }
        }

        Some(map)
    }).unwrap_or(None)
}
