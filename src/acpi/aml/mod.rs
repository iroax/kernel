use crate::stdlib::string::String;

use super::super::parser::{Data, ParseResult};

use self::parsers::*;
use self::structs::*;
pub use self::value::*;

mod parsers;
mod structs;
mod value;

#[derive(PartialEq, Debug)]
pub struct Aml {
    terms: TermList,
}

impl AmlValue for Aml {
    fn as_term_list(&self) -> Option<&TermList> {
        Some(&self.terms)
    }
}

pub fn resolve_path(scope: &String, rel_path: &String) -> String {
    use crate::stdlib::slice::SliceConcatExt;

    vec![scope.clone(), rel_path.clone()].join(".")
}

pub fn parse(bytes: &[u8]) -> ParseResult<Aml> {
    let mut data = Data::new(bytes);

    let terms = parse_term_list(&mut data)?;

    Ok(Aml { terms })
}
