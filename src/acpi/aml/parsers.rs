#![allow(dead_code)]

use bit_field::BitField;

use crate::parser::*;
use crate::stdlib::boxed::Box;
use crate::stdlib::collections::BTreeMap;
use crate::stdlib::rc::Rc;
use crate::stdlib::slice::SliceConcatExt;
use crate::stdlib::string::String;

use super::*;

const EXT_OP: u8 = 0x5B;

#[derive(Debug, PartialEq, Clone)]
struct ParseContext {
    method_arg_count: Rc<BTreeMap<String, usize>>,
    current_scope: String,
}

impl Context for ParseContext {
    fn debug_fmt(&self, f: &mut ::core::fmt::Formatter) -> ::core::fmt::Result {
        <ParseContext as ::core::fmt::Debug>::fmt(self, f)
    }

    fn clone(&self) -> Box<Context> {
        Box::new(<ParseContext as Clone>::clone(self))
    }

    fn eq(&self, rhs: &Context) -> bool {
        match rhs.as_any().downcast_ref::<ParseContext>() {
            None => false,
            Some(other) => <ParseContext as PartialEq<ParseContext>>::eq(self, other)
        }
    }
}

fn get_parse_context<'a>(data: &'a Data) -> ParseResult<&'a ParseContext> {
    data.get_context()
        .map(|ctx| {
            ctx.as_any().downcast_ref::<ParseContext>()
                .map(|ctx| Ok(ctx))
                .unwrap_or_else(|| data.error(ErrorCode::ContextRequired))
        })
        .unwrap_or_else(|| data.error(ErrorCode::ContextRequired))
}

fn parse_pkg_length_ex(data: &mut Data) -> ParseResult<usize> {
    data.check_len(1)?;

    /*
     * PkgLength := PkgLeadByte |
     *              <PkgLeadByte ByteData> |
     *              <PkgLeadByte ByteData ByteData> |
     *              <PkgLeadByte ByteData ByteData ByteData>
     */

    let lead_byte = data.read_scalar::<u8>()?;
    let byte_data_count = usize::from(lead_byte.get_bits(6..8));

    if byte_data_count == 0 {
        let length = usize::from(lead_byte.get_bits(0..6));
        return Ok(length);
    }

    data.check_len(byte_data_count)?;

    let mut length = usize::from(lead_byte.get_bits(0..4));
    for i in 0..byte_data_count {
        let byte_data = data.read_scalar::<u8>()?;
        length += usize::from(byte_data) << (4 + i * 8);
    }

    Ok(length)
}

fn parse_pkg_length(data: &mut Data) -> ParseResult<usize> {
    let len_before = data.len();
    let pkg_length = parse_pkg_length_ex(data)?;
    let len_after = data.len();

    Ok(pkg_length - (len_before - len_after))
}

fn get_object_map() -> &'static ParserByTagMap<'static, Object> {
    static MAP: spin::Once<ParserByTagMap<Object>> = spin::Once::new();

    MAP.call_once(|| {
        ParserByTagMap::builder()
            .add_map(get_named_obj_map(), &|named_obj| {
                Object::NamedObj(named_obj)
            })
            .add_map(get_name_space_modifier_obj_map(), &|name_space_modifier_obj| {
                Object::NameSpaceModifierObj(name_space_modifier_obj)
            })
            .finish()
    })
}

// Object := NameSpaceModifierObj | NamedObj
fn parse_object(data: &mut Data) -> ParseResult<Object> {
    parse_by_tag(data, get_object_map())
}

// ObjectList := Nothing | <Object ObjectList>
fn parse_object_list(data: &mut Data) -> ParseResult<ObjectList> {
    parse_all(data, &parse_object)
}

// DefDevice := DeviceOp PkgLength NameString ObjectList
fn parse_def_device(data: &mut Data) -> ParseResult<(String, ObjectList)> {
    parse_tag(data, &[EXT_OP, 0x82])?;
    parse_cut(data, &parse_pkg_length, &|data| {
        let name = parse_name_string(data)?;
        let obj_list = parse_object_list(data)?;

        Ok((name, obj_list))
    })
}

fn get_name_seg_map() -> &'static ParserByTagMap<'static, String> {
    static MAP: spin::Once<ParserByTagMap<String>> = spin::Once::new();

    MAP.call_once(|| {
        let mut b = ParserByTagMap::builder();

        for prefix in b'A'..=b'Z' {
            b = b.add_parser(Box::leak(Box::new([prefix])), &parse_name_seg);
        }

        b.add_parser(&[b'_'], &parse_name_seg)
            .finish()
    })
}

// NameSeg := <LeadNameChar NameChar NameChar NameChar>
fn parse_name_seg(data: &mut Data) -> ParseResult<String> {
    let mut seg = String::with_capacity(4);

    match data.peek_scalar::<u8>()? {
        b'A'..=b'Z' | b'_' => {
            seg.push(data.read_scalar::<u8>()? as char);
        }
        _ => {
            return data.error(ErrorCode::ExpectedSomethingElse);
        }
    }

    for _ in 0..3 {
        match data.peek_scalar::<u8>()? {
            b'A'..=b'Z' | b'_' | b'0'..=b'9' => {
                seg.push(data.read_scalar::<u8>()? as char);
            }
            _ => {
                return data.error(ErrorCode::ExpectedSomethingElse);
            }
        }
    }

    Ok(seg)
}

fn parse_prefix_path(data: &mut Data) -> ParseResult<String> {
    let mut result = String::new();

    loop {
        match data.eat_scalar(b'^') {
            Ok(_) => {
                result.push('^');
            }
            Err(_) => {
                break;
            }
        }
    }

    Ok(result)
}

fn get_name_string_map() -> &'static ParserByTagMap<'static, String> {
    static MAP: spin::Once<ParserByTagMap<String>> = spin::Once::new();

    MAP.call_once(|| {
        ParserByTagMap::builder()
            .add_parser(&[b'\\'], &|data| {
                data.eat_bytes(&[b'\\'])?;
                let name = parse_by_tag(data, get_name_path_map())?;
                Ok(String::from("\\") + &name)
            })
            .add_parser(&[b'^'], &|data| {
                let prefix = parse_prefix_path(data)?;
                let name = parse_name_path(data)?;
                Ok(prefix + &name)
            })
            .add_map(get_name_path_map(), &|name_path| name_path)
            .finish()
    })
}

// NameString := <RootChar NamePath> | <PrefixPath NamePath>
fn parse_name_string(data: &mut Data) -> ParseResult<String> {
    parse_by_tag(data, get_name_string_map())
}

// DefScope := ScopeOp PkgLength NameString TermList
fn parse_def_scope(data: &mut Data) -> ParseResult<(String, TermList)> {
    parse_tag(data, &[0x10])?;
    parse_cut(data, &parse_pkg_length, &|data| {
        let name = parse_name_string(data)?;
        let term_list = parse_term_list(data)?;

        Ok((name, term_list))
    })
}

// NullName := 0x00
fn parse_null_name(data: &mut Data) -> ParseResult<()> {
    parse_tag(data, &[0x00])?;
    Ok(())
}

// DualNamePath := DualNamePrefix NameSeg NameSeg
fn parse_dual_name_path(data: &mut Data) -> ParseResult<String> {
    parse_tag(data, &[0x2E])?;
    let seg1 = parse_name_seg(data)?;
    let seg2 = parse_name_seg(data)?;
    Ok(format!("{}.{}", seg1, seg2))
}

// MultiNamePath := MultiNamePrefix SegCount NameSeg(SegCount)
fn parse_multi_name_path(data: &mut Data) -> ParseResult<String> {
    parse_tag(data, &[0x2F])?;
    let segs = parse_times(data, &|data| {
        Ok(data.read_scalar::<u8>()? as usize)
    }, &parse_name_seg)?;
    Ok(segs.join("."))
}

fn get_name_path_map() -> &'static ParserByTagMap<'static, String> {
    static MAP: spin::Once<ParserByTagMap<String>> = spin::Once::new();

    MAP.call_once(|| {
        ParserByTagMap::builder()
            .add_parser(&[0x00], &|data| {
                parse_null_name(data)?;
                Ok(String::with_capacity(0))
            })
            .add_parser(&[0x2E], &parse_dual_name_path)
            .add_parser(&[0x2F], &parse_multi_name_path)
            .add_map(get_name_seg_map(), &|x| x)
            .finish()
    })
}

// NamePath := NameSeg | DualNamePath | MultiNamePath | NullName
fn parse_name_path(data: &mut Data) -> ParseResult<String> {
    parse_by_tag(data, get_name_path_map())
}

fn get_term_obj_map() -> &'static ParserByTagMap<'static, TermObj> {
    static MAP: spin::Once<ParserByTagMap<TermObj>> = spin::Once::new();

    MAP.call_once(|| {
        ParserByTagMap::builder()
            .add_map(get_named_obj_map(), &|named_obj| {
                TermObj::NamedObj(named_obj)
            })
            .add_map(get_name_space_modifier_obj_map(), &|name_space_modifier_obj| {
                TermObj::NameSpaceModifierObj(name_space_modifier_obj)
            })
            .add_map(get_type1_opcode_map(), &|type1_opcode| {
                TermObj::Type1Opcode(type1_opcode)
            })
            .add_map(get_type2_opcode_map(), &|type2_opcode| {
                TermObj::Type2Opcode(type2_opcode)
            })
            .finish()
    })
}

fn parse_term_obj(data: &mut Data) -> ParseResult<TermObj> {
    parse_by_tag(data, get_term_obj_map())
}

fn get_arg_obj_map() -> &'static ParserByTagMap<'static, ArgObj> {
    static MAP: spin::Once<ParserByTagMap<ArgObj>> = spin::Once::new();

    MAP.call_once(|| {
        ParserByTagMap::builder()
            .add_const(&[0x68], ArgObj::Arg0)
            .add_const(&[0x69], ArgObj::Arg1)
            .add_const(&[0x6A], ArgObj::Arg2)
            .add_const(&[0x6B], ArgObj::Arg3)
            .add_const(&[0x6C], ArgObj::Arg4)
            .add_const(&[0x6D], ArgObj::Arg5)
            .add_const(&[0x6E], ArgObj::Arg6)
            .finish()
    })
}

fn parse_arg_obj(data: &mut Data) -> ParseResult<ArgObj> {
    parse_by_tag(data, get_arg_obj_map())
}

fn get_local_obj_map() -> &'static ParserByTagMap<'static, LocalObj> {
    static MAP: spin::Once<ParserByTagMap<LocalObj>> = spin::Once::new();

    MAP.call_once(|| {
        ParserByTagMap::builder()
            .add_const(&[0x60], LocalObj::Local0)
            .add_const(&[0x61], LocalObj::Local1)
            .add_const(&[0x62], LocalObj::Local2)
            .add_const(&[0x63], LocalObj::Local3)
            .add_const(&[0x64], LocalObj::Local4)
            .add_const(&[0x65], LocalObj::Local5)
            .add_const(&[0x66], LocalObj::Local6)
            .add_const(&[0x67], LocalObj::Local7)
            .finish()
    })
}

fn parse_local_obj(data: &mut Data) -> ParseResult<LocalObj> {
    parse_by_tag(data, get_local_obj_map())
}

fn get_term_arg_map() -> &'static ParserByTagMap<'static, TermArg> {
    static MAP: spin::Once<ParserByTagMap<TermArg>> = spin::Once::new();

    MAP.call_once(|| {
        ParserByTagMap::builder()
            .add_map(get_name_string_map(), &|name| {
                TermArg::NameString(name)
            })
            .add_map(get_data_object_map(), &|data_object| {
                TermArg::DataObject(data_object)
            })
            .add_map(get_arg_obj_map(), &|arg_obj| {
                TermArg::ArgObj(arg_obj)
            })
            .add_map(get_local_obj_map(), &|local_obj| {
                TermArg::LocalObj(local_obj)
            })
            .add_map(get_type2_opcode_map(), &|type2_opcode| {
                TermArg::Type2Opcode(type2_opcode)
            })
            .finish()
    })
}

// TermArg := Type2Opcode | DataObject | ArgObj | LocalObj
fn parse_term_arg(data: &mut Data) -> ParseResult<TermArg> {
    parse_by_tag(data, get_term_arg_map())
}

// DefOpRegion := OpRegionOp NameString RegionSpace RegionOffset RegionLen
fn parse_def_op_region(data: &mut Data) -> ParseResult<(String, RegionSpace, RegionOffset, RegionLen)> {
    parse_tag(data, &[EXT_OP, 0x80])?;

    let name_string = parse_name_string(data)?;
    let region_space = match data.read_scalar::<u8>()? {
        0x00 => RegionSpace::SystemMemory,
        0x01 => RegionSpace::SystemIO,
        0x02 => RegionSpace::PciConfig,
        0x03 => RegionSpace::EmbeddedControl,
        0x04 => RegionSpace::SMBus,
        0x05 => RegionSpace::SystemCMOS,
        0x06 => RegionSpace::PciBarTarget,
        0x07 => RegionSpace::Ipmi,
        user_defined => RegionSpace::UserDefined(user_defined),
    };
    let region_offset = parse_term_arg(data)?;
    let region_len = parse_term_arg(data)?;

    Ok((name_string, region_space, region_offset, region_len))
}

// DefMethod := MethodOp PkgLength NameString MethodFlags TermList
fn parse_def_method(data: &mut Data) -> ParseResult<(String, MethodFlags, MethodBody)> {
    parse_tag(data, &[0x14])?;
    parse_cut(data, &parse_pkg_length, &|data| {
        let name_string = parse_name_string(data)?;
        let flags = data.read_scalar::<u8>()?;
        let arg_count = flags.get_bits(0..3);
        let serialize_flag = match flags.get_bits(3..4) {
            0 => MethodSerializeFlag::NotSerialized,
            1 => MethodSerializeFlag::Serialized,
            _ => { unreachable!(); }
        };
        let sync_level = flags.get_bits(4..8);
        let method_flags = MethodFlags { arg_count, serialize_flag, sync_level };

        let body = parse_or(data, &|data| {
            let term_list = parse_term_list(data)?;
            Ok(MethodBody::TermList(term_list))
        }, &|data| {
            let bytes = data.read_bytes(data.len())?;
            Ok(MethodBody::ByteCode(bytes.to_vec()))
        })?;

        Ok((name_string, method_flags, body))
    })
}

fn get_named_field_map() -> &'static ParserByTagMap<'static, (String, usize)> {
    static MAP: spin::Once<ParserByTagMap<(String, usize)>> = spin::Once::new();

    MAP.call_once(|| {
        ParserByTagMap::builder()
            .add_map_ex(get_name_seg_map(), &|name, data| {
                let size = parse_pkg_length_ex(data)?;
                Ok((name, size))
            })
            .finish()
    })
}

// NamedField := NameSeg PkgLength
fn parse_named_field(data: &mut Data) -> ParseResult<(String, usize)> {
    parse_by_tag(data, get_named_field_map())
}

// ReservedField := 0x00 PkgLength
fn parse_reserved_field(data: &mut Data) -> ParseResult<usize> {
    parse_tag(data, &[0x00])?;
    parse_pkg_length_ex(data)
}

fn get_field_element_map() -> &'static ParserByTagMap<'static, FieldElement> {
    static MAP: spin::Once<ParserByTagMap<FieldElement>> = spin::Once::new();

    MAP.call_once(|| {
        ParserByTagMap::builder()
            .add_map(get_named_field_map(), &|(name, size)| {
                FieldElement::NamedField(name, size)
            })
            .add_parser(&[0x00], &|data| {
                let size = parse_reserved_field(data)?;
                Ok(FieldElement::ReservedField(size))
            })
            .finish()
    })
}

// FieldElement := NamedField | ReservedField | AccessField | ExtendedAccessField |
//                 ConnectField
fn parse_field_element(data: &mut Data) -> ParseResult<FieldElement> {
    parse_by_tag(data, get_field_element_map())
}

// FieldList := Nothing | <FieldElement FieldList>
fn parse_field_list(data: &mut Data) -> ParseResult<FieldList> {
    parse_all(data, &parse_field_element)
}

fn parse_field_flags(data: &mut Data) -> ParseResult<FieldFlags> {
    let byte = data.read_scalar::<u8>()?;

    let access_type = match byte.get_bits(0..4) {
        0 => FieldFlagsAccessType::AnyAccessType,
        1 => FieldFlagsAccessType::ByteAccessType,
        2 => FieldFlagsAccessType::WordAccessType,
        3 => FieldFlagsAccessType::DWordAccessType,
        4 => FieldFlagsAccessType::QWordAccessType,
        5 => FieldFlagsAccessType::BufferAccessType,
        other => FieldFlagsAccessType::Reserved(other),
    };

    let lock_rule = match byte.get_bits(4..5) {
        0 => FieldFlagsLockRule::NoLock,
        1 => FieldFlagsLockRule::Lock,
        _ => { unreachable!(); }
    };

    let update_rule = match byte.get_bits(5..7) {
        0 => FieldFlagsUpdateRule::Preserve,
        1 => FieldFlagsUpdateRule::WriteAsOnes,
        2 => FieldFlagsUpdateRule::WriteAsZeros,
        _ => { unreachable!(); }
    };

    Ok(FieldFlags { access_type, lock_rule, update_rule })
}

// DefField := FieldOp PkgLength NameString FieldFlags FieldList
fn parse_def_field(data: &mut Data) -> ParseResult<(String, FieldFlags, FieldList)> {
    parse_tag(data, &[EXT_OP, 0x81])?;
    parse_cut(data, &parse_pkg_length, &|data| {
        let name = parse_name_string(data)?;
        let field_flags = parse_field_flags(data)?;
        let field_list = parse_field_list(data)?;
        Ok((name, field_flags, field_list))
    })
}

// DefProcessor := ProcessorOp PkgLength NameString ProcID PblkAddr PblkLen ObjectList
fn parse_def_processor(data: &mut Data) -> ParseResult<(String, ProcID, PblkAddr, PblkLen, ObjectList)> {
    parse_tag(data, &[EXT_OP, 0x83])?;
    parse_cut(data, &parse_pkg_length, &|data| {
        let name = parse_name_string(data)?;
        let proc_id = data.read_scalar::<u8>()?;
        let pblk_addr = data.read_scalar::<u32>()?;
        let pblk_len = data.read_scalar::<u8>()?;
        let obj_list = parse_object_list(data)?;
        Ok((name, proc_id, pblk_addr, pblk_len, obj_list))
    })
}

// DefCreateDWordField := CreateDWordFieldOp SourceBuff ByteIndex NameString
fn parse_def_create_dword_field(data: &mut Data) -> ParseResult<(String, SourceBuff, ByteIndex)> {
    parse_tag(data, &[0x8A])?;
    let source_buff = parse_term_arg(data)?;
    let byte_index = parse_term_arg(data)?;
    let name = parse_name_string(data)?;
    Ok((name, source_buff, byte_index))
}

fn get_named_obj_map() -> &'static ParserByTagMap<'static, NamedObj> {
    static NAMED_OBJ_MAP: spin::Once<ParserByTagMap<NamedObj>> = spin::Once::new();

    NAMED_OBJ_MAP.call_once(|| {
        ParserByTagMap::new(&[
            (&[0x14], &|data| {
                let (name, flags, body) = parse_def_method(data)?;
                Ok(NamedObj::DefMethod(name, flags, body))
            }),
            (&[0x8A], &|data| {
                let (name, source_buff, index) = parse_def_create_dword_field(data)?;
                Ok(NamedObj::DefCreateDWordField(name, source_buff, index))
            }),
            (&[EXT_OP, 0x01], &|data| {
                let (name, sync_flags) = parse_def_mutex(data)?;
                Ok(NamedObj::DefMutex(name, sync_flags))
            }),
            (&[EXT_OP, 0x80], &|data| {
                let (name, space, offset, len) = parse_def_op_region(data)?;
                Ok(NamedObj::DefOpRegion(name, space, offset, len))
            }),
            (&[EXT_OP, 0x81], &|data| {
                let (name, field_flags, field_list) = parse_def_field(data)?;
                Ok(NamedObj::DefField(name, field_flags, field_list))
            }),
            (&[EXT_OP, 0x82], &|data| {
                let (name, obj_list) = parse_def_device(data)?;
                Ok(NamedObj::DefDevice(name, obj_list))
            }),
            (&[EXT_OP, 0x83], &|data| {
                let (name, proc_id, pblk_addr, pblk_len, object_list) = parse_def_processor(data)?;
                Ok(NamedObj::DefProcessor(name, proc_id, pblk_addr, pblk_len, object_list))
            }),
        ])
    })
}

// NamedObj := DefBankField | DefCreateBitField | DefCreateByteField | DefCreateDWordField |
//             DefCreateField | DefCreateQWordField |DefCreateWordField | DefDataRegion |
//             DefOpRegion | DefPowerRes | DefProcessor | DefThermalZone |
//             DefMethod | DefMutex
fn parse_named_obj(data: &mut Data) -> ParseResult<NamedObj> {
    parse_by_tag(data, get_named_obj_map())
}

fn parse_def_return(data: &mut Data) -> ParseResult<ArgObject> {
    parse_tag(data, &[0xA4])?;
    parse_term_arg(data)
}

// AsciiChar := 0x01 - 0x7F
fn parse_ascii_char(data: &mut Data) -> ParseResult<char> {
    match data.peek_scalar::<u8>()? {
        0x01..=0x7F => Ok(data.read_scalar::<u8>()? as char),
        _ => data.error(ErrorCode::ExpectedSomethingElse)
    }
}

// NullChar := 0x00
fn parse_null_char(data: &mut Data) -> ParseResult<()> {
    match data.peek_scalar()? {
        0u8 => {
            data.read_bytes(1)?;
            Ok(())
        }
        x => data.error(ErrorCode::ExpectedDifferentScalar(x.into_scalar_value(), ScalarValue::U8(0)))
    }
}

// String := StringPrefix AsciiCharList NullChar
fn parse_string(data: &mut Data) -> ParseResult<String> {
    parse_tag(data, &[0x0D])?;
    Ok(parse_unless(data, &parse_null_char, &parse_ascii_char)?.1.into_iter().collect())
}

// DefMutex := MutexOp NameString SyncFlags
fn parse_def_mutex(data: &mut Data) -> ParseResult<(String, SyncLevel)> {
    parse_tag(data, &[EXT_OP, 0x01])?;
    let name_string = parse_name_string(data)?;
    let sync_level = data.read_scalar::<u8>()?;
    Ok((name_string, sync_level))
}

fn parse_def_release(data: &mut Data) -> ParseResult<MutexObject> {
    parse_tag(data, &[EXT_OP, 0x27])?;
    parse_super_name(data)
}

// DefElse := Nothing | <ElseOp PkgLength TermList>
fn parse_def_else(data: &mut Data) -> ParseResult<TermList> {
    match parse_tag(data, &[0xA1]) {
        Ok(_) => parse_cut(data, &parse_pkg_length, &parse_term_list),
        Err(_) => Ok(vec![])
    }
}

// DefIfElse := IfOp PkgLength Predicate TermList DefElse
fn parse_def_if_else(data: &mut Data) -> ParseResult<(Predicate, TermList, TermList)> {
    parse_tag(data, &[0xA0])?;
    let (predicate, consequent) = parse_cut(data, &parse_pkg_length, &|data| {
        let predicate = parse_term_arg(data)?;
        let consequent = parse_term_list(data)?;

        Ok((predicate, consequent))
    })?;

    let alternative = parse_def_else(data)?;
    Ok((predicate, consequent, alternative))
}

// DefWhile := WhileOp PkgLength Predicate TermList
fn parse_def_while(data: &mut Data) -> ParseResult<(Predicate, TermList)> {
    parse_tag(data, &[0xA2])?;
    parse_cut(data, &parse_pkg_length, &|data| {
        let predicate = parse_term_arg(data)?;
        let body = parse_term_list(data)?;

        Ok((predicate, body))
    })
}

fn get_type1_opcode_map() -> &'static ParserByTagMap<'static, Type1Opcode> {
    static MAP: spin::Once<ParserByTagMap<Type1Opcode>> = spin::Once::new();

    MAP.call_once(|| {
        ParserByTagMap::new(&[
            (&[0xA0], &|data| {
                let (predicate, consequent, alternative) = parse_def_if_else(data)?;
                Ok(Type1Opcode::DefIfElse(predicate, consequent, alternative))
            }),
            (&[0xA2], &|data| {
                let (predicate, body) = parse_def_while(data)?;
                Ok(Type1Opcode::DefWhile(predicate, body))
            }),
            (&[0xA4], &|data| {
                let arg_obj = parse_def_return(data)?;
                Ok(Type1Opcode::DefReturn(arg_obj))
            }),
            (&[EXT_OP, 0x27], &|data| {
                let mutex_obj = parse_def_release(data)?;
                Ok(Type1Opcode::DefRelease(mutex_obj))
            }),
        ])
    })
}

// Type1Opcode := DefBreak | DefBreakPoint | DefContinue | DefFatal | DefIfElse |
//                DefLoad | DefNoop | DefNotify | DefRelease | DefReset | DefReturn |
//                DefSignal | DefSleep | DefStall | DefUnload | DefWhile
fn parse_type1_opcode(data: &mut Data) -> ParseResult<Type1Opcode> {
    parse_by_tag(data, get_type1_opcode_map())
}

fn get_simple_name_map() -> &'static ParserByTagMap<'static, SimpleName> {
    static MAP: spin::Once<ParserByTagMap<SimpleName>> = spin::Once::new();

    MAP.call_once(|| {
        ParserByTagMap::builder()
            .add_map(get_name_string_map(), &|name_string| {
                SimpleName::NameString(name_string)
            })
            .add_map(get_arg_obj_map(), &|arg_obj| {
                SimpleName::ArgObj(arg_obj)
            })
            .add_map(get_local_obj_map(), &|local_obj| {
                SimpleName::LocalObj(local_obj)
            })
            .finish()
    })
}

// SimpleName := NameString | ArgObj | LocalObj
fn parse_simple_name(data: &mut Data) -> ParseResult<SimpleName> {
    parse_by_tag(data, get_simple_name_map())
}

fn get_type6_opcode_map() -> &'static ParserByTagMap<'static, Type6Opcode> {
    static MAP: spin::Once<ParserByTagMap<Type6Opcode>> = spin::Once::new();

    MAP.call_once(|| {
        ParserByTagMap::builder()
            .add_parser(&[0x88], &|data| {
                let (buff_pkg_str_obj, index_value, target) = parse_def_index(data)?;
                Ok(Type6Opcode::DefIndex(Box::new(buff_pkg_str_obj), Box::new(index_value), target))
            })
            .finish()
    })
}

fn parse_type6_opcode(data: &mut Data) -> ParseResult<Type6Opcode> {
    parse_by_tag(data, get_type6_opcode_map())
}

fn get_super_name_map() -> &'static ParserByTagMap<'static, SuperName> {
    static MAP: spin::Once<ParserByTagMap<SuperName>> = spin::Once::new();

    MAP.call_once(|| {
        ParserByTagMap::builder()
            .add_map(get_simple_name_map(), &|simple_name| {
                SuperName::SimpleName(simple_name)
            })
            .add_map(get_type6_opcode_map(), &|type6_opcode| {
                SuperName::Type6Opcode(Box::new(type6_opcode))
            })
            .finish()
    })
}

// SuperName := SimpleName | DebugObj | Type6Opcode
fn parse_super_name(data: &mut Data) -> ParseResult<SuperName> {
    parse_by_tag(data, get_super_name_map())
}

// DefAcquire := AcquireOp MutexObject Timeout
fn parse_def_acquire(data: &mut Data) -> ParseResult<(MutexObject, Timeout)> {
    parse_tag(data, &[EXT_OP, 0x23])?;
    let mutex_obj = parse_super_name(data)?;
    let timeout = data.read_scalar::<u16>()?;
    Ok((mutex_obj, timeout))
}

fn parse_byte_list(data: &mut Data) -> ParseResult<ByteList> {
    parse_all(data, &|data| data.read_scalar::<u8>())
}

// DefBuffer := BufferOp PkgLength BufferSize ByteList
fn parse_def_buffer(data: &mut Data) -> ParseResult<(BufferSize, ByteList)> {
    parse_tag(data, &[0x11])?;
    parse_cut(data, &parse_pkg_length, &|data| {
        let size = parse_term_arg(data)?;
        let bytes = parse_byte_list(data)?;
        Ok((size, bytes))
    })
}

// BinaryOp := Tag Operand Operand
fn parse_binary_op(data: &mut Data, tag: &[u8]) -> ParseResult<(Operand, Operand)> {
    parse_tag(data, tag)?;
    let left = parse_term_arg(data)?;
    let right = parse_term_arg(data)?;
    Ok((left, right))
}

// BinaryOp := Tag Operand Operand Target
fn parse_binary_op_with_target(data: &mut Data, tag: &[u8]) -> ParseResult<(Operand, Operand, Target)> {
    parse_tag(data, tag)?;
    let left = parse_term_arg(data)?;
    let right = parse_term_arg(data)?;
    let target = parse_target(data)?;
    Ok((left, right, target))
}

// DefStore := StoreOp TermArg SuperName
fn parse_def_store(data: &mut Data) -> ParseResult<(TermArg, SuperName)> {
    parse_tag(data, &[0x70])?;
    let arg = parse_term_arg(data)?;
    let name = parse_super_name(data)?;
    Ok((arg, name))
}

fn get_target_map() -> &'static ParserByTagMap<'static, Target> {
    static MAP: spin::Once<ParserByTagMap<Target>> = spin::Once::new();

    MAP.call_once(|| {
        ParserByTagMap::builder()
            .add_map(get_super_name_map(), &|super_name| {
                Target::SuperName(super_name)
            })
            .add_parser(&[0x00], &|data| {
                parse_null_name(data)?;
                Ok(Target::NullName)
            })
            .finish()
    })
}

fn parse_target(data: &mut Data) -> ParseResult<Target> {
    parse_by_tag(data, get_target_map())
}

// DefToHexString := ToHexStringOp Operand Target
fn parse_def_to_hex_string(data: &mut Data) -> ParseResult<(Operand, Target)> {
    parse_tag(data, &[0x98])?;
    let operand = parse_term_arg(data)?;
    let target = parse_target(data)?;
    Ok((operand, target))
}

// DefBuffer := BufferOp Operand Target
fn parse_def_to_buffer(data: &mut Data) -> ParseResult<(Operand, Target)> {
    parse_tag(data, &[0x96])?;
    let operand = parse_term_arg(data)?;
    let target = parse_target(data)?;
    Ok((operand, target))
}

// DefSizeOf := SizeOfOp SuperName
fn parse_def_size_of(data: &mut Data) -> ParseResult<SuperName> {
    parse_tag(data, &[0x87])?;
    parse_super_name(data)
}

// DefDerefOf := DerefOfOp ObjReference
fn parse_def_deref_of(data: &mut Data) -> ParseResult<ObjReference> {
    parse_tag(data, &[0x83])?;
    parse_term_arg(data)
}

// DefIndex := IndexOp BuffPkgStrObj IndexValue Target
fn parse_def_index(data: &mut Data) -> ParseResult<(BuffPkgStrObj, IndexValue, Target)> {
    parse_tag(data, &[0x88])?;
    let buff_pkg_str_obj = parse_term_arg(data)?;
    let index_value = parse_term_arg(data)?;
    let target = parse_target(data)?;
    Ok((buff_pkg_str_obj, index_value, target))
}

// DefIncrement := IncrementOp SuperName
fn parse_def_increment(data: &mut Data) -> ParseResult<SuperName> {
    parse_tag(data, &[0x75])?;
    parse_super_name(data)
}

// DefNot := NotOp Operand Target
fn parse_def_not(data: &mut Data) -> ParseResult<(Operand, Target)> {
    parse_tag(data, &[0x80])?;
    let operand = parse_term_arg(data)?;
    let target = parse_target(data)?;
    Ok((operand, target))
}

// DefLnot := LnotOp Operand
fn parse_def_lnot(data: &mut Data) -> ParseResult<Operand> {
    parse_tag(data, &[0x92])?;
    parse_term_arg(data)
}

fn get_type2_opcode_map() -> &'static ParserByTagMap<'static, Type2Opcode> {
    static TYPE2_OPCODE_MAP: spin::Once<ParserByTagMap<Type2Opcode>> = spin::Once::new();

    TYPE2_OPCODE_MAP.call_once(|| {
        ParserByTagMap::builder()
            /*.add_map_ex(get_name_string_map(), &|name, data| {
                let ctx = get_parse_context(data)?;
                let abs_path = resolve_path(&ctx.current_scope, &name);
                let args = match ctx.method_arg_count.get(&abs_path) {
                    Some(arg_count) => parse_n(data, *arg_count, &parse_term_arg),
                    None => data.error(ErrorCode::NoViableAlternative),
                }?;

                Ok(Type2Opcode::MethodInvocation(name, args))
            })*/
            .add_parser(&[0x11], &|data| {
                let (size, bytes) = parse_def_buffer(data)?;
                Ok(Type2Opcode::DefBuffer(Box::new(size), bytes))
            })
            .add_parser(&[0x12], &|data| {
                let (num_elements, elements) = parse_def_package(data)?;
                Ok(Type2Opcode::DefPackage(num_elements, elements))
            })
            .add_parser(&[0x70], &|data| {
                let (arg, name) = parse_def_store(data)?;
                Ok(Type2Opcode::DefStore(Box::new(arg), name))
            })
            .add_parser(&[0x72], &|data| {
                let (left, right, target) = parse_binary_op_with_target(data, &[0x72])?;
                Ok(Type2Opcode::DefAdd(Box::new(left), Box::new(right), target))
            })
            .add_parser(&[0x74], &|data| {
                let (left, right, target) = parse_binary_op_with_target(data, &[0x74])?;
                Ok(Type2Opcode::DefSubtract(Box::new(left), Box::new(right), target))
            })
            .add_parser(&[0x75], &|data| {
                let name = parse_def_increment(data)?;
                Ok(Type2Opcode::DefIncrement(name))
            })
            .add_parser(&[0x79], &|data| {
                let (left, right, target) = parse_binary_op_with_target(data, &[0x79])?;
                Ok(Type2Opcode::DefShiftLeft(Box::new(left), Box::new(right), target))
            })
            .add_parser(&[0x7A], &|data| {
                let (left, right, target) = parse_binary_op_with_target(data, &[0x7A])?;
                Ok(Type2Opcode::DefShiftRight(Box::new(left), Box::new(right), target))
            })
            .add_parser(&[0x7B], &|data| {
                let (left, right, target) = parse_binary_op_with_target(data, &[0x7B])?;
                Ok(Type2Opcode::DefAnd(Box::new(left), Box::new(right), target))
            })
            .add_parser(&[0x7C], &|data| {
                let (left, right, target) = parse_binary_op_with_target(data, &[0x7C])?;
                Ok(Type2Opcode::DefNAnd(Box::new(left), Box::new(right), target))
            })
            .add_parser(&[0x7D], &|data| {
                let (left, right, target) = parse_binary_op_with_target(data, &[0x7D])?;
                Ok(Type2Opcode::DefOr(Box::new(left), Box::new(right), target))
            })
            .add_parser(&[0x7E], &|data| {
                let (left, right, target) = parse_binary_op_with_target(data, &[0x7E])?;
                Ok(Type2Opcode::DefNOr(Box::new(left), Box::new(right), target))
            })
            .add_parser(&[0x7F], &|data| {
                let (left, right, target) = parse_binary_op_with_target(data, &[0x7F])?;
                Ok(Type2Opcode::DefXOr(Box::new(left), Box::new(right), target))
            })
            .add_parser(&[0x80], &|data| {
                let (operand, target) = parse_def_not(data)?;
                Ok(Type2Opcode::DefNot(Box::new(operand), target))
            })
            .add_parser(&[0x83], &|data| {
                let obj_reference = parse_def_deref_of(data)?;
                Ok(Type2Opcode::DefDerefOf(Box::new(obj_reference)))
            })
            .add_parser(&[0x87], &|data| {
                let name = parse_def_size_of(data)?;
                Ok(Type2Opcode::DefSizeOf(name))
            })
            .add_parser(&[0x88], &|data| {
                let (buff_pkg_str_obj, index_value, target) = parse_def_index(data)?;
                Ok(Type2Opcode::DefIndex(Box::new(buff_pkg_str_obj), Box::new(index_value), target))
            })
            .add_parser(&[0x90], &|data| {
                let (left, right) = parse_binary_op(data, &[0x90])?;
                Ok(Type2Opcode::DefLAnd(Box::new(left), Box::new(right)))
            })
            .add_parser(&[0x91], &|data| {
                let (left, right) = parse_binary_op(data, &[0x91])?;
                Ok(Type2Opcode::DefLOr(Box::new(left), Box::new(right)))
            })
            .add_parser(&[0x92], &|data| {
                let operand = parse_def_lnot(data)?;
                Ok(Type2Opcode::DefLNot(Box::new(operand)))
            })
            .add_parser(&[0x93], &|data| {
                let (left, right) = parse_binary_op(data, &[0x93])?;
                Ok(Type2Opcode::DefLEqual(Box::new(left), Box::new(right)))
            })
            .add_parser(&[0x94], &|data| {
                let (left, right) = parse_binary_op(data, &[0x94])?;
                Ok(Type2Opcode::DefLGreater(Box::new(left), Box::new(right)))
            })
            .add_parser(&[0x96], &|data| {
                let (operand, target) = parse_def_to_buffer(data)?;
                Ok(Type2Opcode::DefToBuffer(Box::new(operand), target))
            })
            .add_parser(&[0x98], &|data| {
                let (operand, target) = parse_def_to_hex_string(data)?;
                Ok(Type2Opcode::DefToHexString(Box::new(operand), target))
            })
            .add_parser(&[0x95], &|data| {
                let (left, right) = parse_binary_op(data, &[0x95])?;
                Ok(Type2Opcode::DefLLess(Box::new(left), Box::new(right)))
            })
            .add_parser(&[EXT_OP, 0x23], &|data| {
                let (mutex_obj, timeout) = parse_def_acquire(data)?;
                Ok(Type2Opcode::DefAcquire(mutex_obj, timeout))
            })
            .finish()
    })
}

// Type2Opcode := DefAcquire | DefAdd | DefAnd | DefBuffer | DefConcat |
//                DefConcatRes | DefCondRefOf | DefCopyObject | DefDecrement |
//                DefDerefOf | DefDivide | DefFindSetLeftBit | DefFindSetRightBit |
//                DefFromBCD | DefIncrement | DefIndex | DefLAnd | DefLEqual |
//                DefLGreater | DefLGreaterEqual | DefLLess | DefLLessEqual | DefMid |
//                DefLNot | DefLNotEqual | DefLoadTable | DefLOr | DefMatch | DefMod |
//                DefMultiply | DefNAnd | DefNOr | DefNot | DefObjectType | DefOr |
//                DefPackage | DefVarPackage | DefRefOf | DefShiftLeft | DefShiftRight |
//                DefSizeOf | DefStore | DefSubtract | DefTimer | DefToBCD | DefToBuffer |
//                DefToDecimalString | DefToHexString | DefToInteger | DefToString |
//                DefWait | DefXOr | MethodInvocation
fn parse_type2_opcode(data: &mut Data) -> ParseResult<Type2Opcode> {
    parse_by_tag(data, get_type2_opcode_map())
}

fn get_computational_data_map() -> &'static ParserByTagMap<'static, ComputationalData> {
    static MAP: spin::Once<ParserByTagMap<ComputationalData>> = spin::Once::new();

    MAP.call_once(|| {
        ParserByTagMap::new(&[
            (&[0x0], &|data| {
                parse_tag(data, &[0x0])?;
                Ok(ComputationalData::ConstObj(ConstObj::Zero))
            }),
            (&[0x1], &|data| {
                parse_tag(data, &[0x1])?;
                Ok(ComputationalData::ConstObj(ConstObj::One))
            }),
            (&[0xFF], &|data| {
                parse_tag(data, &[0xFF])?;
                Ok(ComputationalData::ConstObj(ConstObj::Ones))
            }),
            (&[0xA], &|data| {
                parse_tag(data, &[0xA])?;
                Ok(ComputationalData::ByteConst(data.read_scalar::<u8>()?))
            }),
            (&[0xB], &|data| {
                parse_tag(data, &[0xB])?;
                Ok(ComputationalData::WordConst(data.read_scalar::<u16>()?))
            }),
            (&[0xC], &|data| {
                parse_tag(data, &[0xC])?;
                Ok(ComputationalData::DWordConst(data.read_scalar::<u32>()?))
            }),
            (&[0xD], &|data| {
                Ok(ComputationalData::String(parse_string(data)?))
            }),
            (&[0x11], &|data| {
                let (size, bytes) = parse_def_buffer(data)?;
                Ok(ComputationalData::DefBuffer(Box::new(size), bytes))
            }),
        ])
    })
}

// ComputationalData := ByteConst | WordConst | DWordConst | QWordConst | String |
//                      ConstObj | RevisionOp | DefBuffer
// ConstObj := ZeroOp | OneOp | OnesOp
fn parse_computational_data(data: &mut Data) -> ParseResult<ComputationalData> {
    parse_by_tag(data, get_computational_data_map())
}

fn get_package_element_map() -> &'static ParserByTagMap<'static, PackageElement> {
    static MAP: spin::Once<ParserByTagMap<PackageElement>> = spin::Once::new();

    MAP.call_once(|| {
        ParserByTagMap::builder()
            .add_map(get_name_string_map(), &|name_string| {
                PackageElement::NameString(name_string)
            })
            .add_map(get_data_ref_object_map(), &|data_ref_object| {
                PackageElement::DataRefObject(data_ref_object)
            })
            .finish()
    })
}

// PackageElement := DataRefObject | NameString
fn parse_package_element(data: &mut Data) -> ParseResult<PackageElement> {
    parse_by_tag(data, get_package_element_map())
}

// PackageElementList := Nothing | <PackageElement PackageElementList>
fn parse_package_element_list(data: &mut Data) -> ParseResult<PackageElementList> {
    parse_all(data, &parse_package_element)
}

// DefPackage := PackageOp PkgLength NumElements PackageElementList
fn parse_def_package(data: &mut Data) -> ParseResult<(NumElements, PackageElementList)> {
    parse_tag(data, &[0x12])?;
    parse_cut(data, &parse_pkg_length, &|data| {
        let num_elements = data.read_scalar::<u8>()?;
        let elements = parse_package_element_list(data)?;

        Ok((num_elements, elements))
    })
}

fn get_data_object_map() -> &'static ParserByTagMap<'static, DataObject> {
    static MAP: spin::Once<ParserByTagMap<DataObject>> = spin::Once::new();

    MAP.call_once(|| {
        ParserByTagMap::builder()
            .add_map(get_computational_data_map(), &|computational_data| {
                DataObject::ComputationalData(computational_data)
            })
            .add_parser(&[0x12], &|data| {
                let (num_elements, elements) = parse_def_package(data)?;
                Ok(DataObject::DefPackage(num_elements, elements))
            })
            .finish()
    })
}

// DataObject := ComputationalData | DefPackage | DefVarPackage
fn parse_data_object(data: &mut Data) -> ParseResult<DataObject> {
    parse_by_tag(data, get_data_object_map())
}

fn get_data_ref_object_map() -> &'static ParserByTagMap<'static, DataRefObject> {
    static MAP: spin::Once<ParserByTagMap<DataRefObject>> = spin::Once::new();

    MAP.call_once(|| {
        ParserByTagMap::builder()
            .add_map(get_data_object_map(), &|data_object| {
                DataRefObject::DataObject(data_object)
            })
            .finish()
    })
}

// DataRefObject := DataObject | ObjectReference | DDBHandle
fn parse_data_ref_object(data: &mut Data) -> ParseResult<DataRefObject> {
    parse_by_tag(data, get_data_ref_object_map())
}

// DefName := NameOp NameString DataRefObject
fn parse_def_name(data: &mut Data) -> ParseResult<(String, DataRefObject)> {
    parse_tag(data, &[0x08])?;

    let name_string = parse_name_string(data)?;
    let data_ref_object = parse_data_ref_object(data)?;

    Ok((name_string, data_ref_object))
}

fn get_name_space_modifier_obj_map() -> &'static ParserByTagMap<'static, NameSpaceModifierObj> {
    static NAME_SPACE_MODIFIER_OBJ_MAP: spin::Once<ParserByTagMap<NameSpaceModifierObj>> = spin::Once::new();

    NAME_SPACE_MODIFIER_OBJ_MAP.call_once(|| {
        ParserByTagMap::new(&[
            (&[0x8][..], &|data| {
                let (name, data_ref_object) = parse_def_name(data)?;
                Ok(NameSpaceModifierObj::DefName(name, data_ref_object))
            }),
            (&[0x10][..], &|data| {
                let (name, term_list) = parse_def_scope(data)?;
                Ok(NameSpaceModifierObj::DefScope(name, term_list))
            }),
        ])
    })
}

// NameSpaceModifierObj := DefAlias | DefName | DefScope
fn parse_name_space_modifier_obj(data: &mut Data) -> ParseResult<NameSpaceModifierObj> {
    parse_by_tag(data, get_name_space_modifier_obj_map())
}

pub fn parse_term_list(data: &mut Data) -> ParseResult<TermList> {
    parse_all(data, &parse_term_obj)
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_context_eq() {
        let parse_context1 = ParseContext {
            method_arg_count: Rc::new({
                let mut map = BTreeMap::new();
                map.insert(String::from("hello"), 5);
                map
            }),
            current_scope: String::from("_SB_"),
        };
        let parse_context2 = ParseContext {
            method_arg_count: Rc::new({
                let mut map = BTreeMap::new();
                map.insert(String::from("hello"), 5);
                map
            }),
            current_scope: String::from("_SB_"),
        };
        let parse_context3 = ParseContext {
            method_arg_count: Rc::new({
                let mut map = BTreeMap::new();
                map.insert(String::from("hello"), 6);
                map
            }),
            current_scope: String::from("_SB_"),
        };

        let ctx1: &Context = &parse_context1;
        let ctx2: &Context = &parse_context2;
        let ctx3: &Context = &parse_context3;

        assert!(ctx1.eq(ctx2));
        assert!(!ctx1.eq(ctx3));
        assert!(!ctx2.eq(ctx3));
    }

    #[test]
    fn test_pkg_length() {
        let mut d1 = Data::new(&[0x16, 0x10][..]);
        let mut d2 = Data::new(&[0xC6, 0x1, 0x0, 0x0, 0x10][..]);

        assert_eq!(parse_pkg_length(&mut d1), Ok(0x15));
        assert_eq!(d1.len(), 1);

        assert_eq!(parse_pkg_length(&mut d2), Ok(0x12));
        assert_eq!(d2.len(), 1);
    }

    #[test]
    fn test_pkg_length_ex() {
        let mut d1 = Data::new(&[0x16, 0x10][..]);
        let mut d2 = Data::new(&[0xC6, 0x1, 0x0, 0x0, 0x10][..]);

        assert_eq!(parse_pkg_length_ex(&mut d1), Ok(0x16));
        assert_eq!(d1.len(), 1);

        assert_eq!(parse_pkg_length_ex(&mut d2), Ok(0x16));
        assert_eq!(d2.len(), 1);
    }

    fn _parse(data: &mut Data, bytes: &[u8]) -> TermList {
        let output = parse_term_list(data)
            .unwrap_or_else(|error| {
                panic!("could not parse input: {:#?}: {:02x?}", error, &bytes[error.start..]);
            });

        assert_eq!(data.len(), 0);

        output
    }

    fn parse(bytes: &[u8]) -> TermList {
        let mut data = Data::new(bytes);
        _parse(&mut data, bytes)
    }

    fn parse_with_context(bytes: &[u8], context: ParseContext) -> TermList {
        let mut data = Data::new_with_context(bytes, Box::new(context));
        _parse(&mut data, bytes)
    }

    #[test]
    fn test_dword_const() {
        let input = [
            0x10, 0x16, 0x5f, 0x53, 0x42, 0x5f, 0x5b, 0x82,
            0x0f, 0x50, 0x43, 0x49, 0x30, 0x08, 0x5f, 0x48,
            0x49, 0x44, 0x0c, 0x41, 0xd0, 0x0a, 0x03,
        ];

        let output = vec![
            TermObj::NameSpaceModifierObj(
                NameSpaceModifierObj::DefScope(
                    String::from("_SB_"),
                    vec![
                        TermObj::NamedObj(
                            NamedObj::DefDevice(
                                String::from("PCI0"),
                                vec![
                                    Object::NameSpaceModifierObj(
                                        NameSpaceModifierObj::DefName(
                                            String::from("_HID"),
                                            DataRefObject::DataObject(
                                                DataObject::ComputationalData(
                                                    ComputationalData::DWordConst(0x030AD041)
                                                )
                                            ),
                                        )
                                    )
                                ],
                            )
                        )
                    ],
                )
            )
        ];

        assert_eq!(parse(&input), output);
    }

    #[test]
    fn test_region() {
        let input = [
            0x10, 0x16, 0x5f, 0x53, 0x42, 0x5f, 0x5b, 0x82,
            0x0f, 0x52, 0x54, 0x43, 0x30, 0x5B, 0x80, 0x43,
            0x4D, 0x53, 0x30, 0x05, 0x00, 0x0a, 0x40,
        ];

        let output = vec![
            TermObj::NameSpaceModifierObj(
                NameSpaceModifierObj::DefScope(
                    String::from("_SB_"),
                    vec![
                        TermObj::NamedObj(
                            NamedObj::DefDevice(
                                String::from("RTC0"),
                                vec![
                                    Object::NamedObj(
                                        NamedObj::DefOpRegion(
                                            String::from("CMS0"),
                                            RegionSpace::SystemCMOS,
                                            TermArg::DataObject(
                                                DataObject::ComputationalData(
                                                    ComputationalData::ConstObj(
                                                        ConstObj::Zero,
                                                    )
                                                )
                                            ),
                                            TermArg::DataObject(
                                                DataObject::ComputationalData(
                                                    ComputationalData::ByteConst(0x40)
                                                )
                                            ),
                                        )
                                    )
                                ],
                            )
                        )
                    ],
                )
            )
        ];

        assert_eq!(parse(&input), output)
    }

    #[test]
    fn test_field() {
        let input = [
            0x10, 0x49, 0x04, 0x5f, 0x53, 0x42, 0x5f, 0x5b,
            0x82, 0x41, 0x04, 0x52, 0x54, 0x43, 0x30, 0x08,
            0x5f, 0x48, 0x49, 0x44, 0x0c, 0x41, 0xd0, 0x0b,
            0x00, 0x5b, 0x80, 0x43, 0x4d, 0x53, 0x30, 0x05,
            0x00, 0x0a, 0x40, 0x5b, 0x81, 0x25, 0x43, 0x4d,
            0x53, 0x30, 0x01, 0x52, 0x54, 0x53, 0x45, 0x08,
            0x00, 0x08, 0x52, 0x54, 0x4d, 0x4e, 0x08, 0x00,
            0x08, 0x52, 0x54, 0x48, 0x52, 0x08, 0x00, 0x08,
            0x52, 0x54, 0x44, 0x59, 0x08, 0x52, 0x54, 0x44,
            0x45, 0x08,
        ];

        let output = vec![
            TermObj::NameSpaceModifierObj(
                NameSpaceModifierObj::DefScope(
                    String::from("_SB_"),
                    vec![
                        TermObj::NamedObj(
                            NamedObj::DefDevice(
                                String::from("RTC0"),
                                vec![
                                    Object::NameSpaceModifierObj(
                                        NameSpaceModifierObj::DefName(
                                            String::from("_HID"),
                                            DataRefObject::DataObject(
                                                DataObject::ComputationalData(
                                                    ComputationalData::DWordConst(0xBD041)
                                                )
                                            ),
                                        )
                                    ),
                                    Object::NamedObj(
                                        NamedObj::DefOpRegion(
                                            String::from("CMS0"),
                                            RegionSpace::SystemCMOS,
                                            TermArg::DataObject(
                                                DataObject::ComputationalData(
                                                    ComputationalData::ConstObj(
                                                        ConstObj::Zero,
                                                    )
                                                )
                                            ),
                                            TermArg::DataObject(
                                                DataObject::ComputationalData(
                                                    ComputationalData::ByteConst(0x40)
                                                )
                                            ),
                                        )
                                    ),
                                    Object::NamedObj(
                                        NamedObj::DefField(
                                            String::from("CMS0"),
                                            FieldFlags {
                                                access_type: FieldFlagsAccessType::ByteAccessType,
                                                lock_rule: FieldFlagsLockRule::NoLock,
                                                update_rule: FieldFlagsUpdateRule::Preserve,
                                            },
                                            vec![
                                                FieldElement::NamedField(String::from("RTSE"), 8),
                                                FieldElement::ReservedField(8),
                                                FieldElement::NamedField(String::from("RTMN"), 8),
                                                FieldElement::ReservedField(8),
                                                FieldElement::NamedField(String::from("RTHR"), 8),
                                                FieldElement::ReservedField(8),
                                                FieldElement::NamedField(String::from("RTDY"), 8),
                                                FieldElement::NamedField(String::from("RTDE"), 8),
                                            ],
                                        )
                                    )
                                ],
                            )
                        )
                    ],
                )
            )
        ];

        assert_eq!(parse(&input), output);
    }

    #[test]
    fn test_method() {
        let input = [
            0x10, 0x20, 0x5f, 0x53, 0x42, 0x5f, 0x5b, 0x82,
            0x19, 0x50, 0x53, 0x32, 0x4b, 0x08, 0x5f, 0x48,
            0x49, 0x44, 0x0c, 0x41, 0xd0, 0x03, 0x03, 0x14,
            0x09, 0x5f, 0x53, 0x54, 0x41, 0x00, 0xa4, 0x0a,
            0x0f,
        ];

        let output = vec![
            TermObj::NameSpaceModifierObj(
                NameSpaceModifierObj::DefScope(
                    String::from("_SB_"),
                    vec![
                        TermObj::NamedObj(
                            NamedObj::DefDevice(
                                String::from("PS2K"),
                                vec![
                                    Object::NameSpaceModifierObj(
                                        NameSpaceModifierObj::DefName(
                                            String::from("_HID"),
                                            DataRefObject::DataObject(
                                                DataObject::ComputationalData(
                                                    ComputationalData::DWordConst(0x303D041)
                                                )
                                            ),
                                        )
                                    ),
                                    Object::NamedObj(
                                        NamedObj::DefMethod(
                                            String::from("_STA"),
                                            MethodFlags {
                                                arg_count: 0,
                                                serialize_flag: MethodSerializeFlag::NotSerialized,
                                                sync_level: 0,
                                            },
                                            MethodBody::TermList(vec![
                                                TermObj::Type1Opcode(
                                                    Type1Opcode::DefReturn(
                                                        TermArg::DataObject(
                                                            DataObject::ComputationalData(
                                                                ComputationalData::ByteConst(0x0F)
                                                            )
                                                        )
                                                    )
                                                )
                                            ]),
                                        )
                                    )
                                ],
                            )
                        )
                    ],
                )
            )
        ];

        assert_eq!(parse(&input), output);
    }

    #[test]
    fn test_package() {
        let input = [
            0x10, 0x1f, 0x5f, 0x53, 0x42, 0x5f, 0x5b, 0x82,
            0x18, 0x42, 0x41, 0x54, 0x58, 0x08, 0x5f, 0x48,
            0x49, 0x44, 0x0c, 0x41, 0xd0, 0x0c, 0x0a, 0x08,
            0x50, 0x42, 0x49, 0x46, 0x12, 0x03, 0x02, 0x01,
        ];

        let output = vec![
            TermObj::NameSpaceModifierObj(
                NameSpaceModifierObj::DefScope(
                    String::from("_SB_"),
                    vec![
                        TermObj::NamedObj(
                            NamedObj::DefDevice(
                                String::from("BATX"),
                                vec![
                                    Object::NameSpaceModifierObj(
                                        NameSpaceModifierObj::DefName(
                                            String::from("_HID"),
                                            DataRefObject::DataObject(
                                                DataObject::ComputationalData(
                                                    ComputationalData::DWordConst(0xA0CD041)
                                                )
                                            ),
                                        )
                                    ),
                                    Object::NameSpaceModifierObj(
                                        NameSpaceModifierObj::DefName(
                                            String::from("PBIF"),
                                            DataRefObject::DataObject(
                                                DataObject::DefPackage(
                                                    2,
                                                    vec![
                                                        PackageElement::DataRefObject(
                                                            DataRefObject::DataObject(
                                                                DataObject::ComputationalData(
                                                                    ComputationalData::ConstObj(
                                                                        ConstObj::One
                                                                    )
                                                                )
                                                            )
                                                        )
                                                    ],
                                                )
                                            ),
                                        )
                                    ),
                                ],
                            )
                        )
                    ],
                )
            )
        ];

        assert_eq!(parse(&input), output);
    }

    #[test]
    fn test_qemu_debug() {
        let input = [
            0x10, 0x49, 0x04, 0x5c, 0x00, 0x5b, 0x80, 0x44,
            0x42, 0x47, 0x5f, 0x01, 0x0b, 0x02, 0x04, 0x01,
            0x5b, 0x81, 0x0b, 0x44, 0x42, 0x47, 0x5f, 0x01,
            0x44, 0x42, 0x47, 0x42, 0x08, 0x14, 0x2c, 0x44,
            0x42, 0x55, 0x47, 0x01, 0x98, 0x68, 0x60, 0x96,
            0x60, 0x60, 0x74, 0x87, 0x60, 0x01, 0x61, 0x70,
            0x00, 0x62, 0xa2, 0x10, 0x95, 0x62, 0x61, 0x70,
            0x83, 0x88, 0x60, 0x62, 0x00, 0x44, 0x42, 0x47,
            0x42, 0x75, 0x62, 0x70, 0x0a, 0x0a, 0x44, 0x42,
            0x47, 0x42,
        ];

        let output = vec![
            TermObj::NameSpaceModifierObj(
                NameSpaceModifierObj::DefScope(
                    String::from("\\"),
                    vec![
                        TermObj::NamedObj(
                            NamedObj::DefOpRegion(
                                String::from("DBG_"),
                                RegionSpace::SystemIO,
                                TermArg::DataObject(
                                    DataObject::ComputationalData(
                                        ComputationalData::WordConst(0x0402)
                                    )
                                ),
                                TermArg::DataObject(
                                    DataObject::ComputationalData(
                                        ComputationalData::ConstObj(ConstObj::One)
                                    )
                                ),
                            )
                        ),
                        TermObj::NamedObj(
                            NamedObj::DefField(
                                String::from("DBG_"),
                                FieldFlags {
                                    access_type: FieldFlagsAccessType::ByteAccessType,
                                    lock_rule: FieldFlagsLockRule::NoLock,
                                    update_rule: FieldFlagsUpdateRule::Preserve,
                                },
                                vec![
                                    FieldElement::NamedField(String::from("DBGB"), 8)
                                ],
                            )
                        ),
                        TermObj::NamedObj(
                            NamedObj::DefMethod(
                                String::from("DBUG"),
                                MethodFlags {
                                    arg_count: 1,
                                    serialize_flag: MethodSerializeFlag::NotSerialized,
                                    sync_level: 0,
                                },
                                MethodBody::TermList(vec![
                                    TermObj::Type2Opcode(
                                        Type2Opcode::DefToHexString(
                                            Box::new(TermArg::ArgObj(ArgObj::Arg0)),
                                            Target::SuperName(
                                                SuperName::SimpleName(
                                                    SimpleName::LocalObj(LocalObj::Local0)
                                                )
                                            ),
                                        )
                                    ),
                                    TermObj::Type2Opcode(
                                        Type2Opcode::DefToBuffer(
                                            Box::new(TermArg::LocalObj(LocalObj::Local0)),
                                            Target::SuperName(
                                                SuperName::SimpleName(
                                                    SimpleName::LocalObj(LocalObj::Local0)
                                                )
                                            ),
                                        )
                                    ),
                                    TermObj::Type2Opcode(
                                        Type2Opcode::DefSubtract(
                                            Box::new(
                                                TermArg::Type2Opcode(
                                                    Type2Opcode::DefSizeOf(
                                                        SuperName::SimpleName(
                                                            SimpleName::LocalObj(LocalObj::Local0)
                                                        )
                                                    )
                                                )
                                            ),
                                            Box::new(
                                                TermArg::DataObject(
                                                    DataObject::ComputationalData(
                                                        ComputationalData::ConstObj(ConstObj::One)
                                                    )
                                                )
                                            ),
                                            Target::SuperName(
                                                SuperName::SimpleName(
                                                    SimpleName::LocalObj(LocalObj::Local1)
                                                ),
                                            ),
                                        )
                                    ),
                                    TermObj::Type2Opcode(
                                        Type2Opcode::DefStore(
                                            Box::new(TermArg::DataObject(
                                                DataObject::ComputationalData(
                                                    ComputationalData::ConstObj(ConstObj::Zero)
                                                )
                                            )),
                                            SuperName::SimpleName(
                                                SimpleName::LocalObj(LocalObj::Local2)
                                            ),
                                        )
                                    ),
                                    TermObj::Type1Opcode(
                                        Type1Opcode::DefWhile(
                                            TermArg::Type2Opcode(
                                                Type2Opcode::DefLLess(
                                                    Box::new(TermArg::LocalObj(LocalObj::Local2)),
                                                    Box::new(TermArg::LocalObj(LocalObj::Local1)),
                                                )
                                            ),
                                            vec![
                                                TermObj::Type2Opcode(
                                                    Type2Opcode::DefStore(
                                                        Box::new(TermArg::Type2Opcode(
                                                            Type2Opcode::DefDerefOf(
                                                                Box::new(TermArg::Type2Opcode(
                                                                    Type2Opcode::DefIndex(
                                                                        Box::new(TermArg::LocalObj(LocalObj::Local0)),
                                                                        Box::new(TermArg::LocalObj(LocalObj::Local2)),
                                                                        Target::NullName,
                                                                    )
                                                                ))
                                                            )
                                                        )),
                                                        SuperName::SimpleName(
                                                            SimpleName::NameString(String::from("DBGB"))
                                                        ),
                                                    )
                                                ),
                                                TermObj::Type2Opcode(
                                                    Type2Opcode::DefIncrement(
                                                        SuperName::SimpleName(
                                                            SimpleName::LocalObj(LocalObj::Local2)
                                                        )
                                                    ),
                                                )
                                            ],
                                        )
                                    ),
                                    TermObj::Type2Opcode(
                                        Type2Opcode::DefStore(
                                            Box::new(TermArg::DataObject(
                                                DataObject::ComputationalData(
                                                    ComputationalData::ByteConst(0x0A)
                                                )
                                            )),
                                            SuperName::SimpleName(
                                                SimpleName::NameString(String::from("DBGB"))
                                            ),
                                        )
                                    )
                                ]),
                            )
                        )
                    ],
                )
            )
        ];

        assert_eq!(parse(&input), output);
    }

    #[test]
    fn test_bool_ops() {
        let input = [
            0x10, 0x47, 0x05, 0x5c, 0x00, 0x14, 0x42, 0x05,
            0x42, 0x4f, 0x4f, 0x4c, 0x02, 0x70, 0x93, 0x68,
            0x00, 0x60, 0x70, 0x92, 0x93, 0x68, 0x01, 0x61,
            0x70, 0x94, 0x68, 0xff, 0x62, 0x70, 0x92, 0x95,
            0x68, 0x69, 0x63, 0x70, 0x95, 0x68, 0x69, 0x64,
            0x70, 0x92, 0x94, 0x68, 0x69, 0x65, 0x7a, 0x68,
            0x69, 0x66, 0x79, 0x68, 0x69, 0x67, 0x70, 0x90,
            0x60, 0x80, 0x61, 0x00, 0x60, 0x70, 0x91, 0x62,
            0x80, 0x63, 0x00, 0x61, 0x7f, 0x64, 0x65, 0x62,
            0x70, 0x7c, 0x66, 0x67, 0x00, 0x63, 0x70, 0x7e,
            0x60, 0x80, 0x61, 0x00, 0x00, 0x64, 0xa4, 0x64,
        ];

        let output = vec![
            TermObj::NameSpaceModifierObj(NameSpaceModifierObj::DefScope(
                String::from("\\"),
                vec![
                    TermObj::NamedObj(NamedObj::DefMethod(
                        String::from("BOOL"),
                        MethodFlags {
                            arg_count: 2,
                            serialize_flag: MethodSerializeFlag::NotSerialized,
                            sync_level: 0,
                        },
                        MethodBody::TermList(vec![
                            TermObj::Type2Opcode(Type2Opcode::DefStore(
                                Box::new(TermArg::Type2Opcode(Type2Opcode::DefLEqual(
                                    Box::new(TermArg::ArgObj(ArgObj::Arg0)),
                                    Box::new(TermArg::DataObject(
                                        DataObject::ComputationalData(
                                            ComputationalData::ConstObj(ConstObj::Zero)
                                        )
                                    )),
                                ))),
                                SuperName::SimpleName(SimpleName::LocalObj(LocalObj::Local0)),
                            )),
                            TermObj::Type2Opcode(Type2Opcode::DefStore(
                                Box::new(TermArg::Type2Opcode(Type2Opcode::DefLNot(
                                    Box::new(TermArg::Type2Opcode(Type2Opcode::DefLEqual(
                                        Box::new(TermArg::ArgObj(ArgObj::Arg0)),
                                        Box::new(TermArg::DataObject(
                                            DataObject::ComputationalData(
                                                ComputationalData::ConstObj(ConstObj::One)
                                            )
                                        )),
                                    )))
                                ))),
                                SuperName::SimpleName(SimpleName::LocalObj(LocalObj::Local1)),
                            )),
                            TermObj::Type2Opcode(Type2Opcode::DefStore(
                                Box::new(TermArg::Type2Opcode(Type2Opcode::DefLGreater(
                                    Box::new(TermArg::ArgObj(ArgObj::Arg0)),
                                    Box::new(TermArg::DataObject(
                                        DataObject::ComputationalData(
                                            ComputationalData::ConstObj(ConstObj::Ones)
                                        )
                                    )),
                                ))),
                                SuperName::SimpleName(SimpleName::LocalObj(LocalObj::Local2)),
                            )),
                            TermObj::Type2Opcode(Type2Opcode::DefStore(
                                Box::new(TermArg::Type2Opcode(Type2Opcode::DefLNot(
                                    Box::new(TermArg::Type2Opcode(Type2Opcode::DefLLess(
                                        Box::new(TermArg::ArgObj(ArgObj::Arg0)),
                                        Box::new(TermArg::ArgObj(ArgObj::Arg1)),
                                    )))
                                ))),
                                SuperName::SimpleName(SimpleName::LocalObj(LocalObj::Local3)),
                            )),
                            TermObj::Type2Opcode(Type2Opcode::DefStore(
                                Box::new(TermArg::Type2Opcode(Type2Opcode::DefLLess(
                                    Box::new(TermArg::ArgObj(ArgObj::Arg0)),
                                    Box::new(TermArg::ArgObj(ArgObj::Arg1)),
                                ))),
                                SuperName::SimpleName(SimpleName::LocalObj(LocalObj::Local4)),
                            )),
                            TermObj::Type2Opcode(Type2Opcode::DefStore(
                                Box::new(TermArg::Type2Opcode(Type2Opcode::DefLNot(
                                    Box::new(TermArg::Type2Opcode(Type2Opcode::DefLGreater(
                                        Box::new(TermArg::ArgObj(ArgObj::Arg0)),
                                        Box::new(TermArg::ArgObj(ArgObj::Arg1)),
                                    )))
                                ))),
                                SuperName::SimpleName(SimpleName::LocalObj(LocalObj::Local5)),
                            )),
                            TermObj::Type2Opcode(Type2Opcode::DefShiftRight(
                                Box::new(TermArg::ArgObj(ArgObj::Arg0)),
                                Box::new(TermArg::ArgObj(ArgObj::Arg1)),
                                Target::SuperName(
                                    SuperName::SimpleName(SimpleName::LocalObj(LocalObj::Local6))
                                ),
                            )),
                            TermObj::Type2Opcode(Type2Opcode::DefShiftLeft(
                                Box::new(TermArg::ArgObj(ArgObj::Arg0)),
                                Box::new(TermArg::ArgObj(ArgObj::Arg1)),
                                Target::SuperName(
                                    SuperName::SimpleName(SimpleName::LocalObj(LocalObj::Local7))
                                ),
                            )),
                            TermObj::Type2Opcode(Type2Opcode::DefStore(
                                Box::new(TermArg::Type2Opcode(Type2Opcode::DefLAnd(
                                    Box::new(TermArg::LocalObj(LocalObj::Local0)),
                                    Box::new(TermArg::Type2Opcode(Type2Opcode::DefNot(
                                        Box::new(TermArg::LocalObj(LocalObj::Local1)),
                                        Target::NullName,
                                    ))),
                                ))),
                                SuperName::SimpleName(SimpleName::LocalObj(LocalObj::Local0)),
                            )),
                            TermObj::Type2Opcode(Type2Opcode::DefStore(
                                Box::new(TermArg::Type2Opcode(Type2Opcode::DefLOr(
                                    Box::new(TermArg::LocalObj(LocalObj::Local2)),
                                    Box::new(TermArg::Type2Opcode(Type2Opcode::DefNot(
                                        Box::new(TermArg::LocalObj(LocalObj::Local3)),
                                        Target::NullName,
                                    ))),
                                ))),
                                SuperName::SimpleName(SimpleName::LocalObj(LocalObj::Local1)),
                            )),
                            TermObj::Type2Opcode(Type2Opcode::DefXOr(
                                Box::new(TermArg::LocalObj(LocalObj::Local4)),
                                Box::new(TermArg::LocalObj(LocalObj::Local5)),
                                Target::SuperName(SuperName::SimpleName(SimpleName::LocalObj(LocalObj::Local2))),
                            )),
                            TermObj::Type2Opcode(Type2Opcode::DefStore(
                                Box::new(TermArg::Type2Opcode(Type2Opcode::DefNAnd(
                                    Box::new(TermArg::LocalObj(LocalObj::Local6)),
                                    Box::new(TermArg::LocalObj(LocalObj::Local7)),
                                    Target::NullName,
                                ))),
                                SuperName::SimpleName(SimpleName::LocalObj(LocalObj::Local3)),
                            )),
                            TermObj::Type2Opcode(Type2Opcode::DefStore(
                                Box::new(TermArg::Type2Opcode(Type2Opcode::DefNOr(
                                    Box::new(TermArg::LocalObj(LocalObj::Local0)),
                                    Box::new(TermArg::Type2Opcode(Type2Opcode::DefNot(
                                        Box::new(TermArg::LocalObj(LocalObj::Local1)),
                                        Target::NullName,
                                    ))),
                                    Target::NullName,
                                ))),
                                SuperName::SimpleName(SimpleName::LocalObj(LocalObj::Local4)),
                            )),
                            TermObj::Type1Opcode(Type1Opcode::DefReturn(TermArg::LocalObj(LocalObj::Local4))),
                        ]),
                    ))
                ],
            ))
        ];

        assert_eq!(parse(&input), output);
    }

    #[test]
    fn test_if_else() {
        let input = [
            0x10, 0x15, 0x5c, 0x00, 0x14, 0x11, 0x45, 0x51,
            0x5f, 0x5f, 0x02, 0xa0, 0x06, 0x93, 0x68, 0x69,
            0xa4, 0x01, 0xa1, 0x03, 0xa4, 0x00,
        ];

        let output = vec![
            TermObj::NameSpaceModifierObj(NameSpaceModifierObj::DefScope(
                String::from("\\"),
                vec![
                    TermObj::NamedObj(NamedObj::DefMethod(
                        String::from("EQ__"),
                        MethodFlags {
                            arg_count: 2,
                            serialize_flag: MethodSerializeFlag::NotSerialized,
                            sync_level: 0,
                        },
                        MethodBody::TermList(vec![
                            TermObj::Type1Opcode(Type1Opcode::DefIfElse(
                                TermArg::Type2Opcode(Type2Opcode::DefLEqual(
                                    Box::new(TermArg::ArgObj(ArgObj::Arg0)),
                                    Box::new(TermArg::ArgObj(ArgObj::Arg1)),
                                )),
                                vec![
                                    TermObj::Type1Opcode(Type1Opcode::DefReturn(
                                        TermArg::DataObject(DataObject::ComputationalData(
                                            ComputationalData::ConstObj(ConstObj::One)
                                        ))
                                    ))
                                ],
                                vec![
                                    TermObj::Type1Opcode(Type1Opcode::DefReturn(
                                        TermArg::DataObject(DataObject::ComputationalData(
                                            ComputationalData::ConstObj(ConstObj::Zero)
                                        ))
                                    ))
                                ],
                            ))
                        ]),
                    ))
                ],
            ))
        ];

        assert_eq!(parse(&input), output);
    }

    #[test]
    fn test_buffer() {
        let input = [
            0x10, 0x10, 0x5c, 0x00, 0x08, 0x42, 0x55, 0x46,
            0x5f, 0x11, 0x07, 0x0a, 0x04, 0x00, 0x01, 0x02,
            0x03,
        ];

        let output = vec![
            TermObj::NameSpaceModifierObj(NameSpaceModifierObj::DefScope(
                String::from("\\"),
                vec![
                    TermObj::NameSpaceModifierObj(NameSpaceModifierObj::DefName(
                        String::from("BUF_"),
                        DataRefObject::DataObject(DataObject::ComputationalData(
                            ComputationalData::DefBuffer(
                                Box::new(TermArg::DataObject(DataObject::ComputationalData(
                                    ComputationalData::ByteConst(4)
                                ))),
                                vec![0, 1, 2, 3],
                            )
                        )),
                    ))
                ],
            ))
        ];

        assert_eq!(parse(&input), output);
    }

    #[test]
    fn test_parent_prefix() {
        let input = [
            0x10, 0x43, 0x04, 0x5f, 0x53, 0x42, 0x5f, 0x5b,
            0x82, 0x19, 0x50, 0x58, 0x31, 0x33, 0x08, 0x5f,
            0x41, 0x44, 0x52, 0x0c, 0x03, 0x00, 0x01, 0x00,
            0x5b, 0x80, 0x50, 0x31, 0x33, 0x43, 0x02, 0x00,
            0x0a, 0xff, 0x5b, 0x82, 0x20, 0x49, 0x53, 0x41,
            0x5f, 0x08, 0x5f, 0x41, 0x44, 0x52, 0x0c, 0x00,
            0x00, 0x01, 0x00, 0x5b, 0x81, 0x0f, 0x5e, 0x2e,
            0x50, 0x58, 0x31, 0x33, 0x50, 0x31, 0x33, 0x43,
            0x00, 0x00, 0x48, 0x2f,
        ];

        let output = vec![
            TermObj::NameSpaceModifierObj(NameSpaceModifierObj::DefScope(
                String::from("_SB_"),
                vec![
                    TermObj::NamedObj(NamedObj::DefDevice(
                        String::from("PX13"),
                        vec![
                            Object::NameSpaceModifierObj(NameSpaceModifierObj::DefName(
                                String::from("_ADR"),
                                DataRefObject::DataObject(DataObject::ComputationalData(
                                    ComputationalData::DWordConst(0x10003),
                                )),
                            )),
                            Object::NamedObj(NamedObj::DefOpRegion(
                                String::from("P13C"),
                                RegionSpace::PciConfig,
                                TermArg::DataObject(DataObject::ComputationalData(
                                    ComputationalData::ConstObj(ConstObj::Zero)
                                )),
                                TermArg::DataObject(DataObject::ComputationalData(
                                    ComputationalData::ByteConst(0xFF)
                                )),
                            ))
                        ],
                    )),
                    TermObj::NamedObj(NamedObj::DefDevice(
                        String::from("ISA_"),
                        vec![
                            Object::NameSpaceModifierObj(NameSpaceModifierObj::DefName(
                                String::from("_ADR"),
                                DataRefObject::DataObject(DataObject::ComputationalData(
                                    ComputationalData::DWordConst(0x10000),
                                )),
                            )),
                            Object::NamedObj(NamedObj::DefField(
                                String::from("^PX13.P13C"),
                                FieldFlags {
                                    access_type: FieldFlagsAccessType::AnyAccessType,
                                    lock_rule: FieldFlagsLockRule::NoLock,
                                    update_rule: FieldFlagsUpdateRule::Preserve,
                                },
                                vec![
                                    FieldElement::ReservedField(0x2F8)
                                ],
                            ))
                        ],
                    ))
                ],
            ))
        ];

        assert_eq!(parse(&input), output);
    }

    #[test]
    fn test_multi_name_path() {
        let input = [
            0x10, 0x12, 0x5f, 0x53, 0x42, 0x5f, 0x5b, 0x82, 0x0b, 0x50, 0x43, 0x49,
            0x30, 0x08, 0x5f, 0x41, 0x44, 0x52, 0x00, 0x10, 0x1b, 0x2e, 0x5f, 0x53, 0x42, 0x5f, 0x50, 0x43,
            0x49, 0x30, 0x5b, 0x82, 0x0f, 0x49, 0x53, 0x41, 0x5f, 0x08, 0x5f, 0x41, 0x44, 0x52, 0x0c, 0x00,
            0x00, 0x01, 0x00, 0x10, 0x20, 0x2f, 0x03, 0x5f, 0x53, 0x42, 0x5f, 0x50, 0x43, 0x49, 0x30, 0x49,
            0x53, 0x41, 0x5f, 0x5b, 0x82, 0x0f, 0x48, 0x50, 0x45, 0x54, 0x08, 0x5f, 0x48, 0x49, 0x44, 0x0c,
            0x41, 0xd0, 0x01, 0x03,
        ];

        let output = vec![
            TermObj::NameSpaceModifierObj(NameSpaceModifierObj::DefScope(
                String::from("_SB_"),
                vec![
                    TermObj::NamedObj(NamedObj::DefDevice(
                        String::from("PCI0"),
                        vec![
                            Object::NameSpaceModifierObj(NameSpaceModifierObj::DefName(
                                String::from("_ADR"),
                                DataRefObject::DataObject(DataObject::ComputationalData(
                                    ComputationalData::ConstObj(ConstObj::Zero)
                                )),
                            )),
                        ],
                    )),
                ],
            )),
            TermObj::NameSpaceModifierObj(NameSpaceModifierObj::DefScope(
                String::from("_SB_.PCI0"),
                vec![
                    TermObj::NamedObj(NamedObj::DefDevice(
                        String::from("ISA_"),
                        vec![
                            Object::NameSpaceModifierObj(NameSpaceModifierObj::DefName(
                                String::from("_ADR"),
                                DataRefObject::DataObject(DataObject::ComputationalData(
                                    ComputationalData::DWordConst(0x10000)
                                )),
                            )),
                        ],
                    )),
                ],
            )),
            TermObj::NameSpaceModifierObj(NameSpaceModifierObj::DefScope(
                String::from("_SB_.PCI0.ISA_"),
                vec![
                    TermObj::NamedObj(NamedObj::DefDevice(
                        String::from("HPET"),
                        vec![
                            Object::NameSpaceModifierObj(NameSpaceModifierObj::DefName(
                                String::from("_HID"),
                                DataRefObject::DataObject(DataObject::ComputationalData(
                                    ComputationalData::DWordConst(0x301D041)
                                )),
                            )),
                        ],
                    )),
                ],
            ))
        ];

        assert_eq!(parse(&input), output);
    }

    #[test]
    fn test_mutex() {
        let input = [
            0x10, 0x21, 0x5f, 0x53, 0x42, 0x5f, 0x5b, 0x01, 0x4c, 0x43, 0x4b, 0x5f, 0x00,
            0x14, 0x14, 0x41, 0x5f, 0x5f, 0x5f, 0x00, 0x5b, 0x23, 0x4c, 0x43, 0x4b, 0x5f,
            0xff, 0xff, 0x5b, 0x27, 0x4c, 0x43, 0x4b, 0x5f,
        ];

        let output = vec![
            TermObj::NameSpaceModifierObj(NameSpaceModifierObj::DefScope(
                String::from("_SB_"),
                vec![
                    TermObj::NamedObj(NamedObj::DefMutex(String::from("LCK_"), 0)),
                    TermObj::NamedObj(NamedObj::DefMethod(
                        String::from("A___"),
                        MethodFlags { sync_level: 0, arg_count: 0, serialize_flag: MethodSerializeFlag::NotSerialized },
                        MethodBody::TermList(vec![
                            TermObj::Type2Opcode(Type2Opcode::DefAcquire(
                                SuperName::SimpleName(SimpleName::NameString(String::from("LCK_"))),
                                0xFFFF,
                            )),
                            TermObj::Type1Opcode(Type1Opcode::DefRelease(
                                SuperName::SimpleName(SimpleName::NameString(String::from("LCK_")))
                            )),
                        ]),
                    ))
                ],
            )),
        ];

        assert_eq!(parse(&input), output);
    }

    #[test]
    fn test_qemu_prt_method() {
        let input = [
            0x14, 0x44, 0x0a, 0x5f, 0x50, 0x52, 0x54, 0x00,
            0x70, 0x12, 0x02, 0x80, 0x60, 0x70, 0x00, 0x61,
            0xa2, 0x42, 0x09, 0x95, 0x61, 0x0a, 0x80, 0x70,
            0x7a, 0x61, 0x0a, 0x02, 0x00, 0x62, 0x70, 0x7b,
            0x72, 0x61, 0x62, 0x00, 0x0a, 0x03, 0x00, 0x63,
            0xa0, 0x10, 0x93, 0x63, 0x00, 0x70, 0x12, 0x09,
            0x04, 0x00, 0x00, 0x4c, 0x4e, 0x4b, 0x44, 0x00,
            0x64, 0xa0, 0x24, 0x93, 0x63, 0x01, 0xa0, 0x11,
            0x93, 0x61, 0x0a, 0x04, 0x70, 0x12, 0x09, 0x04,
            0x00, 0x00, 0x4c, 0x4e, 0x4b, 0x53, 0x00, 0x64,
            0xa1, 0x0d, 0x70, 0x12, 0x09, 0x04, 0x00, 0x00,
            0x4c, 0x4e, 0x4b, 0x41, 0x00, 0x64, 0xa0, 0x11,
            0x93, 0x63, 0x0a, 0x02, 0x70, 0x12, 0x09, 0x04,
            0x00, 0x00, 0x4c, 0x4e, 0x4b, 0x42, 0x00, 0x64,
            0xa0, 0x11, 0x93, 0x63, 0x0a, 0x03, 0x70, 0x12,
            0x09, 0x04, 0x00, 0x00, 0x4c, 0x4e, 0x4b, 0x43,
            0x00, 0x64, 0x70, 0x7d, 0x79, 0x62, 0x0a, 0x10,
            0x00, 0x0b, 0xff, 0xff, 0x00, 0x88, 0x64, 0x00,
            0x00, 0x70, 0x7b, 0x61, 0x0a, 0x03, 0x00, 0x88,
            0x64, 0x01, 0x00, 0x70, 0x64, 0x88, 0x60, 0x61,
            0x00, 0x75, 0x61, 0xa4, 0x60,
        ];

        parse(&input[..]);
    }

    #[test]
    fn test_create_dword_field() {
        let input = [
            0x10, 0x3c, 0x5f, 0x53, 0x42, 0x5f, 0x14, 0x36,
            0x49, 0x51, 0x43, 0x52, 0x09, 0x08, 0x50, 0x52,
            0x52, 0x30, 0x11, 0x0e, 0x0a, 0x0b, 0x89, 0x06,
            0x00, 0x09, 0x01, 0x00, 0x00, 0x00, 0x00, 0x79,
            0x00, 0x8a, 0x50, 0x52, 0x52, 0x30, 0x0a, 0x05,
            0x50, 0x52, 0x52, 0x49, 0xa0, 0x0b, 0x95, 0x68,
            0x0a, 0x80, 0x70, 0x68, 0x50, 0x52, 0x52, 0x49,
            0xa4, 0x50, 0x52, 0x52, 0x30,
        ];

        let output = vec![
            TermObj::NameSpaceModifierObj(NameSpaceModifierObj::DefScope(
                String::from("_SB_"),
                vec![
                    TermObj::NamedObj(NamedObj::DefMethod(
                        String::from("IQCR"),
                        MethodFlags {
                            arg_count: 1,
                            serialize_flag: MethodSerializeFlag::Serialized,
                            sync_level: 0,
                        },
                        MethodBody::TermList(vec![
                            TermObj::NameSpaceModifierObj(NameSpaceModifierObj::DefName(
                                String::from("PRR0"),
                                DataRefObject::DataObject(DataObject::ComputationalData(
                                    ComputationalData::DefBuffer(
                                        Box::new(TermArg::DataObject(DataObject::ComputationalData(
                                            ComputationalData::ByteConst(11)
                                        ))),
                                        vec![
                                            0x89, 0x06, 0x00, 0x09,
                                            0x01, 0x00, 0x00, 0x00,
                                            0x00, 0x79, 0x00,
                                        ],
                                    ),
                                )),
                            )),
                            TermObj::NamedObj(NamedObj::DefCreateDWordField(
                                String::from("PRRI"),
                                TermArg::NameString(String::from("PRR0")),
                                TermArg::DataObject(DataObject::ComputationalData(
                                    ComputationalData::ByteConst(5)
                                )),
                            )),
                            TermObj::Type1Opcode(Type1Opcode::DefIfElse(
                                TermArg::Type2Opcode(Type2Opcode::DefLLess(
                                    Box::new(TermArg::ArgObj(ArgObj::Arg0)),
                                    Box::new(TermArg::DataObject(DataObject::ComputationalData(
                                        ComputationalData::ByteConst(128)
                                    ))),
                                )),
                                vec![
                                    TermObj::Type2Opcode(Type2Opcode::DefStore(
                                        Box::new(TermArg::ArgObj(ArgObj::Arg0)),
                                        SuperName::SimpleName(SimpleName::NameString(
                                            String::from("PRRI")
                                        )),
                                    ))
                                ],
                                vec![],
                            )),
                            TermObj::Type1Opcode(Type1Opcode::DefReturn(
                                TermArg::NameString(String::from("PRR0")),
                            ))
                        ]),
                    ))
                ],
            ))
        ];

        assert_eq!(parse(&input), output);
    }

    #[test]
    fn test_defer_method_invocation() {
        let input = [
            0x10, 0x1b, 0x5f, 0x53, 0x42, 0x5f, 0x14, 0x08,
            0x41, 0x5f, 0x5f, 0x5f, 0x01, 0xa4, 0x68, 0x14,
            0x0c, 0x42, 0x5f, 0x5f, 0x5f, 0x01, 0xa4, 0x41,
            0x5f, 0x5f, 0x5f, 0x68,
        ];

        let output = vec![
            TermObj::NameSpaceModifierObj(NameSpaceModifierObj::DefScope(
                String::from("_SB_"),
                vec![
                    TermObj::NamedObj(NamedObj::DefMethod(
                        String::from("A___"),
                        MethodFlags {
                            arg_count: 1,
                            serialize_flag: MethodSerializeFlag::NotSerialized,
                            sync_level: 0,
                        },
                        MethodBody::TermList(vec![
                            TermObj::Type1Opcode(Type1Opcode::DefReturn(
                                TermArg::ArgObj(ArgObj::Arg0)
                            )),
                        ]),
                    )),
                    TermObj::NamedObj(NamedObj::DefMethod(
                        String::from("B___"),
                        MethodFlags {
                            arg_count: 1,
                            serialize_flag: MethodSerializeFlag::NotSerialized,
                            sync_level: 0,
                        },
                        MethodBody::ByteCode(vec![
                            0xa4, 0x41, 0x5f, 0x5f, 0x5f, 0x68,
                        ]),
                    ))
                ],
            ))
        ];

        assert_eq!(parse(&input), output);
    }

    #[test]
    #[ignore]
    fn test_deferred_method_invocation() {
        let input = [
            0xa4, 0x41, 0x5f, 0x5f, 0x5f, 0x68,
        ];

        let context = ParseContext {
            method_arg_count: Rc::new({
                let mut map = BTreeMap::new();
                map.insert(String::from("_SB_.A___"), 1);
                map
            }),
            current_scope: String::from("_SB_"),
        };

        let output = vec![
            TermObj::Type1Opcode(Type1Opcode::DefReturn(
                TermArg::Type2Opcode(Type2Opcode::MethodInvocation(
                    String::from("A___"),
                    vec![
                        TermArg::ArgObj(ArgObj::Arg0),
                    ],
                ))
            ))
        ];

        assert_eq!(parse_with_context(&input, context), output);
    }

    #[test]
    fn test_string() {
        let input = [
            0x10, 0x17, 0x5f, 0x53, 0x42, 0x5f, 0x08, 0x53,
            0x54, 0x52, 0x5f, 0x0d, 0x48, 0x65, 0x6c, 0x6c,
            0x6f, 0x20, 0x57, 0x6f, 0x72, 0x6c, 0x64, 0x00,
        ];

        let output = vec![
            TermObj::NameSpaceModifierObj(NameSpaceModifierObj::DefScope(
                String::from("_SB_"),
                vec![
                    TermObj::NameSpaceModifierObj(NameSpaceModifierObj::DefName(
                        String::from("STR_"),
                        DataRefObject::DataObject(DataObject::ComputationalData(
                            ComputationalData::String(String::from("Hello World"))
                        )),
                    ))
                ],
            ))
        ];

        assert_eq!(parse(&input), output);
    }

    #[test]
    fn test_processor() {
        let input = [
            0x10, 0x12, 0x5f, 0x53, 0x42, 0x5f, 0x5b, 0x83,
            0x0b, 0x43, 0x30, 0x30, 0x30, 0x00, 0x00, 0x00,
            0x00, 0x00, 0x00,
        ];

        let output = vec![
            TermObj::NameSpaceModifierObj(NameSpaceModifierObj::DefScope(
                String::from("_SB_"),
                vec![
                    TermObj::NamedObj(NamedObj::DefProcessor(
                        String::from("C000"),
                        0, 0, 0, vec![],
                    ))
                ],
            ))
        ];

        assert_eq!(parse(&input), output);
    }

    #[test]
    fn test_s5_package() {
        let input = [
            0x08, 0x5F, 0x53, 0x35, 0x5F, 0x12, 0x06, 0x04,
            0x00, 0x00, 0x00, 0x00,
        ];

        let output = vec![
            TermObj::NameSpaceModifierObj(NameSpaceModifierObj::DefName(
                String::from("_S5_"),
                DataRefObject::DataObject(DataObject::DefPackage(
                    4,
                    vec![
                        PackageElement::DataRefObject(DataRefObject::DataObject(
                            DataObject::ComputationalData(ComputationalData::ConstObj(
                                ConstObj::Zero
                            ))
                        )),
                        PackageElement::DataRefObject(DataRefObject::DataObject(
                            DataObject::ComputationalData(ComputationalData::ConstObj(
                                ConstObj::Zero
                            ))
                        )),
                        PackageElement::DataRefObject(DataRefObject::DataObject(
                            DataObject::ComputationalData(ComputationalData::ConstObj(
                                ConstObj::Zero
                            ))
                        )),
                        PackageElement::DataRefObject(DataRefObject::DataObject(
                            DataObject::ComputationalData(ComputationalData::ConstObj(
                                ConstObj::Zero
                            ))
                        )),
                    ],
                )),
            )),
        ];

        assert_eq!(parse(&input), output);
    }
}
