use crate::stdlib::vec::Vec;

use super::*;

fn is_path_prefixed_with(path: &str, prefix: &str) -> bool {
    let break_char = path.chars().nth(prefix.len());
    path.starts_with(prefix) && break_char == Some('.')
}

fn slice_sub_path(path: &str) -> Option<&str> {
    let dot_index = path.find('.')?;
    Some(&path[dot_index+1..])
}

pub trait AmlValue: ::core::fmt::Debug {
    fn as_term_list(&self) -> Option<&TermList> {
        None
    }

    fn as_def_package(&self) -> Option<(&NumElements, &PackageElementList)> {
        None
    }

    fn as_u32(&self) -> Option<u32> {
        None
    }

    fn get(&self, path: &str) -> Option<&AmlValue> {
        let terms = self.as_term_list()?;
        let sub_path = slice_sub_path(path);

        for term in terms.iter() {
            match term {
                TermObj::NameSpaceModifierObj(NameSpaceModifierObj::DefScope(name, terms)) => {
                    if path == name {
                        return Some(terms);
                    }

                    if is_path_prefixed_with(path, name) {
                        if let Some(val) = term.get(sub_path.unwrap()) {
                            return Some(val);
                        }
                    }
                }
                TermObj::NameSpaceModifierObj(NameSpaceModifierObj::DefName(name, obj)) => {
                    if path == name {
                        return Some(obj);
                    }

                    if is_path_prefixed_with(path, name) {
                        if let Some(val) = term.get(sub_path.unwrap()) {
                            return Some(val);
                        }
                    }
                }
                _ => {}
            }
        }

        None
    }
}

impl AmlValue for Vec<TermObj> {
    fn as_term_list(&self) -> Option<&TermList> {
        Some(&self)
    }
}

impl AmlValue for TermObj {
    fn as_term_list(&self) -> Option<&TermList> {
        match self {
            TermObj::NameSpaceModifierObj(NameSpaceModifierObj::DefScope(_, terms)) => Some(&terms),
            _ => None,
        }
    }
}

impl AmlValue for DataRefObject {
    fn as_def_package(&self) -> Option<(&NumElements, &PackageElementList)> {
        match self {
            DataRefObject::DataObject(data_object) => data_object.as_def_package(),
            _ => None,
        }
    }

    fn as_u32(&self) -> Option<u32> {
        match self {
            DataRefObject::DataObject(data_object) => data_object.as_u32(),
            _ => None,
        }
    }
}

impl AmlValue for DataObject {
    fn as_def_package(&self) -> Option<(&NumElements, &PackageElementList)> {
        match self {
            DataObject::DefPackage(num_elements, element_list) => Some((&num_elements, &element_list)),
            _ => None,
        }
    }

    fn as_u32(&self) -> Option<u32> {
        match self {
            DataObject::ComputationalData(comp_data) => comp_data.as_u32(),
            _ => None,
        }
    }
}

impl AmlValue for PackageElement {
    fn as_u32(&self) -> Option<u32> {
        match self {
            PackageElement::DataRefObject(data_ref_object) => data_ref_object.as_u32(),
            _ => None,
        }
    }
}

impl AmlValue for ComputationalData {
    fn as_u32(&self) -> Option<u32> {
        match self {
            ComputationalData::ByteConst(byte) => Some(*byte as u32),
            ComputationalData::WordConst(word) => Some(*word as u32),
            ComputationalData::DWordConst(dword) => Some(*dword),
            ComputationalData::ConstObj(const_obj) => const_obj.as_u32(),
            _ => None,
        }
    }
}

impl AmlValue for ConstObj {
    fn as_u32(&self) -> Option<u32> {
        match self {
            ConstObj::Zero => Some(0),
            ConstObj::One => Some(1),
            ConstObj::Ones => Some(!0),
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_term_list_as_term_list() {
        let term_list = vec![
            TermObj::Type2Opcode(Type2Opcode::MethodInvocation(
                String::from("TEST"),
                vec![]
            )),
        ];

        let val: &AmlValue = &term_list;

        assert_eq!(val.as_term_list(), Some(&term_list));
    }

    #[test]
    fn test_get_scope() {
        let term_list = vec![
            TermObj::NameSpaceModifierObj(NameSpaceModifierObj::DefScope(
                String::from("_SB_"),
                vec![
                    TermObj::NameSpaceModifierObj(NameSpaceModifierObj::DefScope(
                        String::from("TEST"),
                        vec![]
                    )),
                ]
            )),
        ];

        let val: &AmlValue = &term_list;

        assert_eq!(val.get("_SB_").unwrap().as_term_list(), Some(&vec![
            TermObj::NameSpaceModifierObj(NameSpaceModifierObj::DefScope(
                String::from("TEST"),
                vec![]
            )),
        ]));
    }

    #[test]
    fn test_get_sub_scope() {
        let term_list = vec![
            TermObj::NameSpaceModifierObj(NameSpaceModifierObj::DefScope(
                String::from("_SB_"),
                vec![
                    TermObj::NameSpaceModifierObj(NameSpaceModifierObj::DefScope(
                        String::from("TEST"),
                        vec![
                            TermObj::NameSpaceModifierObj(NameSpaceModifierObj::DefScope(
                                String::from("TST2"),
                                vec![]
                            )),
                        ]
                    )),
                ]
            )),
        ];

        let val: &AmlValue = &term_list;

        assert_eq!(val.get("_SB_.TEST").unwrap().as_term_list(), Some(&vec![
            TermObj::NameSpaceModifierObj(NameSpaceModifierObj::DefScope(
                String::from("TST2"),
                vec![]
            )),
        ]));
    }
}
