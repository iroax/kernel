use super::super::super::stdlib::boxed::Box;
use super::super::super::stdlib::string::String;
use super::super::super::stdlib::vec::Vec;

#[derive(Debug, PartialEq)]
pub enum ConstObj {
    Zero,
    One,
    Ones,
}

#[derive(Debug, PartialEq)]
pub enum Object {
    NameSpaceModifierObj(NameSpaceModifierObj),
    NamedObj(NamedObj),
}

#[derive(Debug, PartialEq)]
pub enum ComputationalData {
    ByteConst(u8),
    WordConst(u16),
    DWordConst(u32),
    String(String),
    ConstObj(ConstObj),
    DefBuffer(Box<BufferSize>, ByteList),
}

#[derive(Debug, PartialEq)]
pub enum DataObject {
    ComputationalData(ComputationalData),
    DefPackage(NumElements, PackageElementList),
}

#[derive(Debug, PartialEq)]
pub enum DataRefObject {
    DataObject(DataObject),
    ObjectReference,
    DDBHandle,
}

pub type ObjectList = Vec<Object>;
pub type TermList = Vec<TermObj>;
pub type ArgObject = TermArg;
pub type Predicate = TermArg;

#[derive(Debug, PartialEq)]
pub enum Type1Opcode {
    DefReturn(ArgObject),
    DefWhile(Predicate, TermList),
    DefIfElse(Predicate, TermList, TermList),
    DefRelease(MutexObject),
}

#[derive(Debug, PartialEq)]
pub enum Type6Opcode {
    DefIndex(Box<BuffPkgStrObj>, Box<IndexValue>, Target)
}

#[derive(Debug, PartialEq)]
pub enum PackageElement {
    DataRefObject(DataRefObject),
    NameString(String),
}

pub type PackageElementList = Vec<PackageElement>;

#[derive(Debug, PartialEq)]
pub enum SimpleName {
    NameString(String),
    ArgObj(ArgObj),
    LocalObj(LocalObj),
}

#[derive(Debug, PartialEq)]
pub enum SuperName {
    SimpleName(SimpleName),
    Type6Opcode(Box<Type6Opcode>),
}

pub type Operand = TermArg;

#[derive(Debug, PartialEq)]
pub enum Target {
    SuperName(SuperName),
    NullName,
}

pub type ObjReference = TermArg;
pub type BuffPkgStrObj = TermArg;
pub type IndexValue = TermArg;
pub type ShiftCount = TermArg;
pub type BufferSize = TermArg;
pub type ByteList = Vec<u8>;
pub type Timeout = u16;
pub type MutexObject = SuperName;
pub type NumElements = u8;
pub type TermArgList = Vec<TermArg>;

#[derive(Debug, PartialEq)]
pub enum Type2Opcode {
    DefPackage(NumElements, PackageElementList),
    DefStore(Box<TermArg>, SuperName),
    DefToHexString(Box<Operand>, Target),
    DefToBuffer(Box<Operand>, Target),
    DefSubtract(Box<Operand>, Box<Operand>, Target),
    DefSizeOf(SuperName),
    DefLLess(Box<Operand>, Box<Operand>),
    DefLGreater(Box<Operand>, Box<Operand>),
    DefLEqual(Box<Operand>, Box<Operand>),
    DefLAnd(Box<Operand>, Box<Operand>),
    DefLOr(Box<Operand>, Box<Operand>),
    DefLNot(Box<Operand>),
    DefNot(Box<Operand>, Target),
    DefDerefOf(Box<ObjReference>),
    DefIndex(Box<BuffPkgStrObj>, Box<IndexValue>, Target),
    DefIncrement(SuperName),
    DefShiftLeft(Box<Operand>, Box<ShiftCount>, Target),
    DefShiftRight(Box<Operand>, Box<ShiftCount>, Target),
    DefXOr(Box<Operand>, Box<Operand>, Target),
    DefNAnd(Box<Operand>, Box<Operand>, Target),
    DefAnd(Box<Operand>, Box<Operand>, Target),
    DefOr(Box<Operand>, Box<Operand>, Target),
    DefAdd(Box<Operand>, Box<Operand>, Target),
    DefNOr(Box<Operand>, Box<Operand>, Target),
    DefBuffer(Box<BufferSize>, ByteList),
    DefAcquire(MutexObject, Timeout),
    MethodInvocation(String, TermArgList),
}

#[derive(Debug, PartialEq)]
pub enum TermObj {
    NameSpaceModifierObj(NameSpaceModifierObj),
    NamedObj(NamedObj),
    Type1Opcode(Type1Opcode),
    Type2Opcode(Type2Opcode),
}

#[derive(Debug, PartialEq)]
pub enum NameSpaceModifierObj {
    DefScope(String, TermList),
    DefName(String, DataRefObject),
}

#[derive(Debug, PartialEq)]
pub enum RegionSpace {
    SystemMemory,
    SystemIO,
    PciConfig,
    EmbeddedControl,
    SMBus,
    SystemCMOS,
    PciBarTarget,
    Ipmi,
    UserDefined(u8),
}

#[derive(Debug, PartialEq)]
pub enum TermArg {
    Type2Opcode(Type2Opcode),
    DataObject(DataObject),
    ArgObj(ArgObj),
    LocalObj(LocalObj),
    NameString(String),
}

#[derive(Debug, PartialEq)]
pub enum FieldFlagsAccessType {
    AnyAccessType,
    ByteAccessType,
    WordAccessType,
    DWordAccessType,
    QWordAccessType,
    BufferAccessType,
    Reserved(u8),
}

#[derive(Debug, PartialEq)]
pub enum FieldFlagsLockRule {
    Lock,
    NoLock,
}

#[derive(Debug, PartialEq)]
pub enum FieldFlagsUpdateRule {
    Preserve,
    WriteAsOnes,
    WriteAsZeros,
}

#[derive(Debug, PartialEq)]
pub struct FieldFlags {
    pub access_type: FieldFlagsAccessType,
    pub lock_rule: FieldFlagsLockRule,
    pub update_rule: FieldFlagsUpdateRule,
}

#[derive(Debug, PartialEq)]
pub enum FieldElement {
    NamedField(String, usize),
    ReservedField(usize), // in bits
}

pub type FieldList = Vec<FieldElement>;

#[derive(Debug, PartialEq)]
pub enum MethodSerializeFlag {
    NotSerialized,
    Serialized,
}

#[derive(Debug, PartialEq)]
pub struct MethodFlags {
    pub arg_count: u8,
    pub serialize_flag: MethodSerializeFlag,
    pub sync_level: u8,
}

pub type SyncLevel = u8;
pub type ByteIndex = TermArg;
pub type SourceBuff = TermArg;

#[derive(Debug, PartialEq)]
pub enum MethodBody {
    TermList(TermList),
    ByteCode(Vec<u8>),
}

pub type ProcID = u8;
pub type PblkAddr = u32;
pub type PblkLen = u8;
pub type RegionOffset = TermArg;
pub type RegionLen = TermArg;

#[derive(Debug, PartialEq)]
pub enum NamedObj {
    DefDevice(String, ObjectList),
    DefOpRegion(String, RegionSpace, RegionOffset, RegionLen),
    DefField(String, FieldFlags, FieldList),
    DefMethod(String, MethodFlags, MethodBody),
    DefMutex(String, SyncLevel),
    DefCreateDWordField(String, SourceBuff, ByteIndex),
    DefProcessor(String, ProcID, PblkAddr, PblkLen, ObjectList),
}

#[derive(Debug, PartialEq, Clone)]
pub enum ArgObj {
    Arg0,
    Arg1,
    Arg2,
    Arg3,
    Arg4,
    Arg5,
    Arg6,
}

#[derive(Debug, PartialEq, Clone)]
pub enum LocalObj {
    Local0,
    Local1,
    Local2,
    Local3,
    Local4,
    Local5,
    Local6,
    Local7,
}
