/** ACPI PM1 Control Register (within PM1a_CNT_BLK or PM1A_CNT_BLK) */
pub const ACPI_PM1_CNT: u16 = 0;

/** Sleep enable */
pub const ACPI_PM1_CNT_SLP_EN: u16 = 1 << 13;

