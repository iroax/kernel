use spin::Once;

use crate::mem::addr::PhysAddr;

pub mod aml;
pub mod structs;
pub mod consts;

static ACPI: Once<Acpi> = Once::new();

#[derive(Debug)]
pub struct Acpi {
    pub structs: structs::AcpiStructs,
    pub dsdt_aml: aml::Aml,
}

pub fn init(rsdp_start_addr: PhysAddr, rsdp_end_addr: PhysAddr) {
    let structs = structs::read(rsdp_start_addr, rsdp_end_addr)
        .expect("could not load ACPI structures");

    let dsdt_aml = aml::parse(structs.dsdt.raw_data.as_slice()).unwrap_or_else(|error| {
        panic!("could not parse DSDT: {:#?}", error);
    });

    ACPI.call_once(|| {
        Acpi {
            structs,
            dsdt_aml,
        }
    });
}

pub fn get_acpi() -> &'static Acpi {
    ACPI.r#try().expect("ACPI not initialized")
}
