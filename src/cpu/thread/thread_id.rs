use core::sync::atomic::{AtomicUsize, ATOMIC_USIZE_INIT, Ordering};

pub type ThreadId = usize;

static GLOBAL_COUNTER: AtomicUsize = ATOMIC_USIZE_INIT;

pub fn next_thread_id() -> ThreadId {
    let mut prev = GLOBAL_COUNTER.load(Ordering::Relaxed);
    loop {
        let old_value = GLOBAL_COUNTER.compare_and_swap(prev, prev + 1, Ordering::Relaxed);
        if old_value == prev {
            return prev;
        } else {
            prev = old_value;
        }
    }
}
