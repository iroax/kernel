use core::any::Any;
use core::marker::PhantomData;

use crate::cpu::local_id::get_local_id;
use crate::cpu::thread::get_idle_fn;
use crate::stdlib::boxed::Box;
use crate::stdlib::collections::{BTreeMap, BTreeSet};
use crate::stdlib::collections::VecDeque;
use crate::stdlib::string::String;
use crate::stdlib::sync::{Arc, Weak};

use super::{JoinHandle, ThreadResult};
use super::thread_id::{next_thread_id, ThreadId};

pub trait ThreadImp: ::core::fmt::Debug {
    fn as_any(&self) -> &Any;
    fn as_any_mut(&mut self) -> &mut Any;
}

#[derive(PartialEq)]
enum ThreadState {
    New,
    Running,
    Finished,
}

struct Thread {
    id: ThreadId,
    name: Option<String>,
    imp: Box<ThreadImp>,
    result: Weak<ThreadResult>,
    state: ThreadState,
}

enum RunningThread {
    IdleThread(Box<ThreadImp>),
    BusyThread(ThreadId),
}

pub struct Scheduler {
    threads: BTreeMap<ThreadId, Thread>,
    thread_queue: VecDeque<ThreadId>,
    running_thread_by_cpu_id: BTreeMap<u32, RunningThread>,
}

impl Scheduler {
    pub fn new() -> Scheduler {
        Scheduler {
            threads: BTreeMap::new(),
            thread_queue: VecDeque::new(),
            running_thread_by_cpu_id: BTreeMap::new(),
        }
    }

    pub fn spawn_thread<F, T>(&mut self, f: F, name: Option<String>, stack_size: Option<usize>) -> JoinHandle<T>
        where F: FnOnce() -> T, F: Send + 'static, T: Send + 'static {
        let result = Arc::new(ThreadResult::new());
        let thread_id = next_thread_id();
        let thread = Thread {
            id: thread_id,
            name,
            imp: create_thread_imp(f, stack_size),
            result: Arc::downgrade(&result),
            state: ThreadState::New,
        };

        self.threads.insert(thread_id, thread);
        self.thread_queue.push_back(thread_id);

        JoinHandle {
            _phantom: PhantomData,
            result,
        }
    }

    pub fn context_switch<FSave, FRestore>(&mut self, save_state: FSave, restore_state: FRestore)
        where FSave: FnOnce(&mut (ThreadImp + 'static)), FRestore: FnOnce(&(ThreadImp + 'static)) {
        let cpu_id = get_local_id();

        // if current thread is finished, remove all references
        if let Some(finished_thread_id) = {
            if let Some(RunningThread::BusyThread(thread_id)) = self.running_thread_by_cpu_id.get(&cpu_id) {
                if let Some(thread) = self.threads.get(thread_id) {
                    if thread.state == ThreadState::Finished {
                        Some(*thread_id)
                    } else { None }
                } else { None }
            } else { None }
        } {
            self.running_thread_by_cpu_id.remove(&cpu_id);
            self.threads.remove(&finished_thread_id);
            self.thread_queue.retain(|thread_id| *thread_id != finished_thread_id);
        }

        if self.thread_queue.len() == 0 {
            let current_running_thread = self.running_thread_by_cpu_id.get(&cpu_id);

            // if there's nothing else to do and current thread is idle, continue
            if let Some(RunningThread::IdleThread(_)) = current_running_thread {
                return;
            }

            // if there's nothing else to do and current thread is unknown, craft idle thread and switch
            if let None = current_running_thread {
                let idle_fn = get_idle_fn().expect("idle fn not set");
                let idle_thread_imp = create_thread_imp(idle_fn, None);
                let running_thread = RunningThread::IdleThread(idle_thread_imp);

                self.running_thread_by_cpu_id.insert(cpu_id, running_thread);

                if let Some(RunningThread::IdleThread(idle_thread_imp)) = self.running_thread_by_cpu_id.get(&cpu_id) {
                    restore_state(idle_thread_imp.as_ref());
                    return;
                }
            }

            // if there's nothing else to do and current thread is busy, continue
            return;
        }

        // yay, there's something else to do
        let next_thread_id = self.thread_queue.pop_front().unwrap();

        // if current thread is busy, save its state and return it to queue
        if let Some(RunningThread::BusyThread(thread_id)) = self.running_thread_by_cpu_id.get(&cpu_id) {
            let thread = self.threads.get_mut(thread_id).unwrap();
            let thread_imp = thread.imp.as_mut();

            save_state(thread_imp);
            self.thread_queue.push_back(*thread_id);
        }

        // switch to next thread
        let running_thread = RunningThread::BusyThread(next_thread_id);
        self.running_thread_by_cpu_id.insert(cpu_id, running_thread);
        let mut thread = self.threads.get_mut(&next_thread_id).unwrap();
        thread.state = ThreadState::Running;

        let thread_imp = thread.imp.as_ref();
        restore_state(thread_imp);
    }

    pub fn end_of_thread<T>(&mut self, result: T) where T: Send + 'static {
        let cpu_id = get_local_id();
        let current_running_thread = self.running_thread_by_cpu_id.get(&cpu_id)
            .expect("cannot end thread that has not been spawned");

        if let RunningThread::BusyThread(thread_id) = current_running_thread {
            let mut thread = self.threads.get_mut(thread_id).unwrap();

            // mark thread as finished
            thread.state = ThreadState::Finished;

            // set thread result
            thread.result.upgrade().map(|r| r.set(result));
        } else {
            panic!("cannot end idle thread");
        }
    }
}

static SCHEDULER: spin::Once<spin::RwLock<Scheduler>> = spin::Once::new();

unsafe impl Send for Scheduler {}

unsafe impl Sync for Scheduler {}

fn get_scheduler_lock() -> &'static spin::RwLock<Scheduler> {
    SCHEDULER.call_once(|| spin::RwLock::new(Scheduler::new()))
}

pub fn spawn_thread<F, T>(f: F, name: Option<String>, stack_size: Option<usize>) -> JoinHandle<T>
    where F: FnOnce() -> T, F: Send + 'static, T: Send + 'static {
    get_scheduler_lock().write().spawn_thread(f, name, stack_size)
}

pub fn context_switch<FSave, FRestore>(save_state: FSave, restore_state: FRestore)
    where FSave: FnOnce(&mut (ThreadImp + 'static)), FRestore: FnOnce(&(ThreadImp + 'static)) {
    get_scheduler_lock().write().context_switch(save_state, restore_state)
}

fn create_thread_imp<F, T>(f: F, stack_size: Option<usize>) -> Box<ThreadImp>
    where F: FnOnce() -> T, F: Send + 'static, T: Send + 'static {
    Box::new(crate::arch::x86_64::cpu::thread::Thread::new(f, stack_size))
}

pub fn end_of_thread<T>(result: T) where T: Send + 'static {
    get_scheduler_lock().write().end_of_thread(result);
}
