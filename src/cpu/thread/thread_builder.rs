use crate::stdlib::string::String;

use super::JoinHandle;
use super::scheduler;

pub struct ThreadBuilder {
    name: Option<String>,
    stack_size: Option<usize>,
}

impl ThreadBuilder {
    pub fn new() -> Self {
        ThreadBuilder { name: None, stack_size: None }
    }

    pub fn set_name(mut self, name: &str) -> Self {
        self.name = Some(String::from(name));
        self
    }

    pub fn set_stack_size(mut self, stack_size: usize) -> Self {
        self.stack_size = Some(stack_size);
        self
    }

    pub fn spawn<F, T>(self, f: F) -> JoinHandle<T>
        where F: FnOnce() -> T, F: Send + 'static, T: Send + 'static
    {
        scheduler::spawn_thread(f, self.name, self.stack_size)
    }
}
