use core::marker::PhantomData;
use core::ptr::NonNull;

use crate::stdlib::sync::Arc;
use crate::stdlib::boxed::Box;

pub mod scheduler;
pub mod thread_id;
pub mod thread_builder;

struct ThreadResult {
    ptr: spin::Mutex<Option<NonNull<()>>>,
}

impl ThreadResult {
    pub fn new() -> ThreadResult {
        ThreadResult {
            ptr: spin::Mutex::new(None),
        }
    }

    pub fn set<T>(&self, result: T) where T: Send + 'static {
        let raw_box = Box::into_raw(Box::new(result));
        let ptr = raw_box as *mut ();
        *self.ptr.lock() = NonNull::new(ptr);
    }

    pub fn get<T>(&self) -> Option<T> where T: Send + 'static {
        let mut lock = self.ptr.lock();
        let ptr = *lock;
        *lock = None;

        ptr.map(|ptr| unsafe { *Box::from_raw(ptr.as_ptr() as *mut T) })
    }
}

pub struct JoinHandle<T: Send + 'static> {
    _phantom: PhantomData<T>,
    result: Arc<ThreadResult>,
}

impl<T: Send + 'static> JoinHandle<T> {
    pub fn join(self) -> Option<T> {
        loop {
            if let Some(val) = self.result.get() {
                return Some(val);
            }

            if Arc::weak_count(&self.result) == 0 {
                return None;
            }

            yield_now();
        }
    }
}

pub fn spawn<F, T>(f: F) -> JoinHandle<T>
    where F: FnOnce() -> T, F: Send + 'static, T: Send + ::core::fmt::Debug + 'static
{
    thread_builder::ThreadBuilder::new().spawn(f)
}

pub fn yield_now() {
    crate::arch::x86_64::cpu::thread::yield_now();
}

static IDLE_FN: spin::RwLock<Option<fn() -> !>> = spin::RwLock::new(None);

pub fn set_idle_fn(f: fn() -> !) {
    *IDLE_FN.write() = Some(f);
}

pub fn get_idle_fn() -> Option<fn() -> !> {
    IDLE_FN.read().clone()
}
