use crate::arch::x86_64::apic::id::read;

pub fn get_local_id() -> u32 {
    unsafe { read() as u32 }
}
